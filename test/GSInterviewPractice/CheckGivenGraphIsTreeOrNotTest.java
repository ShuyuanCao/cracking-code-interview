package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CheckGivenGraphIsTreeOrNotTest {
	
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testNormal(){
		CheckGivenGraphIsTreeOrNot g1 = new CheckGivenGraphIsTreeOrNot(5);
		g1.addEdge(1, 0);
		g1.addEdge(0, 2);
		g1.addEdge(0, 3);
		g1.addEdge(3, 4);
		
		assertTrue(g1.isTree());
		
		CheckGivenGraphIsTreeOrNot g2 = new CheckGivenGraphIsTreeOrNot(5);
		g1.addEdge(1, 0);
		g1.addEdge(0, 2);
		g1.addEdge(2, 1);
		g1.addEdge(0, 3);
		g1.addEdge(3, 4);
		
		assertFalse(g2.isTree());
	}
}