package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SkyLineProblemTest {
	
	private SkyLineProblem skyLineProblem;
	
	@Before
	public void setUp() throws Exception {
		this.skyLineProblem = new SkyLineProblem();
	}
	
	@Test
	public void testNormalCase(){
		int[][] array = new int[][]{
				{2, 9, 10},
				{3, 7, 15},
				{5, 12, 12},
				{15, 20, 10},
				{19, 24, 8}
		};
		
		int[][] realResult = new int[][]{
				{2, 10},
				{3, 15},
				{7, 12},
				{12, 0},
				{15, 10},
				{20, 8},
				{24, 0}
		};
		
		List<int[]> results = this.skyLineProblem.getSkyline(array);
		
		for(int i = 0; i < results.size(); i++){
			assertArrayEquals(realResult[i], results.get(i));
		}
	}
}