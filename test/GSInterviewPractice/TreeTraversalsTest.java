package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class TreeTraversalsTest {
	
	private TreeTraversals treeTraversals;
	private BinaryTreeNode root;
	
	@Before
	public void setUp() throws Exception {
		this.treeTraversals = new TreeTraversals();
		
		BinaryTreeNode node1 = new BinaryTreeNode(1);
		BinaryTreeNode node2 = new BinaryTreeNode(2);
		BinaryTreeNode node3 = new BinaryTreeNode(3);
		BinaryTreeNode node4 = new BinaryTreeNode(4);
		BinaryTreeNode node5 = new BinaryTreeNode(5);
		
		node1.left = node2;
		node1.right = node3;
		node2.left = node4;
		node2.right = node5;
		
		this.root = node1;
	}
	
	@Test
	public void testPreOrderTraversal(){
		int expectedResults[] = {1, 2, 4, 5, 3};
		LinkedList<Integer> results = this.treeTraversals.preOrderTraversal(this.root);
		for(int i = 0; i < expectedResults.length; i++){
			assertEquals(expectedResults[i], results.get(i), 0);
		}
	}
	
	@Test
	public void testInOrderTraversal(){
		int expectedResults[] = {4, 2, 5, 1, 3};
		LinkedList<Integer> results = this.treeTraversals.inOrderTraversal(this.root);
		for(int i = 0; i < expectedResults.length; i++){
			assertEquals(expectedResults[i], results.get(i), 0);
		}
	}
	
	@Test
	public void testPostOrderTraversal(){
		int expectedResults[] = {4, 5, 2, 3, 1};
		LinkedList<Integer> results = this.treeTraversals.postOrderTraversal(this.root);
		for(int i = 0; i < expectedResults.length; i++){
			assertEquals(expectedResults[i], results.get(i), 0);
		}
	}
}