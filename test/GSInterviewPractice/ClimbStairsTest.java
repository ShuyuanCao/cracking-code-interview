package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ClimbStairsTest {
	
	private ClimbStairs climbStairs;
	
	@Before
	public void setUp() throws Exception {
		this.climbStairs = new ClimbStairs();
	}
	
	@Test
	public void testSolution1(){
		assertThat(this.climbStairs.solution1(1), is(1));
		assertThat(this.climbStairs.solution1(2), is(2));
		assertThat(this.climbStairs.solution1(3), is(3));
		assertThat(this.climbStairs.solution1(4), is(5));
	}
	
	@Test
	public void testSolution2(){
		assertThat(this.climbStairs.solution2(1), is(1));
		assertThat(this.climbStairs.solution2(2), is(2));
		assertThat(this.climbStairs.solution2(3), is(3));
		assertThat(this.climbStairs.solution2(4), is(5));
	}
	
	@Test
	public void testSolution3(){
		assertThat(this.climbStairs.solution3(1), is(1));
		assertThat(this.climbStairs.solution3(2), is(2));
		assertThat(this.climbStairs.solution3(3), is(3));
		assertThat(this.climbStairs.solution3(4), is(5));
	}
	
	@Test
	public void testGeneralizedSolution(){
		assertThat(this.climbStairs.generalizedSolution(1, 2), is(1));
		assertThat(this.climbStairs.generalizedSolution(2, 2), is(2));
		assertThat(this.climbStairs.generalizedSolution(3, 2), is(3));
		assertThat(this.climbStairs.generalizedSolution(4, 2), is(5));
	}
}