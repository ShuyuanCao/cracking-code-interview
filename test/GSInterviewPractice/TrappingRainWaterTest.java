package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TrappingRainWaterTest {
	
	private TrappingRainWater trappingRainWater;
	
	@Before
	public void setUp() throws Exception {
		this.trappingRainWater = new TrappingRainWater();
	}
	
	@Test
	public void testNullAndEmptyArray(){
		
		int[] array = {};
		int[] nullArray = null;
		int[] specialArray = {1, 2};
		assertEquals(0, this.trappingRainWater.findWater(array));
		assertEquals(0, this.trappingRainWater.findWater(nullArray));
		assertEquals(0, this.trappingRainWater.findWater(specialArray));
	}
	
	@Test
	public void testNormalCaseS1(){
		int[] array = {2, 0, 2};
		assertEquals(2, this.trappingRainWater.findWater(array));
		
		int[] array2 = {3, 0, 0, 2, 0, 4};
		assertEquals(10, this.trappingRainWater.findWater(array2));
		
		int[] array3 = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
		assertEquals(6, this.trappingRainWater.findWater(array3));
	}
	
	@Test
	public void testNormalCaseS2(){
		int[] array = {2, 0, 2};
		assertEquals(2, this.trappingRainWater.findWaterS2(array));
		
		int[] array2 = {3, 0, 0, 2, 0, 4};
		assertEquals(10, this.trappingRainWater.findWaterS2(array2));
		
		int[] array3 = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
		assertEquals(6, this.trappingRainWater.findWaterS2(array3));
	}
	
	@Test
	public void testNormalCaseS3(){
		int[] array = {2, 0, 2};
		assertEquals(2, this.trappingRainWater.findWaterS3(array));
		
		int[] array2 = {3, 0, 0, 2, 0, 4};
		assertEquals(10, this.trappingRainWater.findWaterS3(array2));
		
		int[] array3 = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
		assertEquals(6, this.trappingRainWater.findWaterS3(array3));
	}
}