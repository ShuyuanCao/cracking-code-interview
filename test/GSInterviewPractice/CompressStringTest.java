package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CompressStringTest {
	
	private CompressString compressString;
	
	@Before
	public void setUp() throws Exception {
		this.compressString = new CompressString();
	}
	
	@Test
	public void testNormalCase(){
		String tmp = "aaabbbccccc";
		assertThat(this.compressString.compressStr(tmp), is("a3b3c5"));
	}
}