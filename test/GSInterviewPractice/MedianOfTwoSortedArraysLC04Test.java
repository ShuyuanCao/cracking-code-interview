package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MedianOfTwoSortedArraysLC04Test {
	
	private MedianOfTwoSortedArraysLC04 medianOfTwoSortedArraysLC04;
	
	@Before
	public void setUp() throws Exception {
		this.medianOfTwoSortedArraysLC04 = new MedianOfTwoSortedArraysLC04();
	}
	
	@Test
	public void testNormalCase(){
		int nums1[] = new int[]{1, 3};
		int nums2[] = new int[]{2};
		
		assertEquals(2, this.medianOfTwoSortedArraysLC04.findMedianSortedArrays(nums1, nums2), 0.0);
	}
}