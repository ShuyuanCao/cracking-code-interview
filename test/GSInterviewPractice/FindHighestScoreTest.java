package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FindHighestScoreTest {
	
	private FindHighestScore findHighestScore;
	
	@Before
	public void setUp() throws Exception {
		this.findHighestScore = new FindHighestScore();
	}
	
	@Test
	public void testNormalCase(){
		String[][] tmp = {{"bob", "88"}, {"ted", "100"}, {"ted", "20"}};
		
		assertThat(88.0, is(this.findHighestScore.findHighestScore(tmp)));
	}
}