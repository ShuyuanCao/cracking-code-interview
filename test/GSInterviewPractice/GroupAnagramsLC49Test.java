package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class GroupAnagramsLC49Test {
	
	private GroupAnagramsLC49 groupAnagramsLC49;
	
	@Before
	public void setUp() throws Exception {
		this.groupAnagramsLC49 = new GroupAnagramsLC49();
	}
	
	@Test
	public void testNormal(){
		String[] emptyStrs = {};
		assertNull(this.groupAnagramsLC49.groupAnagrams(emptyStrs));
		assertNull(this.groupAnagramsLC49.groupAnagrams(null));
		
		String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};
		List<List<String>> results = this.groupAnagramsLC49.groupAnagrams(strs);
		System.out.println(results);
		
		for(int i = 0; i < results.size(); i++){
			List<String> list = results.get(i);
			if(i == 0){
				assertEquals("eat", list.get(0));
				assertEquals("tea", list.get(1));
				assertEquals("ate", list.get(2));
			}else if(i == 1){
				assertEquals("bat", list.get(0));
			}else if(i == 2){
				assertEquals("tan", list.get(0));
				assertEquals("nat", list.get(1));
			}
		}
	}
}