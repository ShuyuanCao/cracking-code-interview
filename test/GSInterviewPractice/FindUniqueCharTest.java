package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FindUniqueCharTest {
	
	private FindUniqueChar findUniqueChar;
	
	@Before
	public void setUp() throws Exception {
		this.findUniqueChar = new FindUniqueChar();
	}
	
	@Test
	public void testNormalCase(){
		assertThat("", is(this.findUniqueChar.findFirstUniqueChar(null)));
		assertThat("", is(this.findUniqueChar.findFirstUniqueChar(" ")));
		assertThat("G", is(this.findUniqueChar.findFirstUniqueChar("GeekFor")));
	}
}