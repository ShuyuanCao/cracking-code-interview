package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTreeSearchInsertionTest {
	
	private BinarySearchTreeSearchInsertion binarySearchTreeSearchInsertion;
	private BinaryTreeNode root;
	
	@Before
	public void setUp() throws Exception {
		this.binarySearchTreeSearchInsertion = new BinarySearchTreeSearchInsertion();
		
		BinaryTreeNode node8 = new BinaryTreeNode(8);
		BinaryTreeNode node3 = new BinaryTreeNode(3);
		BinaryTreeNode node10 = new BinaryTreeNode(10);
		BinaryTreeNode node1  = new BinaryTreeNode(1);
		BinaryTreeNode node6 = new BinaryTreeNode(6);
		BinaryTreeNode node14 = new BinaryTreeNode(14);
		BinaryTreeNode node4 = new BinaryTreeNode(4);
		BinaryTreeNode node7 = new BinaryTreeNode(7);
		BinaryTreeNode node13 = new BinaryTreeNode(13);
		
		node8.left = node3;
		node8.right = node10;
		node3.left = node1;
		node3.right = node6;
		node6.left = node4;
		node6.right = node7;
		node10.right = node14;
		node14.left = node13;
		
		this.root = node8;
	}
	
	@Test
	public void testSearchAndNull(){
		BinaryTreeNode result = this.binarySearchTreeSearchInsertion.search(this.root, 6);
		assertEquals(6, result.val);
		assertNull(this.binarySearchTreeSearchInsertion.search(this.root,100));
	}
	
	@Test
	public void testInsertion(){
		BinaryTreeNode result = this.binarySearchTreeSearchInsertion.insert(this.root, 5);
		assertEquals(5, this.binarySearchTreeSearchInsertion.search(result, 5).val);
	}
}