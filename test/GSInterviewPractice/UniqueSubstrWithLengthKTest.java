package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class UniqueSubstrWithLengthKTest {
	
	private UniqueSubstrWithLengthK uniqueSubstrWithLengthK;
	
	@Before
	public void setUp() throws Exception {
		this.uniqueSubstrWithLengthK = new UniqueSubstrWithLengthK();
	}
	
	@Test
	public void testNormalCase(){
		String[] results = this.uniqueSubstrWithLengthK.numberOfSubstrs("caaab", 2);
		assertThat(results[0], is("aa"));
		assertThat(results[1], is("ab"));
		assertThat(results[2], is("ca"));
	}
}