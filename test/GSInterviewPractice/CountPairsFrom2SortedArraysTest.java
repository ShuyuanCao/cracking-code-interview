package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CountPairsFrom2SortedArraysTest {
	
	private CountPairsFrom2SortedArrays countPairsFrom2SortedArrays;
	
	@Before
	public void setUp() throws Exception {
		this.countPairsFrom2SortedArrays = new CountPairsFrom2SortedArrays();
	}
	
	@Test
	public void testNormal1(){
		int arr1[] = {1, 3, 5, 7};
		int arr2[] = {2, 3, 5, 8};
		assertEquals(2, this.countPairsFrom2SortedArrays.countPairsEqualsTargetS2(arr1, arr2, 10));
		assertEquals(2, this.countPairsFrom2SortedArrays.countPairsEqualsTargetS3(arr1, arr2, 10));
	}
	
	@Test
	public void testNormal2(){
		int arr1[] = {1, 2, 3, 4, 5, 7, 11};
		int arr2[] = {2, 3, 4, 5, 6, 8, 12};
		assertEquals(5, this.countPairsFrom2SortedArrays.countPairsEqualsTargetS2(arr1, arr2, 9));
		assertEquals(5, this.countPairsFrom2SortedArrays.countPairsEqualsTargetS3(arr1, arr2, 9));
	}
}