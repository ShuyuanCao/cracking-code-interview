package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

import static org.junit.Assert.*;

public class MyPrefixSearch1806PhoneTest {
	
	private MyPrefixSearch1806Phone myPrefixSearch1806Phone;
	private static final String _document = String.join("",
			"In publishing and graphic design, lorem ipsum is a ",
			"filler text commonly used to demonstrate the graphic elements of a ",
			"document or visual presentation. Replacing meaningful content that ",
			"could be distracting with placeholder text may allow viewers to focus ",
			"on graphic aspects such as font, typography, and page layout. It also ",
			"reduces the need for the designer to come up with meaningful text, as ",
			"they can instead use hastily generated lorem ipsum text. The lorem ",
			"ipsum text is typically a scrambled section of De finibus bonorum et ",
			"malorum, a 1st-century BC Latin text by Cicero, with words altered, ",
			"added, and removed to make it nonsensical, improper Latin. A variation ",
			"of the ordinary lorem ipsum text has been used in typesetting since ",
			"the 1960s or earlier, when it was popularized by advertisements for ",
			"Letraset transfer sheets. It was introduced to the Information Age in ",
			"the mid-1980s by Aldus Corporation, which employed it in graphics and ",
			"word processing templates for its desktop publishing program, ",
			"PageMaker, for the Apple Macintosh. A common form of lorem ipsum ",
			"reads: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ",
			"eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ",
			"minim veniam, quis nostrud exercitation ullamco laboris nisi ut ",
			"aliquip ex ea commodo consequat. Duis aute irure dolor in ",
			"reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla ",
			"pariatur. Excepteur sint occaecat cupidatat non proident, sunt in ",
			"culpa qui officia deserunt mollit anim id est laborum.");
	
	@Before
	public void setUp() throws Exception {
		this.myPrefixSearch1806Phone = new MyPrefixSearch1806Phone(MyPrefixSearch1806PhoneTest._document);
	}
	
	@Test
	public void findAll() {
		BiFunction<List<?>, List<?>, Boolean> resultMatches = (actual, expected) ->
				actual != null && expected.equals(actual);
		
		if( resultMatches.apply(
				this.myPrefixSearch1806Phone.findAll(null), new ArrayList<Integer>())
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("pub"), Arrays.asList( 3, 988 ))
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("publishing"), Arrays.asList( 3, 988 ))
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("lab"), Arrays.asList( 1173, 1263, 1517 ))
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("laborum"), Arrays.asList( 1517 ))
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("in"),
				Arrays.asList( 0, 404, 717, 839, 857, 873, 930, 1159, 1334, 1351, 1468))
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("lor"),
				Arrays.asList( 34, 434, 456, 686, 1061, 1080 ))
				&& resultMatches.apply(this.myPrefixSearch1806Phone.findAll("l"),
				Arrays.asList( 34, 309, 434, 456, 557, 651, 686, 806, 1061, 1080, 1173, 1263, 1517))
				&& this.myPrefixSearch1806Phone.findAll("").size() == 0
				&& this.myPrefixSearch1806Phone.findAll("hamburger").size() == 0)
			System.out.println("All tests pass");
		else
			System.out.println("Test failed");
	}
}