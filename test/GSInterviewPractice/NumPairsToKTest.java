package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class NumPairsToKTest {
	
	private NumPairsToK numPairsToK;
	
	@Before
	public void setUp() throws Exception {
		this.numPairsToK = new NumPairsToK();
	}
	
	@Test
	public void testNormal(){
		int array[] = {1, 1, 1, 2, 0};
		int array2[] = {12, 0, 6, 6, 6};
		assertThat(this.numPairsToK.getNumPairs(array, 2), is(4));
		assertThat(this.numPairsToK.getNumPairs(array2, 12), is(4));
	}
}