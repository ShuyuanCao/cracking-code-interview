package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DotProductArraysTest {
	
	private DotProductArrays dotProductArrays;
	
	@Before
	public void setUp() throws Exception {
		this.dotProductArrays = new DotProductArrays();
	}
	
	@Test
	public void testNormalCase(){
		int[] firArray = {1, 2, 3, 4};
		int[] secArray = {5, 6, 7, 8};
		
		assertThat(this.dotProductArrays.dotProduct(firArray, secArray), is(70));
	}
}