package GSInterviewPractice;

import org.junit.Before;
import sun.awt.image.ImageWatched;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class PeriodsSalesPriceTest {
	
	private PeriodsSalesPrice periodsSalesPrice;
	private LinkedList<VendorPrice> testCase;
	
	@Before
	public void setUp() throws Exception {
		this.periodsSalesPrice = new PeriodsSalesPrice();
		this.testCase = new LinkedList<VendorPrice>();
		
		VendorPrice vendorPrice1 = new VendorPrice();
		VendorPrice vendorPrice2 = new VendorPrice();
		VendorPrice vendorPrice3 = new VendorPrice();
		VendorPrice vendorPrice4 = new VendorPrice();
		VendorPrice vendorPrice5 = new VendorPrice();
		
		vendorPrice1.setStart(1);
		vendorPrice1.setEnd(5);
		vendorPrice1.setPrice(20);
		
		vendorPrice2.setStart(3);
		vendorPrice2.setEnd(6);
		vendorPrice2.setPrice(15);
		
		vendorPrice3.setStart(2);
		vendorPrice3.setEnd(8);
		vendorPrice3.setPrice(25);
		
		vendorPrice4.setStart(7);
		vendorPrice4.setEnd(12);
		vendorPrice4.setPrice(18);
		
		vendorPrice5.setStart(1);
		vendorPrice5.setEnd(31);
		vendorPrice5.setPrice(22);
	}
	
}