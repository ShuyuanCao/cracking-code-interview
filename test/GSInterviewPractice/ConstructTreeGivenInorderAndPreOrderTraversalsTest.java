package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class ConstructTreeGivenInorderAndPreOrderTraversalsTest {
	
	private ConstructTreeGivenInorderAndPreOrderTraversals constructTreeGivenInorderAndPreOrderTraversals;
	@Before
	public void setUp() throws Exception {
		this.constructTreeGivenInorderAndPreOrderTraversals = new ConstructTreeGivenInorderAndPreOrderTraversals();
	}
	
	@Test
	public void testNormal(){
		char[] inOrder = {'D', 'B', 'E', 'A', 'F', 'C'};
		char[] preOrder = { 'A', 'B', 'D', 'E', 'C', 'F'};
		Node root = this.constructTreeGivenInorderAndPreOrderTraversals.buildBinaryTree(inOrder, preOrder);
		LinkedList<Character> resultPre = this.constructTreeGivenInorderAndPreOrderTraversals.preOrderTraversal(root);
		LinkedList<Character> resultIn = this.constructTreeGivenInorderAndPreOrderTraversals.inOrderTraversal(root);
		LinkedList<Character> resultPost = this.constructTreeGivenInorderAndPreOrderTraversals.postOrderTraversal(root);
		for(char c : resultPre){
			System.out.println(c);
		}
		System.out.println("in order traversal......");
		for(char c : resultIn){
			System.out.println(c);
		}
		System.out.println("post order traversal......");
		for(char c : resultPost){
			System.out.println(c);
		}
	}
}