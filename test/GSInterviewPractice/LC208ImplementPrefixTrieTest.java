package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import java.util.PrimitiveIterator;

import static org.junit.Assert.*;

public class LC208ImplementPrefixTrieTest {
	
	private LC208ImplementPrefixTrie lc208ImplementPrefixTrie;
	
	@Before
	public void setUp() throws Exception {
		this.lc208ImplementPrefixTrie = new LC208ImplementPrefixTrie();
	}
	
	@Test
	public void testCase(){
		this.lc208ImplementPrefixTrie.insert("apple");
		
		assertTrue(this.lc208ImplementPrefixTrie.search("apple"));
		assertFalse(this.lc208ImplementPrefixTrie.search("app"));
		assertTrue(this.lc208ImplementPrefixTrie.startsWith("app"));
		
		this.lc208ImplementPrefixTrie.insert("app");
		assertTrue(this.lc208ImplementPrefixTrie.search("app"));
		
	}
}