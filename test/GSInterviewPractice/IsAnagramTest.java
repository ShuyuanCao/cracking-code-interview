package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class IsAnagramTest {
	
	private IsAnagram isAnagram;
	
	@Before
	public void setUp() throws Exception {
		this.isAnagram = new IsAnagram();
	}
	
	@Test
	public void testSolution1(){
		assertThat(true, is(this.isAnagram.solution1("abaaaa", "baaaaa")));
	}
	
	@Test
	public void testSolution2(){
		assertThat(true, is(this.isAnagram.solution2("abaaaa", "baaaaa")));
	}
}