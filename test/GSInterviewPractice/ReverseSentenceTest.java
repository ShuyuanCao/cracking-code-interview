package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ReverseSentenceTest {
	
	private ReverseSentence reverseSentence;
	
	@Before
	public void setUp() throws Exception {
		this.reverseSentence = new ReverseSentence();
	}
	
	@Test
	public void testNormalCase(){
		String tmp = "hello world";
		assertThat(this.reverseSentence.reverseSentence(tmp), is("olleh dlrow"));
	}
}