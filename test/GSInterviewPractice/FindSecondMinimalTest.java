package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FindSecondMinimalTest {
	
	private FindSecondMinimal findSecondMinimal;
	
	@Before
	public void setUp() throws Exception {
		this.findSecondMinimal = new FindSecondMinimal();
	}
	
	@Test
	public void testNormalCase(){
		int[] arr = {4, 5, 2, 1, 7, 8, -1, -1, -3, -2, -4, -2};
		int[] arr2 = {-1, -1, -1, -1};
		assertThat(this.findSecondMinimal.findSecondMinimal(arr), is(-3));
		assertThat(this.findSecondMinimal.findSecondMinimal(arr2), is(Integer.MAX_VALUE));
	}
}