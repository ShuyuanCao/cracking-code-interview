package GSInterviewPractice;

import org.junit.Before;
import org.junit.Test;
import sun.plugin.javascript.navig4.Link;

import static org.junit.Assert.*;

public class CloneLinkedinListWithNextAndRandomPointersTest {
	
	private CloneLinkedinListWithNextAndRandomPointers cloneLinkedList;
	private LinkedListNode head;
	
	@Before
	public void setUp() throws Exception {
		this.cloneLinkedList = new CloneLinkedinListWithNextAndRandomPointers();
		LinkedListNode node1 = new LinkedListNode(1);
		LinkedListNode node2 = new LinkedListNode(2);
		LinkedListNode node3 = new LinkedListNode(3);
		LinkedListNode node4 = new LinkedListNode(4);
		LinkedListNode node5 = new LinkedListNode(5);
		node1.next = node2;
		node2.next = node3;
		node3.next = node4;
		node4.next = node5;
		node1.random = node3;
		node2.random = node1;
		node3.random = node5;
		node4.random = node3;
		node5.random = node2;
		this.head = node1;
	}
	
	@Test
	public void testNormal(){
	
		LinkedListNode newNode = this.cloneLinkedList.copyLinkedListS1(this.head);
		LinkedListNode tmp = newNode;
		for(int i = 0; i < 5; i++){
			if(tmp != null){
				System.out.println("Current Val: ");
				System.out.println(tmp.val);
				System.out.println("Random Node Val: ");
				System.out.println(tmp.random.val);
				tmp = tmp.next;
			}
		}
	}
}