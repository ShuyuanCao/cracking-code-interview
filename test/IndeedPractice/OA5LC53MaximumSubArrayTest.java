package IndeedPractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OA5LC53MaximumSubArrayTest {
	
	private OA5LC53MaximumSubArray oa5LC53MaximumSubArray;
	
	@Before
	public void setUp() throws Exception {
		this.oa5LC53MaximumSubArray = new OA5LC53MaximumSubArray();
	}
	
	@Test
	public void testNormalCase(){
		int[] tmp = {-2, 1};
		assertEquals(1, this.oa5LC53MaximumSubArray.maxSubArrayS1(tmp));
		assertEquals(1, this.oa5LC53MaximumSubArray.maxSubArrayS2(tmp));
		assertEquals(1, this.oa5LC53MaximumSubArray.maxSubArrayS3(tmp));
	}
}