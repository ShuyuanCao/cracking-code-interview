package EB;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LC125ValidPalindromeTest {
	
	private LC125ValidPalindrome lc125ValidPalindrome;
	
	@Before
	public void setUp() throws Exception {
		this.lc125ValidPalindrome = new LC125ValidPalindrome();
	}
	
	@Test
	public void testIsPalindrome(){
		assertFalse(this.lc125ValidPalindrome.isPalindrome(null));
		assertTrue(this.lc125ValidPalindrome.isPalindrome(""));
		
		assertTrue(this.lc125ValidPalindrome.isPalindrome("A man, a plan, a canal: Panama"));
		assertFalse(this.lc125ValidPalindrome.isPalindrome("race a car"));
	}
	
	@Test
	public void testIsValid(){
		assertTrue(this.lc125ValidPalindrome.isValid('y'));
		assertFalse(this.lc125ValidPalindrome.isValid('*'));
	}
	
	@Test
	public void testIsEqual(){
		assertTrue(this.lc125ValidPalindrome.isEqual('a', 'A'));
		assertFalse(this.lc125ValidPalindrome.isEqual('D', 'F'));
		assertTrue(this.lc125ValidPalindrome.isEqual('6', '6'));
	}
	
}