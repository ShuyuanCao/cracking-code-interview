package EB;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LC680ValidPalindrome2Test {
	
	private LC680ValidPalindrome2 lc680ValidPalindrome2;
	
	@Before
	public void setUp() throws Exception {
		this.lc680ValidPalindrome2 = new LC680ValidPalindrome2();
	}
	
	@Test
	public void testCase(){
		assertTrue(this.lc680ValidPalindrome2.validPalindrome("aba"));
		assertTrue(this.lc680ValidPalindrome2.validPalindrome("abca"));
	}
	
	@Test
	public void testIsPalindrome(){
		assertTrue(this.lc680ValidPalindrome2.isPalindrome("abba", 0, 3));
		assertFalse(this.lc680ValidPalindrome2.isPalindrome("abbc", 0, 3));
	}
}