package cc150.chapter02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PartitionLinkedList_2_4Test {
	private PartitionLinkedList_2_4 partitionLinkedList_2_4 = null;
	private Node list = null;
	@Before
	public void setUp() throws Exception {
		this.partitionLinkedList_2_4 = new PartitionLinkedList_2_4();
		Node one = new Node(1);
		Node two = new Node(2);
		Node three = new Node(4);
		Node fourth = new Node(5);
		Node fifth = new Node(3);
		Node sixth = new Node(2);
		one.setNext(two);
		two.setNext(three);
		three.setNext(fourth);
		fourth.setNext(fifth);
		fifth.setNext(sixth);
		sixth.setNext(null);
		this.list = one;
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void partitionList() throws Exception {
		Node result = this.partitionLinkedList_2_4.partitionList(list, 3);
		assertThat(result, is(not(nullValue())));
		int mid = 0;
		for(int i=0; i<6; i++){
			System.out.println(result.getData());
			if(i == 3){
				mid = result.getData();
			}
			result = result.getNext();
		}
		assertThat(mid, equalTo(4));
	}
	
	@Test
	public void partition_2_Test() throws Exception{
		Node result = this.partitionLinkedList_2_4.partitionList_2(list, 3);
		assertThat(result, is(not(nullValue())));
		int mid = 0;
		for(int i=0; i<6; i++){
			System.out.println(result.getData());
			if(i == 2){
				mid = result.getData();
			}
			result = result.getNext();
		}
		assertThat(mid, equalTo(1));
	}
}