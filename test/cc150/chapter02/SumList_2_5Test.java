package cc150.chapter02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SumList_2_5Test {
	
	private SumList_2_5 sumList_2_5 = null;
	private Node list1 = null;
	private Node list2 = null;
	private Node list3 = null;
	
	@Before
	public void setUp() throws Exception {
		this.sumList_2_5 = new SumList_2_5();
		Node one = new Node(7);
		Node two = new Node(1);
		Node three = new Node(6);
		one.setNext(two);
		two.setNext(three);
		three.setNext(null);
		this.list1 = one;
		
		Node fourth = new Node(5);
		Node fifth = new Node(9);
		Node sixth = new Node(2);
		fourth.setNext(fifth);
		fifth.setNext(sixth);
		sixth.setNext(null);
		this.list2 = fourth;
		
		Node seven = new Node(1);
		Node eight = new Node(1);
		seven.setNext(eight);
		eight.setNext(null);
		this.list3 = seven;
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void solution_1() throws Exception {
		Node result = this.sumList_2_5.solution_1(list1, list2, 0);
		int[] values = {2, 1, 9};
		int counter = 0;
		while(result != null){
			assertThat(result.getData(), is(values[counter]));
			counter++;
			result = result.getNext();
		}
	}
	
	@Test
	public void solution_1_test_case_2() throws Exception {
		Node result = this.sumList_2_5.solution_1(list1, list3, 0);
		int[] values = {2, 1, 9};
		int counter = 0;
		while(result != null){
			assertThat(result.getData(), is(values[counter]));
			counter++;
			result = result.getNext();
		}
	}
	
	@Test
	public void solution_2_test() throws Exception {
		Node result = this.sumList_2_5.follow_up(this.list1, this.list2);
		int values[] = {1, 3, 0, 8};
		int counter = 0;
		while(result != null){
			assertThat(result.getData(), is(values[counter]));
			result = result.getNext();
			counter++;
		}
	}
	@Test
	public void solution_2_testCase_2() throws Exception{
		Node result = this.sumList_2_5.follow_up(this.list1, this.list3);
		int values[] = {7, 2, 7};
		int counter = 0;
		while(result != null){
			assertThat(result.getData(), is(values[counter]));
			result = result.getNext();
			counter++;
		}
	}
	
}