package cc150.chapter02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Palindrome_2_6Test {
	private Palindrome_2_6 palindrome_2_6;
	private Node list;
	private Node list_no;
	private Node list_one;
	private Node list_two;
	@Before
	public void setUp() throws Exception {
		this.palindrome_2_6 = new Palindrome_2_6();
		
		Node one = new Node(1);
		Node two = new Node(2);
		Node three = new Node(3);
		Node fourth = new Node(3);
		Node fifth = new Node(2);
		Node sixth = new Node(1);
		one.setNext(two);
		two.setNext(three);
		three.setNext(fourth);
		fourth.setNext(fifth);
		fifth.setNext(sixth);
		sixth.setNext(null);
		this.list = one;
		
		Node one2 = new Node(1);
		Node two2 = new Node(2);
		Node three2 = new Node(3);
		Node fourth2 = new Node(4);
		Node fifth2 = new Node(2);
		Node sixth2 = new Node(1);
		one2.setNext(two2);
		two2.setNext(three2);
		three2.setNext(fourth2);
		fourth2.setNext(fifth2);
		fifth2.setNext(sixth2);
		sixth2.setNext(null);
		this.list_no = one2;
		
		Node one3 = new Node(1);
		one3.setNext(null);
		this.list_one = one3;
		
		Node one4 = new Node(1);
		Node two4 = new Node(2);
		one4.setNext(two4);
		two4.setNext(null);
		this.list_two = one4;
	}
	
	@After
	public void tearDown() throws Exception {
	
	}
	
	@Test
	public void test_normal_case(){
		boolean result = this.palindrome_2_6.solution_1(list);
		assertThat(result, is(true));
	}
	
	@Test
	public void test_abnormal_case(){
		boolean result = this.palindrome_2_6.solution_1(list_no);
		assertThat(result, is(false));
	}
	
	@Test
	public void test_normal_solution_2(){
		boolean result = this.palindrome_2_6.solution_2(list);
		assertThat(result, is(true));
	}
	
	@Test
	public void test_abnormal_solution_2(){
		boolean result = this.palindrome_2_6.solution_2(list_no);
		assertThat(result, is(false));
	}
	
	@Test
	public void test_normal_solution_3(){
		boolean result = this.palindrome_2_6.solution_3(list);
		assertThat(result, is(true));
	}
	
	@Test
	public void test_abnormal_solution_3(){
		boolean result = this.palindrome_2_6.solution_3(list_no);
		assertThat(result, is(false));
	}
	
	@Test
	public void test_normal_solution_3_2(){
		boolean result = this.palindrome_2_6.solution_3(list_one);
		assertThat(result, is(true));
	}
	
	@Test
	public void test_abnormal_solution_3_3(){
		boolean result = this.palindrome_2_6.solution_3(list_two);
		assertThat(result, is(false));
	}
}