package cc150.chapter02;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

public class IntersectSinglyLinkedList_2_7Test {
	
	private IntersectSinglyLinkedList_2_7 intersectSinglyLinkedList_2_7;
	private Node list1;
	private Node list2;
	private Node list3;
	
	@Before
	public void setUp() throws Exception {
		this.intersectSinglyLinkedList_2_7 = new IntersectSinglyLinkedList_2_7();
		
		Node common1 = new Node(1);
		Node common2 = new Node(2);
		Node common3 = new Node(3);
		Node common4 = new Node(4);
		common1.setNext(common2);
		common2.setNext(common3);
		common3.setNext(common4);
		common4.setNext(null);
		
		Node node_1_1 = new Node(8);
		Node node_1_2 = new Node(9);
		Node node_1_3 = new Node(10);
		node_1_1.setNext(node_1_2);
		node_1_2.setNext(node_1_3);
		node_1_3.setNext(common1);
		this.list1 = node_1_1;
		
		Node node_2_1 = new Node(0);
		Node node_2_2 = new Node(11);
		node_2_1.setNext(node_2_2);
		node_2_2.setNext(common1);
		this.list2 = node_2_1;
		
		Node node_3_1 = new Node(12);
		Node node_3_2 = new Node(13);
		Node node_3_3 = new Node(14);
		node_3_1.setNext(node_3_2);
		node_3_2.setNext(node_3_3);
		node_3_3.setNext(null);
		this.list3 = node_3_1;
	}
	
	@Test
	public void test_find_intersecting_node_null(){
		Node result = this.intersectSinglyLinkedList_2_7.solution(list1, list3);
		assertThat(result, is(nullValue()));
	}
	
	@Test
	public void test_find_intersecting_node_exist(){
		Node result = this.intersectSinglyLinkedList_2_7.solution(list1, list2);
		assertThat(result, is(notNullValue()));
	}
	
}