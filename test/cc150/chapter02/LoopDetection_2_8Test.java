package cc150.chapter02;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

public class LoopDetection_2_8Test {
	
	private LoopDetection_2_8 loopDetection_2_8;
	private Node list;
	private Node list2;
	
	@Before
	public void setUp() throws Exception {
		this.loopDetection_2_8 = new LoopDetection_2_8();
		
		Node common1 = new Node(1);
		Node common2 = new Node(2);
		Node common3 = new Node(3);
		Node common4 = new Node(4);
		common1.setNext(common2);
		common2.setNext(common3);
		common3.setNext(common4);
		common4.setNext(common1);
		
		Node node_1_1 = new Node(8);
		Node node_1_2 = new Node(9);
		Node node_1_3 = new Node(10);
		node_1_1.setNext(node_1_2);
		node_1_2.setNext(node_1_3);
		node_1_3.setNext(common1);
		this.list = node_1_1;
		
		Node node_2_1 = new Node(1);
		Node node_2_2 = new Node(2);
		Node node_2_3 = new Node(3);
		node_2_1.setNext(node_2_2);
		node_2_2.setNext(node_2_3);
		node_2_3.setNext(null);
		this.list2 = node_2_1;
	}
	
	@Test
	public void test_solution_normal(){
		Node result = this.loopDetection_2_8.solution(this.list);
		System.out.print(result.data);
		assertThat(result, is(notNullValue()));
		assertEquals(1, result.getData());
	}
	
	@Test
	public void test_solution_abnormal(){
		Node result = this.loopDetection_2_8.solution(list2);
		assertThat(result, is(nullValue()));
	}
}