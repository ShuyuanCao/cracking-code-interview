package cc150.chapter03;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


public class ThreeInOne_3_1_Test {
	
	private ThreeInOne_3_1_S_1 threeInOne_3_1_s_1;
	private ThreeInOne_3_1_S_2 threeInOne_3_1_s_2;
	
	@Before
	public void setUp() {
		this.threeInOne_3_1_s_1 = new ThreeInOne_3_1_S_1(3);
		this.threeInOne_3_1_s_2 = new ThreeInOne_3_1_S_2();
		
	}
	
	@Test
	public void test_solution() throws Exception {
		
		// push and pop for one stack at one time
		for(int i=0; i<3; i++){
			this.threeInOne_3_1_s_1.push(i, 1);
			this.threeInOne_3_1_s_1.push(i, 2);
			this.threeInOne_3_1_s_1.push(i, 3);
			
			assertThat(this.threeInOne_3_1_s_1.pop(i), is(3));
			assertThat(this.threeInOne_3_1_s_1.pop(i), is(2));
			assertThat(this.threeInOne_3_1_s_1.pop(i), is(1));
			
			this.threeInOne_3_1_s_1.push(i, 1);
			assertThat(this.threeInOne_3_1_s_1.pop(i), is(1));
			this.threeInOne_3_1_s_1.push(i, 2);
			assertThat(this.threeInOne_3_1_s_1.pop(i), is(2));
			this.threeInOne_3_1_s_1.push(i, 3);
			assertThat(this.threeInOne_3_1_s_1.pop(i), is(3));
			
		}
		
		// push and pop for multiple stacks at one time
		this.threeInOne_3_1_s_1.push(0, 0);
		this.threeInOne_3_1_s_1.push(1, 1);
		this.threeInOne_3_1_s_1.push(2, 2);
		assertThat(this.threeInOne_3_1_s_1.pop(0), is(0));
		assertThat(this.threeInOne_3_1_s_1.pop(2), is(2));
		assertThat(this.threeInOne_3_1_s_1.pop(1), is(1));
		
	}
	
	@Test(expected = Exception.class)
	public void test_solution_empty_stack_exception() throws Exception {
		// pop empty stack
		this.threeInOne_3_1_s_1.pop(0);
	}
	
	@Test(expected = Exception.class)
	public void test_solution_push_full_stack_exception() throws Exception {
		// push to full stack
		this.threeInOne_3_1_s_1.push(1, 1);
		this.threeInOne_3_1_s_1.push(1, 2);
		this.threeInOne_3_1_s_1.push(1, 3);
		this.threeInOne_3_1_s_1.push(1, 4);
	}
	
	@Test
	public void test_peek_function() throws Exception {
		// test peek function
		this.threeInOne_3_1_s_1.push(1, 1);
		this.threeInOne_3_1_s_1.push(1, 2);
		this.threeInOne_3_1_s_1.push(1, 3);
		assertThat(this.threeInOne_3_1_s_1.peek(1), is(3));
		this.threeInOne_3_1_s_1.pop(1);
		assertThat(this.threeInOne_3_1_s_1.peek(1), is(2));
	}
}