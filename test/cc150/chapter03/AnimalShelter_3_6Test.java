package cc150.chapter03;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AnimalShelter_3_6Test {
	
	private AnimalShelter_3_6 animalShelter_3_6;
	
	@Before
	public void setUp() throws Exception {
		this.animalShelter_3_6 = new AnimalShelter_3_6();
	}
	
	@Test
	public void test_normal_case(){
		Animal dog = new Dog("dog1");
		Animal cat = new Cat("cat1");
		
		this.animalShelter_3_6.enqueue(dog);
		this.animalShelter_3_6.enqueue(cat);
		Animal result = this.animalShelter_3_6.dequeueAny();
		assertThat(result.name, is("dog1"));
	}
	
}