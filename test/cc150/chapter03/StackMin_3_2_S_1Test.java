package cc150.chapter03;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class StackMin_3_2_S_1Test {
	
	private StackMin_3_2_S_1 stackMin_3_2_s_1;
	private StackMin_3_2_S_2 stackMin_3_2_s_2;
	
	@Before
	public void setUp() throws Exception {
	
		this.stackMin_3_2_s_1 = new StackMin_3_2_S_1();
		this.stackMin_3_2_s_2 = new StackMin_3_2_S_2();
	
	}
	
	@Test
	public void test_min_s1(){
		this.stackMin_3_2_s_1.push(3);
		assertThat(this.stackMin_3_2_s_1.min(), is(3));
		
		this.stackMin_3_2_s_1.push(5);
		this.stackMin_3_2_s_1.push(6);
		this.stackMin_3_2_s_1.push(7);
		assertThat(this.stackMin_3_2_s_1.min(), is(3));
		
		this.stackMin_3_2_s_1.push(1);
		assertThat(this.stackMin_3_2_s_1.min(), is(1));
		
		this.stackMin_3_2_s_1.pop();
		assertThat(this.stackMin_3_2_s_1.min(), is(3));
	}
	
	@Test
	public void test_min_s2(){
		this.stackMin_3_2_s_2.push(3);
		assertThat(this.stackMin_3_2_s_2.min(), is(3));
		
		this.stackMin_3_2_s_2.push(5);
		this.stackMin_3_2_s_2.push(6);
		this.stackMin_3_2_s_2.push(7);
		assertThat(this.stackMin_3_2_s_2.min(), is(3));
		
		this.stackMin_3_2_s_2.push(1);
		assertThat(this.stackMin_3_2_s_2.min(), is(1));
		
		int val = this.stackMin_3_2_s_2.pop();
		
		assertThat(val, is(1));
		assertThat(this.stackMin_3_2_s_2.min(), is(3));
	}
	
}