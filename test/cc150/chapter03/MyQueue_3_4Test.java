package cc150.chapter03;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MyQueue_3_4Test {
	
	private MyQueue_3_4 myQueue_3_4;
	
	@Before
	public void setUp() throws Exception {
		this.myQueue_3_4 = new MyQueue_3_4();
	}
	
	@Test
	public void test_case_1(){
		this.myQueue_3_4.add(1);
		this.myQueue_3_4.add(2);
		this.myQueue_3_4.add(3);
		this.myQueue_3_4.add(4);
		this.myQueue_3_4.add(5);
		
		assertThat(this.myQueue_3_4.peek(), is(1));
		
		assertThat(this.myQueue_3_4.remove(), is(1));
		assertThat(this.myQueue_3_4.remove(), is(2));
		assertThat(this.myQueue_3_4.remove(), is(3));
		
	}
	
}