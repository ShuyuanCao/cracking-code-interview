package cc150.chapter03;

import org.junit.Before;
import org.junit.Test;

import java.util.EmptyStackException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class StackOfPlates_3_3_S_1Test {
	
	private StackOfPlates_3_3_S_1 stackOfPlates_3_3_s_1;
	
	@Before
	public void setUp() throws Exception {
		this.stackOfPlates_3_3_s_1 = new StackOfPlates_3_3_S_1(3);
	}
	
	@Test(expected = EmptyStackException.class)
	public void test_s_1(){
		this.stackOfPlates_3_3_s_1.pop();
	}
	
	@Test(expected = EmptyStackException.class)
	public void test_s_case_2(){
		this.stackOfPlates_3_3_s_1.push(1);
		this.stackOfPlates_3_3_s_1.push(2);
		this.stackOfPlates_3_3_s_1.pop();
		this.stackOfPlates_3_3_s_1.pop();
		this.stackOfPlates_3_3_s_1.pop();
	}
	
	@Test
	public void test_s_case_3(){
		this.stackOfPlates_3_3_s_1.push(1);
		this.stackOfPlates_3_3_s_1.push(2);
		this.stackOfPlates_3_3_s_1.push(3);
		this.stackOfPlates_3_3_s_1.push(4);
		this.stackOfPlates_3_3_s_1.push(5);
		int val = this.stackOfPlates_3_3_s_1.pop();
		assertThat(val, is(5));
	}
	
}