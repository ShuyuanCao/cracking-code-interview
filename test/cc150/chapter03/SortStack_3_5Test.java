package cc150.chapter03;

import org.junit.Before;
import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class SortStack_3_5Test {
	
	private SortStack_3_5 sortStack_3_5;
	private Stack<Integer> stack;
	
	@Before
	public void setUp() throws Exception {
		this.sortStack_3_5 = new SortStack_3_5();
		
		this.stack = new Stack<Integer>();
		this.stack.push(1);
		this.stack.push(2);
		this.stack.push(3);
		this.stack.push(4);
		this.stack.push(5);
	}
	
	@Test
	public void test_wanna_cry(){
		Stack<Integer> result = this.sortStack_3_5.sortStack(this.stack);
		while(!result.isEmpty()){
			System.out.println(result.pop());
		}
	}
	
}