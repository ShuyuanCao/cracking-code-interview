package cc150.chapter05;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Conversion_5_6Test {
	
	private Conversion_5_6 conversion_5_6;
	
	@Before
	public void setUp() throws Exception {
		this.conversion_5_6 = new Conversion_5_6();
	}
	
	@Test
	public void testNormalCase(){
		assertThat(2, is(this.conversion_5_6.bitSwapRequired(29, 15)));
		assertThat(2, is(this.conversion_5_6.bitSwapRequiredS2(29, 15)));
	}
	
}