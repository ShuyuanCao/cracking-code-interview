package cc150.chapter05;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class NextNumber_5_4Test {
	
	private NextNumber_5_4 nextNumber_5_4;
	
	@Before
	public void setUp() throws Exception {
		this.nextNumber_5_4 = new NextNumber_5_4();
	}
	
	@Test
	public void testNormalCase(){
		assertThat(4, is(this.nextNumber_5_4.getNext(2)));
		assertThat(2, is(this.nextNumber_5_4.getPrevious(4)));
		
		assertThat(4, is(this.nextNumber_5_4.getNextArith(2)));
		assertThat(2, is(this.nextNumber_5_4.getPrevArith(4)));
	}
	
}