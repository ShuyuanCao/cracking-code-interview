package cc150.chapter05;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class PairwiseSwap_5_7Test {
	
	private PairwiseSwap_5_7 pairwiseSwap_5_7;
	
	@Before
	public void setUp() throws Exception {
		this.pairwiseSwap_5_7 = new PairwiseSwap_5_7();
	}
	
	@Test
	public void testNormalCase(){
		assertThat(1, is(this.pairwiseSwap_5_7.swapOddEvenBits(2)));
	}
	
}