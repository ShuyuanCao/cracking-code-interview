package cc150.chapter05;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FlipBitToWin_5_3Test {
	
	private FlipBitToWin_5_3 flipBitToWin_5_3;
	
	@Before
	public void setUp() throws Exception {
		this.flipBitToWin_5_3 = new FlipBitToWin_5_3();
	}
	
	@Test
	public void testNormal(){
		assertThat(8, is(this.flipBitToWin_5_3.longestSequence(1775)));
		assertThat(8, is(this.flipBitToWin_5_3.flipBit(1775)));
	}
	
}