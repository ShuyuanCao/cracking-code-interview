package cc150.chapter05;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Insertion_5_1Test {
	
	private Insertion_5_1 insertion_5_1;
	
	@Before
	public void setUp() throws Exception {
		this.insertion_5_1 = new Insertion_5_1();
	}
	
	@Test
	public void testNormalCase(){
		assertThat(12, is(this.insertion_5_1.updateBits(8, 2, 1, 2)));
	}
	
}