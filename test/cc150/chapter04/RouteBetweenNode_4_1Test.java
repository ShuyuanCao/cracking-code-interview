package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class RouteBetweenNode_4_1Test {
	
	private RouteBetweenNode_4_1 routeBetweenNode_4_1;
	private Node a, b, c, d, e;
	private Graph g;
	
	@Before
	public void setUp() throws Exception {
		this.routeBetweenNode_4_1 = new RouteBetweenNode_4_1();
		
		this.a = new Node("a");
		this.b = new Node("b");
		this.c = new Node("c");
		this.d = new Node("d");
		this.e = new Node("e");
		Node[] a_ajacent = {b, c};
		Node[] b_ajacent = {};
		Node[] c_ajacent = {b, d};
		Node[] d_ajacent = {e};
		Node[] e_ajacent = {};
		a.setAjacent(a_ajacent);
		b.setAjacent(b_ajacent);
		c.setAjacent(c_ajacent);
		d.setAjacent(d_ajacent);
		e.setAjacent(e_ajacent);
		
		Node[] graph = {a, b, c, d, e};
		
		this.g = new Graph(graph);
	}
	
	@Test
	public void test_normal_case(){
		assertThat(true, is(this.routeBetweenNode_4_1.findRoute(g, a, e)));
	}
	
}