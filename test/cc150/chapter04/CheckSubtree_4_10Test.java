package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class CheckSubtree_4_10Test {
	
	private CheckSubtree_4_10 checkSubtree_4_10;
	private BinaryTreeNode t1, t2;
	
	@Before
	public void setUp() throws Exception {
		this.checkSubtree_4_10 = new CheckSubtree_4_10();
		
		t1 = new BinaryTreeNode(1);
		
		BinaryTreeNode l1_1 = new BinaryTreeNode(2);
		t2 = l1_1;
		BinaryTreeNode l1_2 = new BinaryTreeNode(3);
		t1.left = l1_1;
		t1.right = l1_2;
		
		BinaryTreeNode l2_1 = new BinaryTreeNode(4);
		BinaryTreeNode l2_2 = new BinaryTreeNode(5);
		l1_1.left = l2_1;
		l1_1.right = l2_2;
		
		BinaryTreeNode l2_3 = new BinaryTreeNode(6);
		BinaryTreeNode l2_4 = new BinaryTreeNode(7);
		l1_2.left = l2_3;
		l1_2.right = l2_4;
		
	}
	
	@Test
	public void testMethod1(){
		assertThat(true, is(this.checkSubtree_4_10.checkSubtree(this.t1, this.t2)));
		assertThat(true, is(this.checkSubtree_4_10.checkSubtree2(this.t1, this.t2)));
	}
	
}