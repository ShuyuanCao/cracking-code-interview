package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ValidateBST_4_5Test {
	
	private ValidateBST_4_5 validateBST_4_5;
	private BinaryTreeNode a, b, c, d, e;
	
	@Before
	public void setUp() throws Exception {
		
		this.validateBST_4_5 = new ValidateBST_4_5();
		
		this.a = new BinaryTreeNode(1);
		this.b = new BinaryTreeNode(2);
		this.c = new BinaryTreeNode(3);
		this.d = new BinaryTreeNode(4);
		this.e = new BinaryTreeNode(5);
		
		// root node is c
		this.c.left = this.b;
		this.c.right = this.d;
		
		this.b.left = this.e;
		this.d.right = this.a;
		
		this.c.size = 5;
		
	}
	
	@Test
	public void testNormal(){
		boolean result = this.validateBST_4_5.isValidBST(this.c);
		assertThat(result, is(false));
		
		boolean result2 = this.validateBST_4_5.isValidBSTS2(this.c);
		assertThat(result2, is(false));
		
		boolean result3 = this.validateBST_4_5.isValidBSTS3(this.c);
		assertThat(result3, is(false));
	}
	
}