package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FirstCommonAncestor_4_8Test {
	
	private FirstCommonAncestor_4_8 firstCommonAncestor_4_8;
	private BinaryTreeNode a, b, c, d, e;
	
	@Before
	public void setUp() throws Exception {
		this.firstCommonAncestor_4_8 = new FirstCommonAncestor_4_8();
		
		this.a = new BinaryTreeNode(1);
		this.b = new BinaryTreeNode(2);
		this.c = new BinaryTreeNode(3);
		this.d = new BinaryTreeNode(4);
		this.e = new BinaryTreeNode(5);
		
		// root node is c
		this.c.left = this.b;
		this.b.parent = this.c;
		this.c.right = this.d;
		this.d.parent = this.c;
		
		this.b.left = this.e;
		this.e.parent = this.b;
		this.d.right = this.a;
		this.a.parent = this.d;
		
		this.c.size = 5;
		
	}
	
	@Test
	public void test_normal_case(){
		BinaryTreeNode result = this.firstCommonAncestor_4_8.findFirstCommonAncestor(a, e);
		assertThat(result.val, is(3));
		
		BinaryTreeNode result_2 = this.firstCommonAncestor_4_8.findFirstCommonAnscestorS2(c, a, e);
		assertThat(result_2.val, is(3));
		
		BinaryTreeNode result_3 = this.firstCommonAncestor_4_8.findFirstCommonAncestorS3(c, a, e);
		assertThat(result_3.val, is(3));
	}
	
}