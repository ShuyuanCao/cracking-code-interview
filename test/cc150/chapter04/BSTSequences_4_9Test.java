package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BSTSequences_4_9Test {
	
	private BSTSequences_4_9 bstSequences_4_9;
	
	@Before
	public void setUp() throws Exception {
		this.bstSequences_4_9 = new BSTSequences_4_9();
	}
	
	@Test
	public void testNormalCase(){
		// form a binary search tree
		BinaryTreeNode root = new BinaryTreeNode(2);
		BinaryTreeNode left = new BinaryTreeNode(1);
		BinaryTreeNode right = new BinaryTreeNode(3);
		
		root.left = left;
		root.right = right;
		
		ArrayList<LinkedList<Integer>> result = this.bstSequences_4_9.findAllSequences(root);
		LinkedList<Integer> list1 = result.get(0);
		LinkedList<Integer> list2 = result.get(1);
		
		assertThat(2, is(list1.get(0)));
		assertThat(1, is(list1.get(1)));
		assertThat(3, is(list1.get(2)));
		
		assertThat(2, is(list2.get(0)));
		assertThat(3, is(list2.get(1)));
		assertThat(1, is(list2.get(2)));
	}
	
	@Test
	public void testAbnormalCase(){
		ArrayList<LinkedList<Integer>> result = this.bstSequences_4_9.findAllSequences(null);
		assertThat(1, is(result.size()));
		assertThat(0, is(result.get(0).size()));
	}
	
}