package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ListsOfDepth_4_3Test {
	
	private ListsOfDepth_4_3 listsOfDepth_4_3;
	private BinaryTreeNode a, b, c, d, e;
	
	@Before
	public void setUp() throws Exception {
		this.listsOfDepth_4_3 = new ListsOfDepth_4_3();
		
		this.a = new BinaryTreeNode(1);
		this.b = new BinaryTreeNode(2);
		this.c = new BinaryTreeNode(3);
		this.d = new BinaryTreeNode(4);
		this.e = new BinaryTreeNode(5);
		
		this.a.left = this.b;
		this.a.right = this.c;
		this.b.left = this.d;
		this.c.right = this.e;
	}
	
	@Test
	public void test_pre_order_case(){
		ArrayList<LinkedList<BinaryTreeNode>> lists = this.listsOfDepth_4_3.preOrderTraversal(this.a);
		
		LinkedList<BinaryTreeNode> level0 = lists.get(0);
		assertThat(level0.get(0).val, is(1));
		
		LinkedList<BinaryTreeNode> level1 = lists.get(1);
		BinaryTreeNode l1_n1 = level1.get(0);
		assertThat(l1_n1.val, is(2));
		BinaryTreeNode l1_n2 = level1.get(1);
		assertThat(l1_n2.val, is(3));
		
		LinkedList<BinaryTreeNode> level2 = lists.get(2);
		BinaryTreeNode l2_n1 = level2.get(0);
		assertThat(l2_n1.val, is(4));
		BinaryTreeNode l2_n2 = level2.get(1);
		assertThat(l2_n2.val, is(5));
	}
	
	@Test
	public void test_breadth_first_case(){
		ArrayList<LinkedList<BinaryTreeNode>> lists = this.listsOfDepth_4_3.breadthFirstSearch(this.a);
		
		LinkedList<BinaryTreeNode> level0 = lists.get(0);
		assertThat(level0.get(0).val, is(1));
		
		LinkedList<BinaryTreeNode> level1 = lists.get(1);
		BinaryTreeNode l1_n1 = level1.get(0);
		assertThat(l1_n1.val, is(2));
		BinaryTreeNode l1_n2 = level1.get(1);
		assertThat(l1_n2.val, is(3));
		
		LinkedList<BinaryTreeNode> level2 = lists.get(2);
		BinaryTreeNode l2_n1 = level2.get(0);
		assertThat(l2_n1.val, is(4));
		BinaryTreeNode l2_n2 = level2.get(1);
		assertThat(l2_n2.val, is(5));
	}
}