package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CheckBalanced_4_4Test {
	
	private CheckBalanced_4_4 checkBalanced_4_4;
	private BinaryTreeNode a, b, c, d, e;
	
	@Before
	public void setUp() throws Exception {
		
		this.checkBalanced_4_4 = new CheckBalanced_4_4();
		
		this.a = new BinaryTreeNode(1);
		this.b = new BinaryTreeNode(2);
		this.c = new BinaryTreeNode(3);
		this.d = new BinaryTreeNode(4);
		this.e = new BinaryTreeNode(5);
		
		this.a.left = this.b;
		this.a.right = this.c;
		this.b.left = this.d;
		this.c.right = this.e;
		
	}
	
	@Test
	public void test_normal_method(){
		boolean result = this.checkBalanced_4_4.isBinaryTreeBalanced(this.a);
		assertThat(result, is(true));
		
		boolean result_s2 = this.checkBalanced_4_4.isBinaryTreeBalancedS2(this.a);
		assertThat(result_s2, is(true));
	}
}