package cc150.chapter04;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Successor_4_6Test {
	
	private Successor_4_6 successor_4_6;
	private BinaryTreeNode a, b, c, d, e;
	
	@Before
	public void setUp() throws Exception {
		this.successor_4_6 = new Successor_4_6();
		
		this.a = new BinaryTreeNode(1);
		this.b = new BinaryTreeNode(2);
		this.c = new BinaryTreeNode(3);
		this.d = new BinaryTreeNode(4);
		this.e = new BinaryTreeNode(5);
		
		// root node is c
		this.c.left = this.b;
		this.b.parent = this.c;
		this.c.right = this.d;
		this.d.parent = this.c;
		
		this.b.left = this.a;
		this.a.parent = this.b;
		this.d.right = this.e;
		this.e.parent = this.d;
		
		this.c.size = 5;
	}
	
	@Test
	public void test_normal(){
		BinaryTreeNode successor = this.successor_4_6.inorderSuccessor(this.a);
		assertThat(successor.val, is(2));
	}
	
}