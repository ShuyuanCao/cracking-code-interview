package classical_algo;

import classical_algo.sorting_algorithms.SortingAlgorithms;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SortingAlgorithmsTest {
	
	private SortingAlgorithms sortingAlgorithms;
	private int[] arr = {7, 6, 4, 5, 3, 1, 2};
	private int[] ordered = {1, 2, 3, 4, 5, 6, 7};
	
	@Before
	public void setUp() throws Exception {
		this.sortingAlgorithms = new SortingAlgorithms();
	}
	
	@Test
	public void testSortingAlgorithms(){
		this.sortingAlgorithms.bubleSort(this.arr);
		for(int i = 0; i<this.arr.length; i++){
			System.out.println(this.arr[i]);
			assertThat(this.arr[i], is(this.ordered[i]));
		}
	}
	
}