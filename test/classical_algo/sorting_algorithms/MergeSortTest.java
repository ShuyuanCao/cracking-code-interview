package classical_algo.sorting_algorithms;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MergeSortTest {
	
	private MergeSort mergeSort;
	
	@Before
	public void setUp() throws Exception {
		this.mergeSort = new MergeSort();
	}
	
	@Test
	public void testNormalCase(){
		int[] input = { 51, 46, 20, 18 };
		this.mergeSort.mergeSort(input, 0, input.length - 1);
		
		assertEquals(18, input[0]);
		assertEquals(20, input[1]);
		assertEquals(46, input[2]);
		assertEquals(51, input[3]);
	}
}