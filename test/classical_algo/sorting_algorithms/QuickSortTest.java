package classical_algo.sorting_algorithms;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class QuickSortTest {
	
	private QuickSort quickSort;
	
	@Before
	public void setUp() throws Exception {
		this.quickSort = new QuickSort();
	}
	
	@Test
	public void testNormal(){
		int[] tmp = {5, 4, 3, 2, 1};
		this.quickSort.sort(tmp, 0, 4);
		
		for(int i = 0; i < tmp.length; i++){
			assertEquals(i + 1, tmp[i]);
		}
	}
}