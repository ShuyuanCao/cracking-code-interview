package classical_algo.sorting_algorithms;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BubbleSortTest {
	
	private BubbleSort bubbleSort;
	
	@Before
	public void setUp() throws Exception {
		this.bubbleSort = new BubbleSort();
	}
	
	@Test
	public void testNormal(){
		int[] tmp = {5, 4, 3, 2, 1};
		int[] results = this.bubbleSort.sort(tmp);
		
		for(int i = 0; i < results.length; i++){
			assertEquals(i + 1, results[i]);
		}
	}
}