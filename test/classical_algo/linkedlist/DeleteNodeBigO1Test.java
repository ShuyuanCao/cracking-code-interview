package classical_algo.linkedlist;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeleteNodeBigO1Test {
	
	private DeleteNodeBigO1 deleteNodeBigO1;
	private Node head;
	private Node target;
	
	@Before
	public void setUp() throws Exception {
		
		Node first = new Node(1);
		Node second = new Node(2);
		Node third = new Node(3);
		first.next = second;
		second.next = third;
		third.next = null;
		
		this.head = first;
		this.target = second;
		this.deleteNodeBigO1 = new DeleteNodeBigO1();
	}
	
	@Test
	public void testNormal(){
		this.deleteNodeBigO1.deleNodeBigO1(this.target);
		int[] tmp = {1, 3};
		for(int i = 0; i <= 1; i++){
			assertEquals(tmp[i], this.head.val);
			this.head = this.head.next;
		}
	}
}