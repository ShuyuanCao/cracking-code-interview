package classical_algo.linkedlist;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReverseLinkedListTest {
	
	private ReverseLinkedList reverseLinkedList;
	private Node head;
	
	@Before
	public void setUp() throws Exception {
		
		Node first = new Node(1);
		Node sec = new Node(2);
		Node thr = new Node(3);
		Node four = new Node(4);
		Node fifth = new Node(5);
		
		first.next = sec;
		sec.next = thr;
		thr.next = four;
		four.next = fifth;
		fifth.next = null;
		
		this.head = first;
		this.reverseLinkedList = new ReverseLinkedList();
	}
	
	@Test
	public void testS1(){
		
		Node result = this.reverseLinkedList.reverseS1(this.head);
		
		for(int i = 5; i > 0; i--){
			assertEquals(i, result.val);
			result = result.next;
		}
		
	}
	
	@Test
	public void testS2(){
		
		Node result = this.reverseLinkedList.reverseS2(this.head);
		
		for(int i = 5; i > 0; i--){
			assertEquals(i, result.val);
			result = result.next;
		}
		
	}
	
}