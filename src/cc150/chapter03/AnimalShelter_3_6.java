package cc150.chapter03;

import java.util.LinkedList;

/**
 * My girlfriend left me today......
 */

abstract class Animal{
	private int order;
	protected String name;
	public Animal(String n){ name = n;}
	
	public int getOrder() { return order; }
	
	public void setOrder(int order) { this.order = order; }
	
	// compare the orders (timestamp) of animals to return the older item
	public boolean isOlderThan(Animal animal){
		return this.order < animal.getOrder();
	}
}

class Dog extends Animal{
	public Dog(String n){ super(n); }
}

class Cat extends Animal{
	public Cat(String n){ super(n);}
}

public class AnimalShelter_3_6 {

	private int order = 0; // acts as a timestamp
	
	LinkedList<Dog> dogs = new LinkedList<Dog>();
	LinkedList<Cat> cats = new LinkedList<Cat>();
	
	public void enqueue(Animal animal){
		animal.setOrder(order);
		order++;
		
		if(animal instanceof Dog) dogs.addLast((Dog) animal);
		else if(animal instanceof Cat) cats.addLast((Cat) animal);
	}
	
	public Animal dequeueAny(){
		/*
			look at top of dog and cat queues, and pop the queue with
			the oldest value
		 */
		if(cats.size() == 0){
			return dequeueCats();
		}else if(dogs.size() == 0){
			return dequeueDogs();
		}
		
		Dog dog = dogs.peek();
		Cat cat = cats.peek();
		
		if(dog.isOlderThan(cat)){
			return dequeueDogs();
		}else{
			return dequeueCats();
		}
	}
	
	private Animal dequeueDogs() {
		//Retrieves and removes the head (first element) of this list.
		return dogs.poll();
	}
	
	private Animal dequeueCats() {
		return cats.poll();
	}
	
}
