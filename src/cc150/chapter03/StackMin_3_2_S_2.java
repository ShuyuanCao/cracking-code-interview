package cc150.chapter03;

import java.util.Stack;

public class StackMin_3_2_S_2 extends Stack<Integer>{
	
	/**
	 *  Solution 2:
	 *      For the previous solution, there is a issue:
	 *      if we have a huge stack, we waste a lot of space by
	 *      keeping track of the min for every single element.
	 *
	 *    How to improve it?
	 *      we maybe do a bit better than the previous solution
	 *      by using stack that keeps track of the mins.
	 *      when the val is <= current_min in the min stack,
	 *      we push the val into min stack.
	 *
	 *      but we also have to modify the pop function.
	 *
	 */
	private Stack<Integer> minStack;
	
	public StackMin_3_2_S_2(){
		this.minStack = new Stack<Integer>();
	}
	
	public void push(int val){
		super.push(val);
		
		if(val <= this.min()){
			this.minStack.push(val);
		}
	}
	
	public Integer pop(){
		
		int val = super.pop();
		if(val == this.min()){
			this.minStack.pop();
		}
		
		return val;
	}
	
	public int min() {
		if (this.minStack.isEmpty()) {
			return Integer.MAX_VALUE;
		} else {
			return this.minStack.peek();
		}
	}
}
