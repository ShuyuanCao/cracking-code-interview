package cc150.chapter03;

public class Stack{
	private int capacity;
	
	public Node top, bottom;
	public int size = 0;
	
	public Stack(int capacity){
		this.capacity = capacity;
	}
	
	public boolean isFull(){
		return this.capacity == this.size;
	}
	
	public boolean isEmpty(){ return this.size == 0;}
	
	public boolean push(int val){
		if(size >= capacity) return false;
		size++;
		
		Node n = new Node(val);
		if(size == 1) bottom = n;
		join(n, top);
		top = n;
		
		return true;
	}
	
	public int pop(){
		Node result = top;
		top = top.below;
		size--;
		return result.data;
	}
	
	public int removeBottom(){
		Node b = bottom;
		bottom = bottom.above;
		if(bottom != null) bottom.below = null;
		size--;
		return b.data;
	}
	
	/**
	 * Connection the new node with the top node of a stack
	 * @param above: new node, which will be the top
	 * @param below: the current top node of a stack
	 */
	public void join(Node above, Node below){
		if(below != null) below.above = above;
		if(above != null) above.below = below;
	}
	
}
