package cc150.chapter03;

import java.util.ArrayList;
import java.util.EmptyStackException;

public class StackOfPlates_3_3_S_1 {
	
	private int CAPACITY;
	/**
	 * Use LinkedList to implement each stack in the set of stacks (stacks)
	 */
	private ArrayList<Stack> stacks = new ArrayList<Stack>();
	
	public StackOfPlates_3_3_S_1(int capacity){
		this.CAPACITY = capacity;
	}
	
	/**
	 *  Follow up (Implement popAt(int index)): ----> using Bio-directional LinkedList is much easier.
	 *      Suppose that: in array list 'stacks':
	 *          stack1 (1st Element); stack 2 (2nd Element); stack 3 (3rd Element)...
	 *
	 *      If we pop an element from stack 1, we need to remove the bottom of stack 2
	 *      and push it onto stack 1. We then need to rollover from stack 3 to stack 2,
	 *      stack 4 to stack 3, etc.
	 *
	 * @return
	 */
	public int popAt(int index){
		return leftShift(index, true);
	}
	
	private int leftShift(int index, boolean removeTop) {
		Stack stack = this.stacks.get(index);
		
		int removedVal;
		if(removeTop) removedVal = stack.pop();
		else removedVal = stack.removeBottom();
		if(stack.isEmpty()){
			this.stacks.remove(index);
		}else if(this.stacks.size() > index + 1){ // NOTE: here it is index + 1, because this is 'index' of arraylist
			int v = leftShift(index + 1, false); // remove bottom for following stacks
			stack.push(v);
		}
		
		return removedVal;
	}
	
	
	// implement function pop()
	public int pop(){
		Stack lastStack = this.getLastStack();
		
		if(lastStack == null) throw new EmptyStackException();
		
		int val = lastStack.pop();
		//if the last stack is empty after popping an element,
		//just remove this stack.
		if(lastStack.isEmpty()) this.stacks.remove(this.stacks.size() - 1);
		return val;
	}
	
	// implement function push()
	public void push(int val){
		Stack lastStack = this.getLastStack();

		// set of stacks is empty or lastStack is full
		if(lastStack == null || lastStack.isFull()){
			// set of stacks is empty
			Stack tmp = new Stack(this.CAPACITY);
			tmp.push(val);
			// NOTE: should add tmp not lastStack!!!!!!
			this.stacks.add(tmp);
		}else{
			lastStack.push(val);
		}
		
	}
	
	//get the last empty stack in the set of stacks
	private Stack getLastStack(){
		
		if(this.stacks.isEmpty())return null;
		
		return this.stacks.get(this.stacks.size() - 1);
	}

}
