package cc150.chapter03;

import java.util.Stack;

public class MyQueue_3_4 {

	private Stack<Integer> newestStack, oldestStack;
	
	public MyQueue_3_4(){
		this.newestStack = new Stack<Integer>();
		this.oldestStack = new Stack<Integer>();
	}
	
	public void add(int val){
		this.newestStack.push(val);
	}
	
	public int size(){
		return this.newestStack.size() + this.oldestStack.size();
	}
	
	public int peek(){
		shiftStacks();
		return this.oldestStack.peek();
	}
	
	private void shiftStacks() {
		if(this.oldestStack.isEmpty()){
			while(!this.newestStack.isEmpty()){
				this.oldestStack.push(this.newestStack.pop());
			}
		}
	}
	
	public int remove(){
		shiftStacks();
		return this.oldestStack.pop();
	}

}
