package cc150.chapter03;

public class ThreeInOne_3_1_S_1 {
	
	/**
	 *      values[]: the big array made of 3 stacks.
	 *      size[]: stores the current size for each of the 3 stack.
	 *      stackCapacity: the size for each of the 3 stacks.
	 *      numOfStacks: the number of Stacks, which is 3.
 	 */
	
	private int values[];
	private int size[];
	private int stackCapacity;
	
	/**
	 *  Solution (Fixed Spaces for each Stacks):
	 *      Inside the array, split the array space into 3
	 *      equal spaces. One for each of the 3 stacks.
	 */
	public ThreeInOne_3_1_S_1(int stackCapacity){
		int numOfStacks = 3;
		this.values = new int[stackCapacity * numOfStacks];
		this.stackCapacity = stackCapacity;
		this.size = new int[numOfStacks];
	}
	
	/**
	 *  Push:
	 *      push value on to stack
	 *  @param stackNum: from 0 to 2
	 */
	public void push(int stackNum, int value) throws Exception {
		if(isFull(stackNum)){
			throw new Exception("Full Stack Exception!!!");
		}
		
		int topIndex = getTopIndexOfStack(stackNum);
		
		this.values[topIndex] = value;
		this.size[stackNum]++;
	}
	
	/**
	 * Pop:
	 *      pop the top value
	 * @param stackNum
	 * @return
	 */
	public int pop(int stackNum) throws Exception {
		// check empty or not
		if(isEmpty(stackNum)){
			throw new Exception("Empty Stack Exception!!!");
		}
		
		// should reduce the size by 1, then get top index
		// because we used "return offSet + size;" in getTopIndex function
		this.size[stackNum]--;
		
		int index = getTopIndexOfStack(stackNum);
		int top = this.values[index]; //get top
		this.values[index] = 0; //clear
		
		
		return top;
	}
	
	/**
	 * Peek:
	 *      return the top value in the stack.
	 * @param stackNum
	 * @return
	 * @throws Exception
	 */
	public int peek(int stackNum) throws Exception {
		//check empty
		if(isEmpty(stackNum)){
			throw new Exception("Empty Stack Exception!!!");
		}
		// note the index should - 1
		return this.values[getTopIndexOfStack(stackNum)-1];
	}
	
	private boolean isEmpty(int stackNum) {
		return this.size[stackNum] == 0;
	}
	
	private int getTopIndexOfStack(int stackNum) {
		
		int offSet = stackNum * stackCapacity;
		int size = this.size[stackNum];
		// the index starts from 0
		return offSet + size;
		
	}
	
	private boolean isFull(int stackNum) {
		return this.size[stackNum] >= this.stackCapacity;
	}
	
}
