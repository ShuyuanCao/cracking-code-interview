package cc150.chapter03;


import java.util.Stack;

/**
 * getting fking away "Broke Up"...whatever
 *
 * Biggest values are on the top of the stack.
 * Smallest values are at the bottom of the stack.
 *
 *      2 important points:
 *          1. input stack is empty or not:
 *             if input stack is empty, finished and return the
 *             new stack back.
 *          2. new stack is empty or not
 *              push the element from input stack directly into
 *              new stack.
 *              if the new stack is not empty, just compare the top
 *              elements of these 2 stacks.
 */

public class SortStack_3_5 {
	
	public Stack<Integer> sortStack(Stack<Integer> stack) {
		Stack<Integer> r = new Stack<Integer>();
		
		while (!stack.isEmpty()){
			int tmp = stack.pop();
			while(!r.isEmpty() && r.peek() > tmp){
				stack.push(r.pop());
			}
			r.push(tmp);
		}
		
		return r;
	}
}
