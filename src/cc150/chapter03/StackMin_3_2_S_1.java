package cc150.chapter03;

import java.util.Stack;


public class StackMin_3_2_S_1 extends Stack<NodeWithMin>{
	
	/**
	 * Solution 1:
	 *      Push(5); // stack is {5}, min is 5
	 *      Push(6); // stack is {6, 5}, min is 5
	 *      Push(3); // stack is {3, 6, 5}, min is 3
	 *      Push(7); // stack is {7, 3, 6, 5}, min is 3
	 *      Pop();   // pops 7. stack is {3, 6, 5}, min is 3
	 *      Pop();   // pops 3. stack is {6, 5}, min is 5
	 *
	 *      when the stack goes back to a prior state({6, 5}),
	 *      the min value also goes back to its prior state ({5}).
	 *
	 *      If we kept track of the min at each state, we would be
	 *      able to easily to know the min value. We can do this by
	 *      having each node record with the min beneath itself is.
	 *      Then, to find the min, you just look at what the top element
	 *      thinks is the min.
	 *
	 */
	public void push(int val) {
		super.push(new NodeWithMin(val, Math.min(val, this.min())));
	}
	
	public int min(){
		if(this.isEmpty()){
			return Integer.MAX_VALUE;
		}else{
			return super.peek().min;
		}
	}
}

class NodeWithMin {
	public int value;
	public int min;
	public NodeWithMin(int val, int min){
		this.value = val;
		this.min = min;
	}
}