package cc150.chapter06;

public class EggDropProblem_6_8 {
	
	/**
	 * We may observe that, regardless how we drop Egg1, Egg2 must do a linear
	 * search (from lowest to highest) between the 'breaking floor' and the
	 * next highest non-breaking floor. For example, if Egg1 is dropped from
	 * floor 5 and 10 without breaking, but it breaks when it's dropped from
	 * floor 15, then Egg2 must be dropped, in the worst case, from floor 11,
	 * 12, 13, and 14.
	 *
	 * The Approach:
	 * As a first try, suppose we drop an egg from 10th floor, then the 20th,...
	 *      1) If Egg1 breaks on the first drop (floor 10), then we have at
	 *      most 10 drops total.
	 *      2) If Egg1 breaks on the last drop (floor 100), then we have at most
	 *      19 drops total(floors 10, 20, .., 90, 100, then 91 through 99).
	 *
	 * That's pretty good, but all we've considered is the absolute worst case.
	 * We should do some 'load balancing' to make those two cases more even.
	 *
	 * Our goal is to create a system for dropping Egg1 such that the number of
	 * drops is as consistent as possible, whether Egg1 breaks on the first drop
	 * or the last drop.
	 *
	 * 1. A perfectly load balanced system would be one in which Drops(Egg 1) +
	 * Drop(Egg 2) is always the same, regardless of where Egg 1 breaks.
	 *
	 * 2. For that to be the case, since each drop of Egg 1 takes one more step,
	 * Egg 2 is allowed one fewer step.
	 *
	 * 3. we must reduce the number of steps potentially required by Egg2 by one
	 * drop each time. For example, if egg 1 is dropped on floor 20, and then floor
	 * 30, Egg2 is potentially required to take 9 steps. When we drop Egg1 again,
	 * we must reduce potential Egg2 steps to only 8. That is, we must drop Egg1 at
	 * floor 39.
	 *
	 * 4. Therefore, Egg 1 must start at floor X, then go up by X-1 floors, then
	 * X-2,..., until it gets 100.
	 *
	 * 5. Solve X.
	 *              X + (X - 1) + (X - 2) + (X - 3) + ... + 1 = 100
	 *              X(X + 1)/2 = 100
	 *              X ~ 13.65
	 *
	 * Therefore, we should round X to 14. That is, we go to floor 14, then 27,
	 * then 39, ... This takes 14 steps in the worst case.
	 * As in many other maximizing / minimizing problems, the key in this problem
	 * is 'worst case balancing'.
	 *
	 *
	 * If we want to generalize this code for more building sizes, then we can solve
	 * for x in:
	 *          x(x + 1)/2 = numberOfFloors
	 *      this will involve the quadratic formula.
	 */
	int coutDrops = 0;

	public int findBreakingPoint(int floors){
		int interval = 14;
		int previousFloor = 0;
		int egg1 = interval;
		
		/*drop egg 1 at decreasing intervals*/
		while(!drop(egg1) && egg1 <= floors){
			interval -= 1;
			previousFloor = egg1;
			egg1 += interval;
		}
		
		/* drop egg2 at 1 unit increments */
		int egg2 = previousFloor + 1;
		while (egg2 < egg1 && egg2 <= floors && !drop(egg2)){
			egg2 += 1;
		}
		
		/*if it didn't break, return -1*/
		return egg2 > floors? -1:egg2;
	}
	
	private boolean drop(int egg1) {
		return false;
	}
}
