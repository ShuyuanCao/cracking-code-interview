package cc150.chapter06;

public class Dominos_6_3 {
	
	/**
	 * At first, it seems like this should be possible. It is an
	 * 8*8 board, which has 64 squares, but 2 have been cut off,
	 * so we are down to 62 squares. A set of 31 dominoes should
	 * be able to fit there, right?
	 *
	 * When we try to lay down dominoes on row 1, which only has
	 * 7 squares, we may notice that 1 domino must stretch into
	 * row 2. then, when we try to lay down dominoes onto row 2,
	 * again we need to stretch a domino into row 3.
	 *
	 * For each row we place, we will always have one domino that
	 * needs to poke into the next row. No matter how many times
	 * and ways we try to solve this issue, we won't be able to
	 * successfully lay all the dominoes.
	 *
	 * There's a cleaner, more solid proof for why it won't work,
	 * the checkboard initially has 32 black and 32 white squares.
	 * By removing opposite corners (which must be the same colors),
	 * we are left with 30 of one color and 32 of the other color.
	 * Let say for the sake of argument, that we have 30 black and 32
	 * white squares.
	 *
	 * Each domino we set on the board will always take up one white and
	 * one black square. Therefore, 31 dominoes will take up 31 white
	 * squares and 31 black squares exactly. On this board, however, we
	 * must have 30 black squares and 32 white squares.
	 *
	 * Hence, it is impossible.
	 */

}
