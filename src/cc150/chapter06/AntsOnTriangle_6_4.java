package cc150.chapter06;

public class AntsOnTriangle_6_4 {
	
	/**
	 * The ants will collide if any of them are moving
	 * towards each other. So, the only way that they
	 * won't collide is if they all moving in the same
	 * direction (clockwise or counterclockwise). We
	 * can compute this probability and work backwards from
	 * there.
	 *
	 * Since each ant can move in 2 directions, and there are
	 * 3 ants, the probability is:
	 *
	 *        P(clockwise) = (1/2)^3
	 *        P(counter clockwise) = (1/2)^3
	 *        P(same direction) = (1/2)^3 + (1/2)^3 = 1/4
	 *
	 * The probability of collision is therefore the probability of
	 * the ants not moving in the same direction:
	 *      P(collision) = 1 - P(same direction) = 1 - 1/4 = 3/4
	 *
	 * To generalize this to an n-vertex polygon: there are still only
	 * 2 ways in which the ants can move to avoid collision, but there
	 * are 2^n ways they can move in total. Therefore, in general, probability
	 * of collision is:
	 *      P(clockwise) = (1/2)^n
	 *      P(counter) = (1/2)^n
	 *      P(same direction) = 2(1/2)^n = (1/2)^(n - 1)
	 *      P(collision) = 1 - P(same direction) = 1 - (1/2)^(n - 1)
	 *
	 *
	 */
	
}
