package cc150.chapter06;

public class JugsOfWater_6_5 {
	/**
	 *    If we just play with the jugs, we'll find that
	 *    we can pour water back and forth between them as follows:
	 *          5 quart             3 quart             Action
	 *            5                     0               filled 5-quart jug
	 *            2                     3               filled 3-quart jug with 5-quart's content
	 *            2                     0               Dumped 3-quart
	 *            0                     2               filled 3-quart jug with 5-quart's content
	 *            5                     2               filled 5-quart jug
	 *            4                     3               filled remainder of 3-quart with 5-quart's content
	 *            4                                     Done! We have 4 quarts.
	 *    This question, like many puzzle questions, has a math/computer science root.
	 *    If the two jug sizes relatively prime, you can measure any value between
	 *    one and the sum of the sum of the jug size.
	 *
	 */
}
