package cc150.chapter06;

import java.util.Random;

public class TheApocalyse_6_7 {
	
	/**
	 * The sequence of children will look like one of:
	 * 'G' represents 'girl'; 'B' represents 'boy';
	 *      G;BBG;BBBG;BBBBG; and so on.
	 *
	 * Mathematically:
	 *      We can work out the probability for each gender sequence:
	 *      P(G) = 1/2
	 *      P(BG) = 1/4
	 *      P(BBG) = 1/8
	 *
	 *  Sequence        Number of Boys          Probability         Number of Boys * Probability
	 *  G               0                       1/2                 0
	 *  BG              1                       1/4                 1/4
	 *  BBG             2                       1/8                 2/8
	 *  BBBG            3                       1/16                3/16
	 *  BBBBG           4                       1/32                4/32
	 *  BBBBBG          5                       1/64                5/64
	 *  BBBBBBG         6                       1/128               6/128
	 *
	 *  The expected value of number of boys is the probability of each sequence multiplied
	 *  by the number of boys in that sequence.
	 *
	 *  1/4 = 32/128            4/32 = 16/128
	 *  2/8 = 32/128            5/64 = 10/128
	 *  3/16 = 24/128           6/128 = 6/128
	 *
	 *  (32 + 32 + 24 + 16 + 10 + 6)/128 = 120/128
	 *
	 *  This looks like it is going to inch closer to 128/128 (which is of course 1). This
	 *  looks like intuition is valuable, but it is not exactly a mathematical concept.
	 *
	 *  in fact, we don't really care about the groupings of families because we are concerned
	 *  about the population as a whole. As soon as a child is born, we can just append its
	 *  gender (B or G) to the string.
	 *
	 *  What are the odds of next character being a G? if the odds of having a boy and girl
	 *  is the same, then the odds of next character being a G is 50%. Therefore, roughly half
	 *  of the string should be Gs and half should be Bs, giving an even gender ratio.
	 *
	 *  This actually makes a lot of sense. Biology hasn't been changed. Half of newborn babies
	 *  are girls and half are boys. Abiding by some rule about when to stopping having children
	 *  doesn't change this fact.
	 *
	 *  Therefore, the gender ratio is 50% girls and 50% boys.
	 */
	public double simulation(int n){
		int boys = 0;
		int girls = 0;
		
		for(int i = 0; i < n; i++){
			int[] genders = runOneFamily();
			girls += genders[0];
			boys += genders[1];
		}
		
		return girls/(double)(boys + girls);
	}
	
	private int[] runOneFamily() {
		
		Random random = new Random();
		int boys = 0;
		int girls = 0;
		
		while (girls == 0){ // until we have a girl
			if(random.nextBoolean()){//girl
				girls++;
			}else{//boy
				boys++;
			}
		}
		
		return new int[]{girls, boys};
	}
	
}
