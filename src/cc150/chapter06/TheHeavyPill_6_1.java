package cc150.chapter06;

public class TheHeavyPill_6_1 {
	
	/**
	 * If we took one pill from Bottle #1 and two pill #2, what would the scale
	 * show? It depends. If bottle #1 were the heavy bottle, we would get 3.1
	 * grams. If bottle #2 were the heavy bottle, we would get 3.2 gram. And
	 * that is the trick to this problem.
	 *
	 * We know the "expected" weight of a bunch of pills. The differences between
	 * the expected weight and the actual weight will indicate which bottle
	 * contributed the heavier pills, provided we select a different number of pills
	 * from each bottle.
	 *
	 * We can generalized this to the full solution: take 1 pill from bottle #1,
	 * 2 pills from bottle #2, 3 pills from bottle #3, and so on. Weigh this mix of
	 * pills. If all pills were one gram each, the scale would read 210 grams(1 + 2
	 * + 3 +...+ 20 = 20 * 21/2 = 210). Any "overage" must come from the extra 0.1
	 * gram pills.
	 *
	 * This formula will tell you the bottle number:
	 *              (weight - 210grams)/0.1grams
	 * So, if the set of pills weighed 211.3 grams, then bottle #13 would have the
	 * heavy pills.
	 *
	 */

}
