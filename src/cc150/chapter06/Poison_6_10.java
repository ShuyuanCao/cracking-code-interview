package cc150.chapter06;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Poison_6_10 {
	
	/**
	 * Observe the wording of the problem. Why seven days? why not
	 * have the results just return immediately?
	 *
	 * The fact that there's such a lag between starting a test and
	 * reading the results likely means that we will be doing something else in
	 * the meantime (running additional tests). Let's hold on to that thought,
	 * but start off with a simple approach just to wrap our heads around the
	 * problem.
	 *
	 * Naive Approach (28 days)
	 * A simple approach is to divide the bottles across the 10 test strips,
	 * first in group 100. Then we wait 7 days. When the results come back, we
	 * look for a positive result across the test strips.
	 *
	 *  Note that this approach makes the assumption that there will always be multiple
	 *  test strips at each round. This assumptions is valid for 1000 bottles and 10 test
	 *  strips.
	 */
	public int findPoisonedBottle(ArrayList<Bottle> bottles, ArrayList<TestStrip> strips){
		int today = 0;
		
		while(bottles.size() > 1 && strips.size() > 0){
			/* run tests */
			runTestSet(bottles, strips, today);
			
			/*wait for results*/
			today += TestStrip.DAYS_FOR_RESULT;
			
			/* check results */
			for(TestStrip strip : strips){
				if(strip.isPositiveOnDay(today)){
					bottles = strip.getLastWeeksBottle(today);
					strips.remove(strip);
					break;
				}
			}
		}
		
		if(bottles.size() == 1){
			return bottles.get(0).getId();
		}
		
		return -1;
	}
	
	/* distribute bottles across test strips evenly */
	public void runTestSet(ArrayList<Bottle> bottles, ArrayList<TestStrip> strips, int today) {
		int index = 0;
		for(Bottle bottle : bottles){
			TestStrip strip = strips.get(index);
			strip.addDropOnDay(today, bottle);
			index = (index + 1)%strips.size();
		}
	}
	
	/**
	 * Optimal Approach (7 days)
	 * We can actually optimize this slightly more, to return a result in just seven days.
	 * This is of course the minimum number of days possible.
	 *
	 * Notice what each test strip really means. It's a binary indicator for poisoned or unpoisoned.
	 * Is it possible to map 1000 keys to 10 binary values such that each key is mapped to a
	 * unique configuration of values?  yes, of course. this is what a binary number is.
	 *
	 * We can take each bottle number and look at its binary representation. If there's a 1 in the ith
	 * digit, then we will add a drop of this bottle's contents to test strip i. Observe that 2^10 is
	 * 1024, so 10 test strips will be enough to handle up to 1024 bottles.
	 *
	 * We wait seven days, and then read the results. If test strip 1 is positive, then set bit i of
	 * the result value. Reading all the test strips will give us the ID of the poisoned bottle.
	 *
	 * This approach will work  as long as 2^T >= B, where T is the number of test strips and B is
	 * the number of bottles.
	 */
	public int findPoisonedBottle2(ArrayList<Bottle> bottles, ArrayList<TestStrip> testStrips){
		runTests(bottles, testStrips);
		ArrayList<Integer> positive = getPositiveOnDay(testStrips, 7);
		return setBits(positive);
	}
	
	/*add bottle contents to test strips*/
	private void runTests(ArrayList<Bottle> bottles, ArrayList<TestStrip> testStrips) {
	
		for(Bottle bottle : bottles){
			int id = bottle.getId();
			int bitIndex = 0;
			while(id > 0){
				if((id & 1) == 1){
					testStrips.get(bitIndex).addDropOnDay(0, bottle);
				}
				bitIndex++;
				id >>= 1;
			}
		}
	
	}
	
	/*get test strips that are positive on a particular day.*/
	public ArrayList<Integer> getPositiveOnDay(ArrayList<TestStrip> testStrips, int day){
		ArrayList<Integer> positive = new ArrayList<>();
		
		for(TestStrip testStrip : testStrips){
			int id = testStrip.getId();
			if(testStrip.isPositiveOnDay(day)){
				positive.add(id);
			}
		}
		
		return positive;
	}
	
	/*create number by setting bits with indices specified in positive*/
	public int setBits(ArrayList<Integer> positive){
		int id = 0;
		for (Integer bitIndex : positive){
			id |= 1 << bitIndex;
		}
		return id;
	}
	
}

class TestStrip{
	public static int DAYS_FOR_RESULT = 7;
	private ArrayList<ArrayList<Bottle>> dropsByDay = new ArrayList<ArrayList<Bottle>>();
	private int id;
	
	public TestStrip(int id){ this.id = id; }
	public int getId(){return id;}
	
	/* Resize list of days/drops to be large enough. */
	private void sizeDropsForDay(int day){
		while(dropsByDay.size() <= day){
			dropsByDay.add(new ArrayList<Bottle>());
		}
	}
	
	/* Add drop from bottle on specific day. */
	public void addDropOnDay(int day, Bottle bottle){
		sizeDropsForDay(day);
		ArrayList<Bottle> drops = dropsByDay.get(day);
		drops.add(bottle);
	}
	
	/* checks if any of the bottles in the set are poisoned.*/
	private boolean hasPoison(ArrayList<Bottle> bottles){
		for(Bottle b : bottles){
			if(b.isPoisoned()){
				return true;
			}
		}
		return false;
	}
	
	/* gets bottles used in the test DAYS_FOR_RESULT days ago. */
	public ArrayList<Bottle> getLastWeeksBottle(int day){
		if(day < DAYS_FOR_RESULT){
			return null;
		}
		return dropsByDay.get(day - DAYS_FOR_RESULT);
	}
	
	/* checks for poisoned bottles since before DAYS_FOR_RESULT */
	public boolean isPositiveOnDay(int day){
		int testDay = day - DAYS_FOR_RESULT;
		if(testDay < 0 || testDay >= dropsByDay.size()){
			return false;
		}
		for(int d = 0; d <= testDay; d++){
			ArrayList<Bottle> bottles = dropsByDay.get(d);
			if(hasPoison(bottles)){
				return true;
			}
		}
		return false;
	}
	
}

class Bottle{
	private boolean poisoned = false;
	private int id;
	
	public Bottle(int id){ this.id = id;}
	public int getId(){ return id;}
	public void setAsPoisoned(){ poisoned = true;}
	public boolean isPoisoned(){return poisoned;}
}