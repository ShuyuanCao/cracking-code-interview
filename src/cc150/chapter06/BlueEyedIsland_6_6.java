package cc150.chapter06;

public class BlueEyedIsland_6_6 {
	
	/**
	 *
	 * Let's apply the Base Case and build approach. Assume that there are n people
	 * on the island and c of them have blue eyes. We are explicitly told that c > 0.
	 *
	 * Case c = 1: Exactly one has blue eyes.
	 * Assuming all the people are intelligent, the blue-eyed person should look around
	 * and realize that no one else has blue eyes. Since he knows that at least one
	 * person has blue eyes, he must concludes taht it is him who has blue eyes. Therefore,
	 * he would take the flight that evening.
	 *
	 * Case c = 2: Exactly two people have blue eyes.
	 * The 2 blue-eyed people see each other, but unsure whether c is 1 or 2.  They know,
	 * from the previous case, that if c = 1, the blue-eyed person would leave on the first
	 * night. Therefore, if the other blue eyed person is still there, he must deduce that
	 * c = 2, which means that he himself has bue eyes. Both men would then leave on the
	 * second night.
	 *
	 * Case c > 2: The General Case
	 * As we increase c, we can see that this logic continues apply. If c = 3, then those
	 * 3 people will immediately know that there are either 2 or 3 people with blue eyes.
	 * If there were 2 people, then those two people would left on the second night. So,
	 * when the others are still around after that night, each person would conclude that
	 * c = 3 and that they have blue eyes too. They would leave that night.
	 *
	 * This same pattern extends up through any value of C. Therefore, if c men have blue eyes,
	 * it will take c nights for blue-eyed men to leave. All will leave on the same night.
	 *
	 */

}
