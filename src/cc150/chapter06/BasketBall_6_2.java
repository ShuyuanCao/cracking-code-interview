package cc150.chapter06;

public class BasketBall_6_2 {
	
	/**
	 * To solve this problem we can apply straightforward probability
	 * laws by comparing the probabilities of winning each game.
	 *
	 *  Probability of winning game 1:
	 *      The probability of winning game 1 is p;
	 *
	 *  Probability of winning game 2:
	 *      Let s(k, n) be the probability of making exactly k shots
	 *      out of n. The probability of winning game 2 is the probability
	 *      of exactly making 2 shots out of 3 or making all 3 shots.
	 *
	 *      P(winning) = s(2, 3) + s(3, 3)
	 *      s(3, 3) = p^3
	 *
	 *      P(making 1 and 2, and missing 3)
	 *          + P(making 1 and 3, missing 2)
	 *          + P(making 2 and 3, missing 1)
	 *       = p * p * (1 - p) + p * (1 - p) * p + p * p * (1 - p)
	 *       = 3(1-p)*P^3
	 *
	 *     Adding these together:
	 *          = p^3 + 3(1-p)p^2
	 *          = p^3 + 3p^2 - 3p^3
	 *          = 3p^2 - 2p^3
	 *
	 *  Which game should you play?
	 *      You should play game 1 if P(Game 1) > P(Game 2)?
	 *      p > 3p^2 - 2p^3
	 *      1 > 3p - 2p
	 *      2p^2 - 3p + 1 > 0
	 *      (2p - 1)(p - 1) > 0
	 *
	 *      Both terms must be positive, or both must be completely negative.
	 *      But we know p < 1, so p - 1 < 0. This means both terms must be negative.
	 *      2p - 1 < 0
	 *      2p < 1
	 *      p < 0.5
	 *
	 *      So, we should play game 1 if 0 < p < 0.5 and game 2 if 0.5 < p < 1.
	 *      If p = 0, 0.5, or 1, then P(game 1) = P(game 2), so it doesn't matter
	 *      which game to play.
	 */

}
