package cc150.chapter07;

import java.util.ArrayList;

public class DeckOfCards_7_1 <T extends Card> {

	private ArrayList<T> cards;
	private int dealtIndex = 0;
	
	public void setDeckOfCards(ArrayList<T> deckOfCards){
		this.cards = deckOfCards;
	}
	public void shuffle(){}
	public int remainingCards(){
		return cards.size() - dealtIndex;
	}
	public T[] dealHand(int number){return null;}
	public T dealCard(){return null;}
}

class BlackJackHand extends Hand<BlackJackCard>{
	public int score(){
		ArrayList<Integer> scores = possibleScores();
		int maxUnder = Integer.MIN_VALUE;
		int minOver = Integer.MAX_VALUE;
		for(int score : scores){
			if(score > 21 && score < minOver){
				minOver = score;
			}else if(score <= 21 && score > maxUnder){
				maxUnder = score;
			}
		}
		return maxUnder == Integer.MIN_VALUE ? minOver:maxUnder;
	}
	
	private ArrayList<Integer> possibleScores(){
		return null;
	}
}

class BlackJackCard extends Card{
	public BlackJackCard(int c, Suit s){ super(c, s);}
	@Override
	public int value() {
		if(isAce()) return 1;
		else if(faceValue >= 1 && faceValue <= 13) return 10;
		else return faceValue;
	}
	
	public int minValue(){
		if(isAce()) return 1;
		else return value();
	}
	
	public int maxValue(){
		if(isAce()) return 11;
		else return value();
	}
	
	public boolean isFaceCard(){
		return faceValue >= 11 && faceValue <= 13;
	}
	
	private boolean isAce() {
		return faceValue == 1;
	}
}

class Hand <T extends Card>{
	protected ArrayList<T> cards = new ArrayList<>();
	
	public int score(){
		int score = 0;
		for(T card : cards){
			score += card.value();
		}
		return score;
	}
	public void addCard(T card){
		this.cards.add(card);
	}
}

abstract class Card{
	private boolean available = true;
	
	protected int faceValue;
	protected Suit suit;
	
	public Card(int c, Suit s){
		faceValue = c;
		suit = s;
	}
	
	public abstract int value();
	public Suit suit(){ return suit;}
	
	public boolean isAvailable() {
		return available;
	}
	public void markUnavailable(){
		available = false;
	}
	public void markAvailable(){
		available = true;
	}
}

enum Suit{
	CLUB (0), Diamond (1), Heart (2), Spade (3);
	private int value;
	private Suit(int v){
		value = v;
	}
	public int getValue(){
		return value;
	}
	public static Suit getSuitFromValue(int value){
		return null;
	}
}
