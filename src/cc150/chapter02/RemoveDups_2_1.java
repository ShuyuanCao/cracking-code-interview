package cc150.chapter02;

import java.util.HashSet;

public class RemoveDups_2_1 {

	public static void main(String args[]){
		Node one = new Node(1);
		Node two = new Node(2);
		Node three = new Node(2);
		Node fourth = new Node(2);
		Node fifth = new Node(4);
		one.setNext(two);
		two.setNext(three);
		three.setNext(fourth);
		fourth.setNext(fifth);
		fifth.setNext(null);
		
		RemoveDups_2_1 removeDups = new RemoveDups_2_1();
		//removeDups.removeDups(one);
		removeDups.removeDups_2(one);
		
		// print out the modified linkedlist
		while(one != null){
			System.out.println(one.getData());
			one = one.getNext();
		}
	}
	
	/**
	 * Solution 2:
	 *      If extra buffer is not allowed, we need to use
	 *      2 pointers, the 'current' pointer is iterating
	 *      through all the nodes in the list, and the 'runner'
	 *      pointer is iterating through the following nodes
	 *      after the 'current' pointer.
	 * @param head
	 */
	private void removeDups_2(Node head){
	
		Node current = head;
		while(current != null){
			// important step: assigning current to runner, not
			// current.next to runner
			Node runner = current;
			while(runner.getNext() != null){
				if(runner.getData() == runner.getNext().getData()){
					runner.setNext(runner.getNext().getNext());
				}else{
					runner = runner.getNext();
				}
			}
			current = current.getNext();
		}
	
	}
	
	/**
	 * Solution 1:
	 * If we wanna remove the duplicates from the
	 * linked list, we need to track the duplicate
	 * using extra spaces, like HashSet.
	 * @param head
	 */
	private void removeDups(Node head) {
		HashSet<Integer> values = new HashSet();
		
		Node pre = null;
		while(head != null){
			if(values.contains(head.getData())){
				pre.setNext(head.getNext());
			}else{
				values.add(head.getData());
				pre = head;
			}
			head = head.getNext();
		}
		
	}
	
}
