package cc150.chapter02;

public class DeleteMidNode_2_3 {
	
	/**
	 * In this problem, you are not given access to the head
	 * of the list. You only have access to that node.
	 *
	 * The solution is simply to copy the data from the next
	 * node over to the current node, then to delete the next
	 * Node.
	 *
	 * @param mid
	 */
	private void solution_1(Node mid){
		
		System.out.println(this.deleteMidNode(mid));
		
	}
	private boolean deleteMidNode(Node mid){
		
		if(mid == null || mid.getNext() == null){
			return false;
		}
		
		mid.setData(mid.getNext().getData());
		mid.setNext(mid.getNext().getNext());
		return true;
	}
	
	public static void main(String args[]){
		
		Node one = new Node(1);
		Node two = new Node(2);
		Node three = new Node(2);
		Node fourth = new Node(2);
		Node fifth = new Node(4);
		one.setNext(two);
		two.setNext(three);
		three.setNext(fourth);
		fourth.setNext(fifth);
		fifth.setNext(null);
		
		DeleteMidNode_2_3 deleteMidNode_2_3 = new DeleteMidNode_2_3();
		deleteMidNode_2_3.solution_1(two);
		
		while(one != null){
			System.out.println(one.getData());
			one = one.getNext();
		}
	}
}
