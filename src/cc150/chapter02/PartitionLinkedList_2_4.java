package cc150.chapter02;

public class PartitionLinkedList_2_4 {
	
	/**
	 *  Solution 2:
	 *      If you don't care about making the elements in the list stable,
	 *      we can modifying the list at the head and tail instead of using
	 *      two lists "before" and "after".
	 *
	 *      Use the first Node as the Axis
	 *
	 * @param head
	 * @param partition
	 * @return node
	 */
	public Node partitionList_2(Node head, int partition){
		// we may assign both pointers to point to head of the list
		Node start = head;
		Node tail = head;
		
		while(head != null){
			// we need to store the pointer pointing to next node,
			// cuz we need to move the node to go ahead of start
			// pointer or to the tail of this .
			Node next = head.getNext();
			if(head.getData() < partition){
				//insert the node at head
				head.setNext(start);
				start = head;
			}else{
				// insert the node at tail
				tail.setNext(head);
				tail = head;
			}
			head = next;
		}
		tail.setNext(null);
		return start;
	}
	
	/**
	 * Solution 1:
	 *      we can just create 2 node lists 'before' and 'after'.
	 *      Then, iterate through
	 * @param head
	 * @param partition
	 * @return Node
	 */
	public Node partitionList(Node head, int partition) {
		
		Node before = null;
		Node after = null;
		Node beforeEnd = null;
		Node afterEnd = null;
		
		while(head != null){
			if(head.getData() < partition){
				if(before == null){
					before = head;
					beforeEnd = before;
				}else{
					beforeEnd.setNext(head);
					beforeEnd = head;
				}
			}else{
				if(after == null){
					after = head;
					afterEnd = after;
				}else{
					afterEnd.setNext(head);
					afterEnd = head;
				}
			}
			head = head.getNext();
		}
		
		if(before == null){
			return after;
		}
		//merge the list
		beforeEnd.setNext(after);
		return before;
	}
	
}
