package cc150.chapter02;


import java.util.Stack;

public class Palindrome_2_6 {
	
	class Result{
		public Node node;
		public boolean result;
		
		public Result(Node node, boolean result) {
			this.node = node;
			this.result = result;
		}
	}
	/**
	 * Solution 3: (Recursion)
	 *      isPalindrome: list = 0 ( 1 ( 2 ( 3 ( 4 ) 3 ) 2 ) 1 ) 0. len=9
	 *          isPalindrome: list = 1 ( 2 ( 3 ( 4 ) 3 ) 2 ) 1 ) 0. len=7
	 *              isPalindrome: list = 2 ( 3 ( 4 ) 3 ) 2 ) 1 ) 0. len=5
	 *                  isPalindrome: list = 3 ( 4 ) 3 ) 2 ) 1 ) 0. len=3
	 *                      isPalindrome: list = 4 ) 3 ) 2 ) 1 ) 0. len=1
	 *                      returns node 3b, true   (compare 4f, 4b)(don't need to compare)
	 *                  returns node 2b, true       (compare 3f, 3b)
	 *              returns node 1b, true           (compare 2f, 2b)
	 *          returns node 0b, true               (compare 1f, 1b)
	 *      returns node null, true                 (compare 0f, 0b) and return final result true
	 *
	 *      To generalize, each call compares its head to returned_node, and
	 *      then passes the returned_node.next up the stack. In this way, every
	 *      node i gets compared to node n-i. If at any point the values do
	 *      not match, we return false, and every call up the stack checks that value.
	 *
	 *      Sometimes we said we will return a boolean value, and sometimes we are returning
	 *      a node, which is it?
	 *      It is both. We created a simple class with two members, a boolean and a node
	 *      and return an instance of that class.
	 *
	 * @param list
	 * @return boolean
	 */
	public boolean solution_3(Node list){
		
		int len = getListLength(list);
		Result result = recursion(list, len);
		
		return result.result;
	}
	
	private Result recursion(Node list, int len) {
		
		/*
		*      !!!!!!!!
	    *      when the len of list is Odd number and the len is equal to 1, we should return head.next
	    *      when the len of list is Even number and the len is equal to 0, we should return head
	    *      !!!!!!!!
		*/
		if(list == null || len == 0) {
			return new Result(list, true);
		} else if(len == 1){
			return new Result(list.next, true);
		}
		
		Result retur = recursion(list.next, len - 2);
		// if the child calls are not palindrome
		if(!retur.result || retur.node == null){
			return retur;
		}
		
		// check if the current left side node value matches the one on right side.
		retur.result = (retur.node.data == list.data);
		// return the next node on the right side.
		retur.node = retur.node.next;
		
		return retur;
	}
	
	private int getListLength(Node list) {
		int len = 0;
		while(list != null){
			len ++;
			list = list.next;
		}
		return len;
	}
	
	/**
	 *  Solution_2 (Iteration):
	 *      1. using slow, fast pointers and stack
	 *      2. when slow pointer is moving, put each of the
	 *          node into stack. when faster pointer reached
	 *          the end of the linked list, the slow pointer reached
	 *          to the mid of the list and the first half of the list
	 *          is already put into stack in reversed order.
	 *      3. moving the slow pointer for the rest of the list,
	 *          comparing each of the rest nodes with the node in
	 *          stack.
	 *      4. essentially this is comparing the reversed first half
	 *          of the list with the second half of the list.
	 *
	 * @param list
	 * @return  boolean
	 */
	public boolean solution_2(Node list){
		Node fast = list;
		Node slow = list;
		
		Stack<Integer> stack = new Stack<Integer>();
		
		// we need check fast and fast.next, because when
		// moving the fast pointer, we need to assign 'fast.next.next'
		// to current fast pointer so we have to make sure that
		// fast.next is not null. when fast is null, the fast pointer already
		// reached to the end of the list.
		while(fast != null && fast.next != null){
			stack.push(slow.getData());
			slow = slow.getNext();
			// step length is 2
			fast = fast.getNext().getNext();
		}
		
		// when the above loop terminated, if fast is not null, then this list has
		// odd number of nodes.
		if(fast != null){
			// skip the mid node
			// the value of the mid node is not stored in stack yet.
			slow = slow.getNext();
		}
		
		while(slow != null){
			int val = stack.pop();
			
			if(val != slow.getData()){
				return false;
			}
			
			slow = slow.getNext();
		}
		
		return true;
	}
	
	/**
	 *  Solution 1:
	 *      Our first solution is to reverse the linkedlist
	 *      and compareList the reversed list to the original list.
	 *      if they are the same, the lists are identical.
	 *      We just need to compareList the first half of the linked list.
	 *
	 * @return boolean
	 */
	public boolean solution_1(Node list){
		HeadAndTail reversed = this.reverseLinkList(list);
		reversed.tail.setNext(null);
		
		boolean result = compareList(list, reversed.head);
		
		return result;
	}
	
	private boolean compareList(Node list, Node reversed) {
		
		while(list != null && reversed != null){
			if(list.getData() != reversed.getData()){
				return false;
			}
			list = list.getNext();
			reversed = reversed.getNext();
		}
		
		return list == null && reversed == null;
	}
	
	/**
	 * For the reversed list, we need to keep the head and tail of
	 * it, otherwise we cannot add the new node into the tail of this
	 * reversed list. This is the most important point!!!
	 * @param list
	 * @return Node
	 */
	private HeadAndTail reverseLinkList(Node list) {
		
		if(list == null){
			return null;
		}
		
		HeadAndTail returnedVal = reverseLinkList(list.getNext());
		/**
		 * Here we have to use clone node, because we have keep the
		 * original list as it is in the original state.
		 * Otherwise, when reached to the first node after recursion,
		 * we need to set the next node to be null, which will break
		 * the original state of the linked list.
		 */
		Node clone = new Node(list.getData());
		if(returnedVal == null){
			returnedVal = new HeadAndTail(clone, clone);
		}else{
			returnedVal.tail.setNext(clone);
			returnedVal.tail = clone;
		}
		
		return returnedVal;
	}
	class HeadAndTail{
		public Node head;
		public Node tail;
		
		public HeadAndTail(Node head, Node tail) {
			this.head = head;
			this.tail = tail;
		}
	}
}
