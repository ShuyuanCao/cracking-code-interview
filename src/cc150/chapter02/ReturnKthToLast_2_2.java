package cc150.chapter02;

/**
 * Example:
 *      "1->2->3->4->5"
 *    if k = 3, then the function should return
 *    the head node of the list "3->4->5".
 */

public class ReturnKthToLast_2_2 {
	
	/**
	 * Solution 1:
	 *      Recursion:
	 *      One way to do this is to change the problem to simply
	 *      PRINTING the Kth to last element. Then, we can pass
	 *      back the value of the counter simply through return
	 *      values.
	 * @param one
	 */
	private void returnKthToLast(Node one, int k) {
		this.printKthToLast(one, k);
	}
	
	/**
	 *  whenever you meet a Recursion problem, always think of this problem
	 *  from the deepest node or layer, (e.g., what kind of value it will return
	 *  to the upper layer or loop?).
	 * @param one
	 * @param k
	 * @return
	 */
	private int printKthToLast(Node one, int k) {
		if(one == null){
			return 0;
		}
		int index = printKthToLast(one.getNext(), k) + 1;
		if(index <= k){
			System.out.println(one.getData() + "is the " + index + "th node~~~");
		}
		return index;
	}
	
	/**
	 *  Solution 2:
	 *      We couldn't return a counter and index simultaneously.
	 *      if we can wrap the counter with simple class (or even
	 *      a single element array), we can mimic passing by reference.
	 *
	 *  @param one
	 *  @param k
	 */
	private void solution_2(Node one, int k){
		Index index = new Index();
		Node kth = this.returnKthToLast_2(one, k, index);
		
		//print out node values from kth to last.
		while(kth != null){
			System.out.println(kth.getData());
			kth = kth.getNext();
		}
	}
	
	private Node returnKthToLast_2(Node one, int k, Index index){
		
		if(one == null){
			return null;
		}
		
		Node node = returnKthToLast_2(one.getNext(), k, index);
		// the following line should be after the upper line,
		// because this is recursion, and the last node will
		// be reached first. Then we have to mark the last node as
		// 1.
		index.counter = index.counter + 1;
		
		if(index.counter == k){
			// Note: return the 'current node' one.
			return one;
		}
		// always return the node returned by deepest layer of the recursion
		return node;
	}
	
	class Index{
		public int counter = 0;
	}
	
	/**
	 *
	 * Solution 3(Iteration):
	 *      Suppose the length of the linked list is n.
	 *      Using two pointers p1, p2. Then following the
	 *      steps below:
	 *      1. p1, p2 are all pointing to the head of the linked list.
	 *      2. p1 moves k steps towards to the last node. after this,
	 *          p1 will pointing to (n - k)th node.
	 *      3. iterating through p1 and p2 at same time until p1 reach to
	 *          the end. At this moment, p2 will be pointing to Kth node
	 *          from the last node.
	 *      This solution takes O(n) time, and O(1) space.
	 * @param head
	 * @param k
	 */
	private void solution_3(Node head, int k){
		Node kth = this.iterativeSolution(head, k);
		while(kth != null){
			System.out.println(kth.getData());
			kth = kth.getNext();
		}
	}
	private Node iterativeSolution(Node head, int k){
		Node p1 = head;
		Node p2 = head;
		// Step 2: after the k loops, p1 is pointing to k + 1 node.
		for(int i=1; i<=k; i++){
			if(p1 == null){
				return null; // out of bounds
			}
			p1 = p1.getNext();
		}
		// Step 3: after this loop, p2 will be pointing to Kth node
		// and p1 will pointing to null, which is the end of the list.
		while(p1 != null){
			p1 = p1.getNext();
			p2 = p2.getNext();
		}
		return p2;
	}
	
	public static void main(String args[]){
		
		Node one = new Node(1);
		Node two = new Node(2);
		Node three = new Node(2);
		Node fourth = new Node(2);
		Node fifth = new Node(4);
		one.setNext(two);
		two.setNext(three);
		three.setNext(fourth);
		fourth.setNext(fifth);
		fifth.setNext(null);
		
		ReturnKthToLast_2_2 returnKthToLast_2_2 = new ReturnKthToLast_2_2();
		returnKthToLast_2_2.returnKthToLast(one, 7);
		System.out.println("solution 2 starts~~~~~~~");
		returnKthToLast_2_2.solution_2(one, 3);
		System.out.println("solution 3 starts~~~~~~~");
		returnKthToLast_2_2.solution_3(one, 3);
	}
	
}
