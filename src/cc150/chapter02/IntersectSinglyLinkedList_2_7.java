package cc150.chapter02;

public class IntersectSinglyLinkedList_2_7 {
	class Result{
		Node tailNode;
		int length;
		
		public Result(Node tailNode, int length) {
			this.tailNode = tailNode;
			this.length = length;
		}
	}
	/**
	 * Solution 1:
	 *      1. run through each linked list to ge the lengths
	 *      and the tails.
	 *      2. compare the tails. if they are different(by reference not values),
	 *      return immediately, which means there is no intersection.
	 *      3. set 2 pointers on the start of each linked lists.
	 *      On the longer linked list, move its pointer by the difference in
	 *      length comparing with shorter list.
	 *      4. traverse each linked list at the same time until the reference values
	 *      are the same.
	 *
	 * @param lit1
	 * @param lit2
	 * @return
	 */
	
	public Node solution(Node lit1, Node lit2) {

		if(lit1 == null || lit2 == null)return null;
		
		// get tail and length
		Result result_1 = getTailAndLen(lit1);
		Result result_2 = getTailAndLen(lit2);
		
		// if the references are different, there is no intersection
		if(result_1.tailNode != result_2.tailNode){
			return null;
		}
		
		// set the shorter or longer pointer to each of the linked lists
		Node shorter = result_1.length > result_2.length?lit2:lit1;
		Node longer = result_1.length > result_2.length?lit1:lit2;
		
		// for the longer list, move the pointer by difference in lengths.
		longer = movePointer(longer, Math.abs(result_1.length - result_2.length));
		
		while(shorter != longer){
			shorter = shorter.next;
			longer = longer.next;
		}
		
		return shorter;
	}
	
	private Node movePointer(Node longer, int diff) {
		
		Node head = longer;
		while(diff > 0 && head != null){
			head = head.getNext();
			diff--;
		}
		
		return head;
	}
	
	private Result getTailAndLen(Node list) {
		
		// len starts from 1, because while loop is checking
		// list.next
		int len = 1;
		while(list.next != null){
			len ++;
			list = list.getNext();
		}
		
		return new Result(list, len);
	}
}
