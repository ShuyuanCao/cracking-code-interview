package cc150.chapter02;

public class SumList_2_5 {
	
	/**
	 * Follow up:
	 *      1. one list maybe shorter than the other, and we
	 *      cannot handle this "on the fly".Suppose we are adding
	 *      "1->2->3" and "5->6". We need to know that "5" should be
	 *      matched to 2 not 1. We can accomplish this by comparing
	 *      the lengths of the lists in the beginning and padding the shorter
	 *      list with zeros.
	 *      2. The recursive call must return the result, as before, as well
	 *      as the carry. We can solve this issue by creating a wrapper class.
	 */
	class PartialResult{
		public int carry = 0;
		public Node result = null;
	}
	public Node follow_up(Node list1, Node list2){
		
		// get the length of each list
		int len1 = calLength(list1);
		int len2 = calLength(list2);
		
		// compare the length of these two lists
		// and padding the shorter list with 0s
		if(len1 < len2){
			list1 = padList(list1, len2 - len1);
		}else{
			list2 = padList(list2, len1 - len2);
		}
		
		// we just need to pass two list as params to the
		// recursion function, because the return value of
		// this recursion function is an object containing
		// two values.
		PartialResult result = recursionCall(list1, list2);
		
		// if the current carry value is 1, add one more node
		// to the head of list.
		if(result.carry == 1){
			return insertBefore(result.result, result.carry);
		}else{
			return result.result;
		}
	}
	
	private Node insertBefore(Node result, int carry) {
		Node node = new Node(carry);
		node.setNext(result);
		result = node;
		
		return result;
	}
	
	private PartialResult recursionCall(Node list1, Node list2) {
		
		if(list1 == null && list2 == null){
			PartialResult partialResult = new PartialResult();
			return  partialResult;
		}
		
		PartialResult returnValue = recursionCall(list1.getNext(), list2.getNext());
		int value = list1.getData() + list2.getData() + returnValue.carry;
		
		Node newList = insertBefore(returnValue.result, value%10);
		returnValue.carry = value/10;
		returnValue.result = newList;
		
		return returnValue;
	}
	
	private Node padList(Node list2, int i) {
		
		for(int a=1; a<=i; a++){
			Node tmp = new Node(0);
			tmp.setNext(list2);
			list2 = tmp;
		}
		
		return list2;
	}
	
	private int calLength(Node list) {
		if(list == null){
			return 0;
		}
		int counter = 0;
		while(list != null){
			counter++;
			list = list.getNext();
		}
		return counter;
	}
	
	/**
	 * Solution 1:
	 *      using recursion.
	 *      the 2 lists as input and carry as another param.
	 *
	 * Note(!!!!!!!!!!):
	 *      when implementing the code, we must be careful to
	 *      handle the condition when one linkedlist is shorter
	 *      than another. we don't want to get null pointer exception.
	 * @return
	 */
	public Node solution_1(Node list1, Node list2, int carry){
		// termination condition
		// the following condition satisfies the problem
		// that two lists have different lengths
		if(list1 == null || list2 == null || carry == 0){
			return null;
		}
		
		int value = 0;
		if(list1 != null){
			value += list1.getData();
		}
		if(list2 != null){
			value += list2.getData();
		}
		value += carry;
		Node newList = new Node(value%10);
		
		//recursion call
		if(list1 != null || list2 != null){
			Node next = solution_1(list1==null?null:list1.getNext(), list2==null?null:list2.getNext(), value>=10?1:0);
			newList.setNext(next);
		}
		
		return newList;
	}
}
