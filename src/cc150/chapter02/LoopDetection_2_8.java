package cc150.chapter02;

public class LoopDetection_2_8 {
	
	/**
	 * Solution1:
	 *      When SlowRunner enters the looped portion after k steps,
	 *      FastRunner has taken 2k steps total and must be 2k-k, k steps
	 *      into the looped portion. Since k might be much larger than the
	 *      loop length we should actually write this as mod(k, LOOP_SIZE),
	 *      which we will denote as K.
	 *
	 *      At each subsequent step, FastRunner and SlowRunner get either one
	 *      step farther away or one step closer, depending on your perspective.
	 *      Because we are in a circle, when A moves q steps away from B, it is
	 *      always moving q steps closer to B.
	 *
	 *      1. SlowRunner is 0 steps into the loop.
	 *      2. FastRunner is K steps into the loop.
	 *
	 *      3. SlowRunner is K steps Behind the FastRunner.
	 *      4. FastRunner is LOOP_SIZE - K steps behind SlowRunner.
	 *
	 *      5. FastRunner catches up to SlowRunner at a rate of 1 step per unit
	 *      of time.
	 *
	 *      There are 2 TRIPS inside this problem:
	 *          1) before the slow runner get into the loop, until it reach
	 *          to the starting node of the loop.
	 *
	 *          2) after getting into the loop, there will be another trip, which
	 *          is different from the first trip above.
	 *
	 *      So, when do they meet? if FastRunner is LOOP_SIZE - K steps behind SlowRunner,
	 *      and FastRunner catches up at the rate of 1 step per unit of time, then they
	 *      meet at LOOP_SIZE - K steps. At this point, they will be K steps before the head
	 *      of the loop. (why K steps before the head?  (K = LOOP_SIZE - (LOOP_SIZE - K)) )
	 *
	 * Algorithm:
	 *      1. Create 2 pointers: SlowPointer and FastPointer.
	 *      2. Move FastPointer at a rate of 2 steps and SlowPointer at a rate of 1 step.
	 *      3. When they collide, move SlowPointer to LinkedListHead. Keep FastPointer where it is.
	 *      4. Move SlowPointer and FastPointer at a rate of 1 step. Return the new collision point.
	 *
	 * @param list
	 * @return
	 */
	public Node solution(Node list) {
		
		Node slow = list;
		Node fast = list;
		
		// find the meeting point
		while(fast != null && fast.next != null){
			slow = slow.next;
			fast = fast.next.next;
			if(slow == fast){
				break;
			}
		}
		
		// if fast == null or fast.next == nul, there is no loop in the linked list
		if(fast == null || fast.next == null) return null;
		
		// move slow to head of list
		slow = list;
		// move slow and fast at the same speed at rate of 1 step per time
		while(slow != fast){
			slow = slow.next;
			fast = fast.next;
		}
		// both slow and fast are pointing to the start of the loop
		return fast;
	}
	
}
