package cc150.chapter04;

import java.util.ArrayList;
import java.util.Stack;

public class BuildOrder_4_7 {
	
	/**
	 *                  f                   d
	 *                / | \                 |
	 *               <  |  >                <
	 *              c   |   b               g
	 *               \  |  /| \
	 *                > a < |  \>h
	 *                   \  |
	 *                    > e
	 *
	 *  Solution 2: using Depth-First Search
	 *      Alternatively, we can use depth-first search to build the path.
	 *
	 *      Suppose we picked an arbitrary node (say b) and performs a depth first search
	 *      on it. When we get to the end of the path and cannot go any further (which will
	 *      happen at h and e), we know that those terminating nodes can be the last projects
	 *      to be built. No projects depend on them.
	 *
	 *      DFS(b)                              // step 1
	 *          DFS(h)                          // step 2
	 *              build order = ..., h        // step 3
	 *          DFS(a)                          // step 4
	 *              DFS(e)                      // step 5
	 *                  build order = ...,e,h   // step 6
	 *              build order = ..., a, e,h   // step 7
	 *          DFS(e) -> return                // step 8
	 *          build order = ..., b, a, e, h   // step 9
	 *
	 *
	 *      Now consider what happens at node a when we return from the DFS of e.
	 *      All of its dependencies has been built, which means that we are free to
	 *      build a. And once we've built a, we know that all of b's dependencies
	 *      have been built. We can now build b.
	 *
	 *      Now we can start with any old node again, doing a DFS on it and then adding
	 *      the node to the front of the build queue when the DFS is completed.
	 *
	 *      DFS(d)
	 *          DFS(g)
	 *              build order = ..., g, b, a, e, h
	 *          build order = ..., d, g, b, a, e, h
	 *
	 *      DFS(f)
	 *          DFS(c)
	 *              build order = ..., c, d, g, b, a, e, h
	 *          build order = f, c, d, g, b, a, e, h
	 *
	 *      In an algorithm like this, we should think about the issue of cycles.
	 *      There is no possible build order if there is a cycle. But still we don't
	 *      want to get stuck in an infinite loop because there is no possible solutions.
	 *
	 *      A cycle will happen, when a DFS on a node, we run back into the same path.
	 *      what we need is a signal that indicates 'I'm still processing the node, so
	 *      if you see the node again, we have a problem.'
	 *
	 *      What we can do for this is mark each node as a 'partial' or 'is visiting' state
	 *      before we start the DFS on it. If we see any node whose state is partial, then we
	 *      know we have a problem. when we have done with this node's DFS, we need to
	 *      update the state.
	 *
	 *      we also need a state to indicate "I have already processed/built this node", so we
	 *      don't rebuild the node. Our state therefore can have 3 options: COMPLETED, PARTIAL,
	 *      BLANK.
	 *
	 *
	 *      Like solution 1, this solution is O(P+D) time, where P is the number of projects
	 *      and D is the number of dependency pair.
	 *
	 *      This problem is called topological sort: linearly ordering the vertices in a graph
	 *      such that for every edge (a, b), a appears before b in the linear order.
	 *
	 * @param projects
	 * @param dependencies
	 * @return
	 */
	public Stack<Project> findBuildOrderS2(String[] projects, String[][] dependencies){
	
		// same function as before buildGraph();
		NewGraph newGraph = buildGraph(projects, dependencies);
		return orderProjectsS2(newGraph.getNodes());
		
	}
	
	private Stack<Project> orderProjectsS2(ArrayList<Project> nodes) {
		
		Stack<Project> stack = new Stack<Project>();
		
		for(Project project : nodes){
			if(project.getState() == Project.State.BLANK){
				if(!doDFS(project, stack)){
					return null;
				}
			}
		}
		
		return stack;
	}
	
	private boolean doDFS(Project project, Stack<Project> stack) {
		
		if(project.getState() == Project.State.PARTIAL){
			return false;//cycle
		}
		
		if(project.getState() == Project.State.BLANK){
			project.setState(Project.State.PARTIAL);
			ArrayList<Project> children = project.getChildren();
			
			for(Project child : children){
				if(!doDFS(child, stack)){
					return false;
				}
			}
			
			project.setState(Project.State.COMPLETE);
			stack.push(project);
		}
		
		return true;
	}
	
	/**
	 *
	 * Solution 1:
	 *
	 *  This solution takes O(P + D) time, where P is the number of projects
	 *  and D is the number of dependency pair.
	 *
	 * @param projects
	 * @param dependencies
	 * @return
	 */
	public Project[] findBuildOrder(String[] projects, String[][] dependencies){
		
		NewGraph newGraph = buildGraph(projects, dependencies);
		
		return orderProjects(newGraph.getNodes());
	}
	
	/**
	 *
	 * Return a list of the projects in a correct build order.
	 *
	 * @param nodes
	 * @return Project[]
	 */
	private Project[] orderProjects(ArrayList<Project> nodes) {
		
		Project[] order = new Project[nodes.size()];
		
		/*add roots to build order first*/
		int endOfList = addNonDependent(order, nodes, 0);
		
		int toBeProcessed = 0;
		while(toBeProcessed < order.length){
			Project current = order[toBeProcessed];
			
			/*
			*   we have a circular dependency since there are no
			*   remaining projects with zero dependencies
			* */
			if(current == null){
				return null;
			}
			
			/*remove myself as a dependency*/
			ArrayList<Project> children = current.getChildren();
			for(Project child : children){
				child.decreaseDependencies();
			}
			
			/*add children that have no one depending on them*/
			endOfList = addNonDependent(order, children, endOfList);
			toBeProcessed++;
		}
		
		return order;
	}
	
	private int addNonDependent(Project[] order, ArrayList<Project> children, int offset) {
		
		for(Project project : order){
			if(project.getDependencies() == 0){
				order[offset] = project;
				offset++;
			}
		}
		
		return offset;
	}
	
	/**
	 *  Build the graph, adding the edge (a, b) if b is dependent on a.
	 *  Assume a pair is listed in 'build order'. The pair (a, b) in
	 *  dependencies indicates that b depends on a and a must be built
	 *  before b.
	 *
	 * @param projects
	 * @param dependencies
	 * @return NewGraph
	 */
	private NewGraph buildGraph(String[] projects, String[][] dependencies) {
	
		NewGraph newGraph = new NewGraph();
		
		for(String project : projects){
			newGraph.getOrCreateNode(project);
		}
		
		for(String[] dependency : dependencies){
			String first = dependency[0];
			String second = dependency[1];
			newGraph.addEdge(first, second);
		}
	
		return newGraph;
	}

	
}
