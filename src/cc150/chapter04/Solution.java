package cc150.chapter04;

public class Solution {
	
	public int solution(int[] A) {
		// write your code in Java SE 8
		
		// calculate the difference between each pair of the consecutive positions
		// e.g. if there are 10 elements in the array A, there are 9 intervals for this array
		int intervals[] = getIntervals(A);
		
		int numOfPeriods = 0;
		// finding the number of periods with same value of intervals.
		for(int first = 0; first < intervals.length; first++){
			for(int second = first + 1; second < intervals.length; second++){
				if(intervals[first] == intervals[second]){
					numOfPeriods++;
				}else{
					break;
				}
			}
		}
		
		if(numOfPeriods > 1000000000) return -1;
		return numOfPeriods;
	}
	
	private int[] getIntervals(int[] a) {
		int[] result = new int[a.length - 1];
		
		for(int i = 1; i < a.length; i++){
			result[i - 1] = a[i] - a[i - 1];
		}
		
		return result;
	}
	
	
	public static void main(String args[]){
		int A[] = {-1, 1, 3, 3, 3, 2, 3, 2, 1, 0};
		
		Solution solution = new Solution();
		System.out.println(solution.solution(A));
	}

}
