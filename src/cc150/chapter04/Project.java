package cc150.chapter04;

import java.util.ArrayList;
import java.util.HashMap;

public class Project {

	public enum State {COMPLETE, PARTIAL, BLANK}
	private State state = State.BLANK;
	
	private ArrayList<Project> children = new ArrayList<Project>();
	private HashMap<String, Project> map = new HashMap<String, Project>();
	private String name;
	private int dependencies=0;
	
	public Project(String name) {
		this.name = name;
	}
	
	public void addNeighbor(Project node){
		if(!map.containsKey(node.getName())){
			children.add(node);
			node.incrementDependencies();
		}
	}
	
	public State getState() {
		return state;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public void incrementDependencies() {
		dependencies++;
	}
	
	public void decreaseDependencies(){
		dependencies--;
	}
	
	public ArrayList<Project> getChildren() {
		return children;
	}
	
	public void setChildren(ArrayList<Project> children) {
		this.children = children;
	}
	
	public HashMap<String, Project> getMap() {
		return map;
	}
	
	public void setMap(HashMap<String, Project> map) {
		this.map = map;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getDependencies() {
		return dependencies;
	}
	
	public void setDependencies(int dependencies) {
		this.dependencies = dependencies;
	}
}
