package cc150.chapter04;

import java.util.LinkedList;

public class RouteBetweenNode_4_1 {
	
	/**
	 *      Using Breadth-First Search.
	 * @param g
	 * @param start
	 * @param end
	 * @return
	 */
	public boolean findRoute(Graph g, Node start, Node end){
	
		if(start == end){ return true;}
		
		// use queue to solve BFS
		LinkedList<Node> q = new LinkedList<Node>();
		
		for(Node u : g.getNodes()){
			u.state = State.Unvisited;
		}
	
		start.state = State.Visiting;
		q.add(start);
		
		Node u;
		while(!q.isEmpty()){
			u = q.removeFirst(); // dequeue()
			
			if(u != null){
				for(Node v : u.getAjacent()){
					if(v.state == State.Unvisited){
						if(v == end){
							return true;
						}else {
							v.state = State.Visiting;
							q.add(v);
						}
					}
				}
				
				u.state = State.Visited;
			}
		}
		
		return false;
	}

}
