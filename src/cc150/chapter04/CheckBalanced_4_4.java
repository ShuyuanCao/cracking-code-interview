package cc150.chapter04;

public class CheckBalanced_4_4 {
	
	/**
	 * A balanced tree is defined to a tree such that the heights
	 * of the two subtrees of any node never differ by more than one.
	 *
	 * Solution1:
	 *      we can implement a solution based on the above definition.
	 *      we can simply recurse through the entire tree, and for each
	 *      node, compute the heights of each subtree.
	 */
	
	public boolean isBinaryTreeBalanced(BinaryTreeNode root){
		
		if(root == null) return true;
		
		int heightDiff = Math.abs(getHeight(root.left) - getHeight(root.right));
		
		if(heightDiff > 1){
			return false;
		}else{
			// recursion to subtrees
			return isBinaryTreeBalanced(root.left) && isBinaryTreeBalanced(root.right);
		}
	}
	
	private int getHeight(BinaryTreeNode root) {
		
		if(root == null) return -1;
		
		return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
	}
	
	/**
	 *  Solution:
	 *      the first solution is not efficient. For each node of the binary
	 *      tree, we have to call getHeight function, which will go through
	 *      each of the subtree node and this will result in duplicate calculations.
	 *
	 *      the time complexity should be O(Nlog(N)), since each node is touched
	 *      once per node above it.
	 *
	 *      S2:
	 *      the getHeight method can check if the tree is balanced as the same time as
	 *      it is checking heights. when discover that the subtree is not balanced,
	 *      just return error code.
	 *
	 *      Note:
	 *      the height of a null tree is generally defined to be -1, so we can use
	 *      Integer.MIN_VALUE as the error code.
	 *
	 *      Complexity:
	 *          O(N) time and O(H), where H is the height
	 *
	 * @param root
	 * @return boolean
	 */
	public boolean isBinaryTreeBalancedS2(BinaryTreeNode root){
		return getHeightS2(root) != Integer.MIN_VALUE;
	}
	
	private int getHeightS2(BinaryTreeNode root) {
	
		if(root == null){
			return -1;
		}
		
		//pass the error code up
		int heightLeft = getHeightS2(root.left);
		if(heightLeft == Integer.MIN_VALUE) return Integer.MIN_VALUE;
		
		//pass the error code up
		int heightRight = getHeightS2(root.right);
		if(heightRight == Integer.MIN_VALUE) return Integer.MIN_VALUE;
		
		if(Math.abs(heightLeft - heightRight) > 1){
			// return error code back
			return Integer.MIN_VALUE;
		}else{
			return Math.max(heightLeft, heightRight);
		}
	}
}
