package cc150.chapter04;


import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 *                  50
 *                /    \
 *               20     60
 *              /  \      \
 *            10   25     70
 *          /   \       /   \
 *         5    15     65   80
 *
 *         The ordering of items in a binary search tree: Given a node
 *         all nodes on its left must be less than all nodes on its right.
 *         Once we reach a place without a node, we insert the new value there.
 *
 *         what this means is that the very first element in our array must
 *         have been a 50 in order to create the above tree. If it were anything else,
 *         then that value would have been the value instead.
 *         The order of the left or right items doesn't matter.
 *
 *         Once the 50 is inserted, all items less than 50 will be routed to the left
 *         and all items greater than 50 will be routed to the right. The 60 or the 20
 *         could be inserted first, and it wouldn't matter.
 *
 *         Let's think about this problem recursively. If we had all the arrays that could
 *         have created the subtree rooted at 20 (call this arraySet20), and all arrays
 *         that could have created the subtree rooted at 60 (call this arraySet20), how
 *         would that give us the full answer? We could just "weave" each array from
 *         arraySet20 with each array from arraySet60 and then prepend each array with a
 *         50.
 *
 *         Here's what we mean by weaving. We are merging 2 arrays in all possible ways,
 *         while keeping the elements within each array in the same relative order.
 *         array1: {1, 2}
 *         array2: {3, 4}
 *         weave:  {1, 2, 3, 4}, {1, 3, 2, 4}, {1, 3, 4, 2},
 *                 {3, 1, 2, 4}, {3，1, 4, 2}, {3, 4, 1, 2}.
 *         Note that as long as there aren't duplicates in the original array sets,
 *         we don't have to worry that weaving will create duplicates.
 *
 *         The last piece to talk about here is how the weaving work. Let's think recursively
 *         about how to weave {1, 2, 3} and {4, 5, 6}. What are the sub-problems?
 *         1) prepend a 1 to all weaves of {2, 3} and {4, 5, 6}.
 *         2) prepend a 4 to all weaves of {1, 2, 3} and {5, 6}.
 *
 *         To implement this, we will store each as linked lists. This will make it easy
 *         to add and remove elements. When we recurse, we will push the prefixed elements
 *         down the recursion. When first or second are empty, we add the remainder to
 *         prefix and store the result.
 *
 *         It works something like this:
 *          weave(first, second, prefix):
 *              weave({1, 2}, {3, 4}, {})
 *                  weave({2}, {3, 4}, {1})
 *                      weave({}, {3, 4}, {1, 2})
 *                          {1, 2, 3, 4}
 *                      weave({2}, {4}, {1, 3})
 *                          weave({}, {4}, {1, 3, 2})
 *                              {1, 3, 2, 4}
 *                          weave({2}, {}, {1, 3, 4})
 *                              {1, 3, 4, 2}
 *
 *                  weave({1, 2}, {4}, {3})
 *                      weave({2}, {4}, {3, 1})
 *                          weave({}, {4}, {3, 1, 2})
 *                              {3, 1, 2, 4}
 *                          weave({2}, {}, {3, 1, 4})
 *                              {3, 1, 4, 2}
 *                      weave({1, 2}, {}, {3, 4})
 *                          {3, 4, 1, 2}
 *
 *
 *         Now let's think through the implementation of removing 1 from {1, 2} and recursing.
 *         We need to be careful about modifying this list, since a latter recursive call(e.g.
 *         weave({1, 2}, {4}, {3}))
 *
 *         We could clone the list when we recurse, so that we only modify the recursive calls.
 *         Or we could modify the list, but then reverse the changes after we are done with recursing.
 *
 *         We've chosen to implement it the latter way. Since we are keeping the same reference
 *         to first, second and prefix the entire way down the recursive call stack, then we will
 *         need to clone prefix just before we store the complete result.
 */


public class BSTSequences_4_9 {

	public ArrayList<LinkedList<Integer>> findAllSequences(BinaryTreeNode root){
		ArrayList<LinkedList<Integer>> result = new ArrayList<LinkedList<Integer>>();
	
		if(root == null){
			result.add(new LinkedList<Integer>());
			return result;
		}
		
		LinkedList<Integer> prefix = new LinkedList<Integer>();
		prefix.add(root.val);
		
		// recurse on right and left subtrees.
		ArrayList<LinkedList<Integer>> leftSeqs = findAllSequences(root.left);
		ArrayList<LinkedList<Integer>> rightSeqs = findAllSequences(root.right);
		
		// weave together each list from the left and right sides
		for (LinkedList<Integer> left : leftSeqs){
			for (LinkedList<Integer> right : rightSeqs){
				ArrayList<LinkedList<Integer>> weaved = new ArrayList<LinkedList<Integer>>();
				weaveLists(left, right, weaved, prefix);
				result.addAll(weaved);
			}
		}
	
		return result;
	}
	
	/**
	 *      Weave lists together in all possible ways.
	 *      This algorithm works by removing the head from one list, recursing, and then
	 *      doing the same thing with the other list.
	 *
	 * @param first
	 * @param second
	 * @param results
	 * @param prefix
	 */
	private void weaveLists(LinkedList<Integer> first, LinkedList<Integer> second,
	                        ArrayList<LinkedList<Integer>> results, LinkedList<Integer> prefix) {
		
		/*
			One list is empty. Add remainder to [a cloned] prefix and store the result
		 */
		if(first.size() == 0 || second.size() == 0){
			LinkedList<Integer> result = (LinkedList<Integer>) prefix.clone();
			result.addAll(first);
			result.addAll(second);
			results.add(result);
			
			return;
		}
		
		/*
			Recurse with head of first added to the prefix. Removing the head will damage
			the first, so we'll need to put it back where we found it afterwards.
		 */
		int headFirst = first.removeFirst();
		prefix.addLast(headFirst);
		weaveLists(first, second, results, prefix);
		prefix.removeLast();
		first.addFirst(headFirst);
		
		/*
			Do the same thing with the second, damaging and then restoring the list.
		 */
		int headSecond = second.removeFirst();
		prefix.addLast(headSecond);
		weaveLists(first, second, results, prefix);
		prefix.removeLast();
		second.addFirst(headSecond);
		
	}
	
}
/**
 *      Some people struggle with this problem because there are 2 different recursive algorithms
 *      that must be designed and implemented. They get confused with how the algorithms should interact
 *      with each other and they try to juggle both in their heads.
 *
 *      If this sounds like you, try this: trust and focus. Trust that one method does the right thing
 *      when implementing an independent method, and focus on the one thing that this independent method
 *      needs to do.
 *
 *      Look at weaveLists. It has a specific job: to weave 2 lists together and return a list of all
 *      possible weaves. The existence of findAllSequences is irrelevant. Focus on the task that weaveLists
 *      has to do and design this algorithm.
 *
 *      As you are implementing findAllSequences (whether you do this before or after weaveLists), trust
 *      that weaveLists will do the right thing. Don't concern yourself with the particulars of how
 *      weaveLists operates while implementing something that is essentially independent. Focus on what you are
 *      doing while you are doing.
 *
 *      In fact, this is a good advice in general when you are confused during whiteboard coding. Have a good
 *      understanding of what a particular function should do("Ok, this function is going to return a list of __").
 *      You should verify that it is really doing what you think. But when you are not dealing that function,
 *      focus on the one you are dealing with and trust that the others do the right thing.
 *
 *      It is often too much to keep the implementations of multiple algorithms straight in your head.
 */
