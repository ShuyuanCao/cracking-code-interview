package cc150.chapter04;


import java.util.HashMap;

/**
 *  Paths with Sums:
 *
 *      You are given a binary tree in which each node contains an integer
 *      value (which might be positive or negative). Design an algorithm
 *      to count the number of paths that sum to a given value.
 *      The path doesn't need to start or end at the root or a leaf, but
 *      it must go downwards (traveling only from parent nodes to child
 *      nodes).
 *
 */
public class PathsWithSum_4_12 {
	
	/**
	 * Let's pick a potential sum--say, 8--and then draw a binary tree based
	 * on this. This tree intentionally has a number of paths with sum.
	 *
	 *                              10
	 *                            /    \
	 *                          5       -3
	 *                        /   \        \
	 *                      3     2        11
	 *                    /   \     \
	 *                   3    -2     1
	 * One option is the brute force approach.
	 *
	 * Solution #1:
	 *      In the brute force approach, we just look at all possible paths.
	 *      To do this, we traverse to each node. At each node, we recursively
	 *      try all paths downwards, tracking the sum as we go. As soon as we hit
	 *      our target sum, we increment the total.
	 *
	 *      Double Recursion
	 *
	 *
	 * What is the time complexity of this algorithm?
	 * Consider that node at depth d will be 'touched' (via countPathsWithSumFromNode)
	 * by d node above it.
	 *
	 * In a balanced binary search tree, d will not be more than approximately logN.
	 * Therefore, we know that with N nodes in the tree, countPathsWithSumFromNode will
	 * be called O(NlogN) times. The runtime is O(NlogN).
	 *
	 * In an unbalanced tree, the runtime could be much worse. Consider a tree that is just
	 * straight line down. At the root, we traverse to N-1 nodes. At the next level (with just
	 * a single node ), we traverse to N - 2 nodes. At the third level, we traverse to
	 * N - 3 nodes, and so on. This leads us to the sum of numbers between 1 and N, which is
	 * O(N^2).
	 *
	 *
	 * @param root
	 * @param targetSum
	 * @return
	 */
	public int bruteForce(BinaryTreeNode root, int targetSum){
		/**
		 * First recursion for recursing through the Binary Tree.
		 */
		if(root == null) return 0;
		
		/* Count paths with sum starting from the root*/
		int pathsFromRoot = countPathsWithSumFromNode(root, targetSum, 0);
		
		/* Try the nodes on the left and right. */
		int pathsFromLeft = bruteForce(root.left, targetSum);
		int pathsFromRiht = bruteForce(root.right, targetSum);
		
		return pathsFromRoot + pathsFromLeft + pathsFromRiht;
	}
	
	/* Returns the number of paths with this sum starting from this node. */
	private int countPathsWithSumFromNode(BinaryTreeNode node, int targetSum, int currentSum) {
		
		/**
		 * Second recursion for counting the number of valid paths.
		 */
		
		if(node == null) return 0;
		
		currentSum += node.val;
		
		int totalPaths = 0;
		if(currentSum == targetSum){ // found a path from the root
			totalPaths++;
		}
		
		totalPaths += countPathsWithSumFromNode(node.left, targetSum, currentSum);
		totalPaths += countPathsWithSumFromNode(node.right, targetSum, currentSum);
		
		return totalPaths;
	}
	
	/**
	 * Solution 2:
	 *      In analyzing the last solution, we may realize that we repeat some work.
	 *      For a path such as, 10 -> 5 -> 3 -> -2, we traverse this path repeatedly.
	 *      We do it when start with node 10, then when we go to node 5 (looking at 5,
	 *      then 3, -2), then when we go to node 3, and then finally when we go to node
	 *      -2. Ideally, we'd like to re-use this work.
	 *
	 *                                  10
	 *                                /    \
	 *                              5       -3
	 *                            /   \        \
	 *                          3     1        11
	 *                        /   \     \
	 *                       3    -2     2
	 *
	 *      Let's isolate a given path and treat it just as an array. Consider a (hypothetical,
	 *      extended) path like:
	 *          10 -> 5 -> 1 -> 2 -> -1 -> -1 -> 7 -> 1 -> 2.
	 *
	 *      What we're really saying then is: How many contiguous sequences in this array sum to
	 *      a target sum such as 8? In other words, for each y, we are trying to find the x values
	 *      below. (or, more accurately, the number of x values below.)
	 *
	 *                                      targetSum
	 *                                     |         |
	 *                     ------------------------------
	 *                     ^               ^         ^
	 *                     |               |         |
	 *                     s               x         y
	 *
	 *      If each value knows its running sum (the sum of values from s through itself), then
	 *      we can find this pretty easily. We just need to leverage this simple equation:
	 *
	 *                      runningSum_x = runningSum_y - targetSum.
	 *
	 *      We then look for the values of x where this is true.
	 *
	 *                                  runningSum_y
	 *                         |                            |
	 *                           runningSum_x     targetSum
	 *                       |                 ||             |
	 *                      ------------------------------------
	 *                       ^                 ^              ^
	 *                       |                 |              |
	 *                       s                 x              y
	 *
	 *      Since we just looking for the number of paths, we can use a hash table. As we iterate
	 *      through the array, build a hash table that maps from a runningSum to the number of times
	 *      we have seen that sum. Then, for each y, look up runningSum_y - targetSum in the hash
	 *      table. The value in the hash table will tell you the number of paths with sum targetSum
	 *      that end at y.
	 *
	 *      For example,
	 *          index:   0    1    2    3    4     5     6     7     8
	 *          value:  10 -> 5 -> 1 -> 2 -> -1 ->-1  -> 7  -> 1 ->  2
	 *          sum:    10   15   16   18    17   16     23   24   26
	 *
	 *      The value of running sum is 24. If targetSum is 8, then we would look up 16 in the hash
	 *      table. This would have a value of 2 (originating from index 2 and index 5). As we can
	 *      see above, indexes 3 through 7 and indexes 6 through 7 have sums of 8.
	 *
	 *      Now that we have settled the algorithm for an array, let's review this on a tree.
	 *      We take a similar approach.
	 *
	 *      We traverse through the tree using depth-first search. As we visit each node:
	 *          1) Track its runningSum. We'll take this in as a parameter and immediately increment
	 *              it by node.val.
	 *          2) Look up runningSum - targetSum in the hash table. The value there indicates the total
	 *              number. Set totalPaths to this value.
	 *          3) If runningSum == targetSum, then there's one additional path that starts the root.
	 *              Increment totalPaths.
	 *          4) Add runningSum to the hash table (incrementing the value if it's already there).
	 *          5) Recurse left and right, counting the number of paths with sum targetSum.
	 *          6) After we are done recursing left and right, decrement the value of runningSum in the
	 *              hash table. This is essentially backing out of our work; it reverses the changes
	 *              to the hash table so that other nodes don't use it(since we are now done with node).
	 *
	 *      Despite the complexity of deriving this algorithm, the code to implement this is relatively
	 *      simple.
	 *
	 */
	public int countPathsWithSumS2(BinaryTreeNode root, int targetSum){
		return countPathsWithSum(root, targetSum, 0, new HashMap<Integer, Integer>());
	}
	
	public int countPathsWithSum(BinaryTreeNode node, int targetSum, int runningSum,
	                             HashMap<Integer, Integer> pathCount){
		if(node == null) return 0; //base case
		
		/* count paths with sum ending at the current node. */
		runningSum += node.val;
		int sum = runningSum - targetSum;
		int totalPaths = pathCount.getOrDefault(sum, 0);
		
		/*If runningSum equals targetSum, then one additional path starts at root.
		* add in this paths*/
		if(runningSum == targetSum){
			totalPaths++;
		}
		
		/*increment pathCount, recurse, then decrement pathCount*/
		incrementHashTable(pathCount, runningSum, 1); // increment pathCount
		totalPaths += countPathsWithSum(node.left, targetSum, runningSum, pathCount);
		totalPaths += countPathsWithSum(node.right, targetSum, runningSum, pathCount);
		incrementHashTable(pathCount, runningSum, -1); // decrement pathCount
	
		return totalPaths;
	}
	
	private void incrementHashTable(HashMap<Integer, Integer> hashTable, int key, int newCount) {
		if(newCount == 0){
			hashTable.remove(key);
		}else{
			hashTable.put(key, newCount);
		}
	}
	/**
	 * The runtime for this algorithm is O(N), where N is the number of nodes in the tree.
	 * We know it is O(N), because we travel to each node just once, doing O(1) work each time.
	 * In a balanced tree, the space complexity is O(log N) due to the hash table. The
	 * space complexity can grow to O(n) in an unbalanced tree.
	 */
	
}
