package cc150.chapter04;

public class BinaryTreeNode {
	int val;
	BinaryTreeNode left;
	BinaryTreeNode right;
	BinaryTreeNode parent;
	int size;
	
	public BinaryTreeNode(int val) {
		this.val = val;
	}
}
