package cc150.chapter04;


/**
 *
 *  Definition of Binary Search Tree:
 *          the condition is that all left nodes must be less than
 *          or equal to the current node, which must be less than
 *          all the right nodes.
 *
 *
 *  Thought for solving this problem:
 *  Essentially, when traverse through the binary search tree in
 *  in-order traversal, the elements should be from small values
 *  to big values.
 */
public class ValidateBST_4_5 {
	/**
	 * Do in-order traversal and copy the elements into an array
	 * or arraylist and then check whether the array is ordered or
	 * not.
	 *
	 * Assumption:
	 *      no duplicates in the binary tree.
	 *
	 * @param root
	 * @return
	 */
	private int index = 0;
	public boolean isValidBST(BinaryTreeNode root){
		
		int[] elements = new int[root.size];
		
		copyEleToList(root, elements);
		
		for(int i=0; i < root.size-1; i++){
			if(elements[i] >= elements[i+1]) return false;
		}
	
		return true;
	}
	
	private void copyEleToList(BinaryTreeNode root, int[] elements) {
		if(root == null) return;
		
		// in-order traversal
		copyEleToList(root.left, elements);
		elements[index] = root.val;
		// don't forget to add 1 to index
		index += 1;
		copyEleToList(root.right, elements);
	}
	
	/**
	 * actually the array is not necessary, we never use it other than
	 * to compare an element to the previous element. So why not just
	 * track the last element we saw and compare it as we go.
	 *
	 * still using in-order traversal
	 *
	 * @param root
	 * @return
	 */
	private Integer lastEle = null;
	public boolean isValidBSTS2(BinaryTreeNode root){
		
		if(root == null) return true;
		
		// recurse to left subtree
		if(!isValidBSTS2(root.left)) return false;
		
		// check current root
		if(lastEle != null && root.val < lastEle){ return false;}
		// use laseEve to track the previous value
		lastEle = root.val;
		
		// recurse
		if(!isValidBSTS2(root.right)) return false;
	
		return true;//all good~
	}
	
	/**
	 *
	 *                      20
	 *                10            30
	 *            5         15
	 *       3        7         17
	 *
	 *  For solution 3, we start with a range (min=null, max=null),
	 *  which the root obviously meets(null means that there is no
	 *  min or max). we then branch left, checking that these nodes
	 *  are within the range (min=null, max=20)
	 *
	 *  we proceed through the with this approach. when we branch left,
	 *  the max gets updated. when we branch right, the min gets updated.
	 *  if anything fails these checks, stop and return false.
	 *
	 * @return
	 */
	public boolean isValidBSTS3(BinaryTreeNode root){
		if(root == null) return true;
		
		return checkValidBST(root, null, null); // root, min, max
	}
	
	/**
	 * should correctly handle false cases and true cases in recursion.
	 * @param root
	 * @param min
	 * @param max
	 * @return
	 */
	public boolean checkValidBST(BinaryTreeNode root, Integer min, Integer max){
	
		//check leaf nodes
		if(root == null) return true;
		
		//check current root node
		if((min != null && min > root.val) || (max != null && root.val > max)){
			return false;
		}
		
		// recurse left and right subtree (false case: either one side is false, then should
		// return false), so should use '||'
		if(!checkValidBST(root.left, min, root.val) || !checkValidBST(root.right, root.val, max)){
			return false;
		}
		
		return true;
	}
	
	
	/**
	 *  Using pre-order traversal:(NO)
	 *  this method has bugs
	 *
	 *              2
	 *          1       3
	 *       0     9
	 *
	 * @param root
	 * @return
	 */
	/*
	private boolean checkValidBST(BinaryTreeNode root) {
	
		if(root == null){
			return true;
		}
		if(root.left != null && root.left.val > root.val){
			return false;
		}
		if(root.right != null && root.val > root.right.val){
			return false;
		}
		
		return checkValidBST(root.left) && checkValidBST(root.right);
	}
	*/
}
