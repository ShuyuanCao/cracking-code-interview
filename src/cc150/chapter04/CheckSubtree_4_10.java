package cc150.chapter04;

/**
 *      Question:
 *          Check Subtree: T1 and T2 are 2 very large binary trees, with T1 much bigger than
 *                T2. Create an algorithm to determine if T2 is a subtree of T1.
 *
 *                A tree T2 is a subtree of T1 if there exist a node n in T1 such that the
 *                subtree of n is identical to T2. That is, if you cut off the tree at node
 *                n, the 2 trees would be identical.
 *
 *      In problems like this, it is useful to attempt to solve the problem assuming that there
 *      is just a small amount of data. This will give us a basic idea of an approach that might
 *      work.
 */

public class CheckSubtree_4_10 {
	
	/**
	 *      Simple Approach
	 *          In this smaller, simpler problem, we could consider comparing string representations
	 *          of traversals of each tree. If T2 is a subtree of T1, then T2's traversal should be a
	 *          substring of T1. Is the reverse true? If so, should we use in-order traversal or a
	 *          pre-order traversal?
	 *
	 *          An in-order traversal definitely will not work. Consider a scenario in which we were
	 *          using binary search trees. A binary search tree's in-order traversal always prints out
	 *          the values in sorted order. Therefore, 2 binary search trees with the same values will
	 *          always have the same in-order traversals, even if their structure is different.
	 *
	 *          What about a pre-order traversal? This is a bit more promising. At least in this case
	 *          we know certain things, like the first element in the pre-order traversal is the root
	 *          node. The left and right elements will follow.
	 *
	 *          Unfortunately, trees with different structures could still have the same pre-order traversal.
	 *
	 *                  3           3
	 *                /              \
	 *               4               4
	 *
	 *          There is a simple fix though. We can store NULL nodes in the pre-order traversal string as
	 *          a special character, like 'X'.(We will assume that binary trees contain only integers).
	 *          The left tree would have the traversal {3, 4, X} and the right tree would have the traversal
	 *          {3, X, 4}.
	 *
	 *          Observe that, as long as we present the NULL nodes, the pre-order traversal is unique. That
	 *          is if 2 trees have the same pre-order traversal, then we know they are identical trees in
	 *          values and structure.
	 *
	 *          To see this, consider re-constructing a tree from its pre-order traversal (with NULL node indicated).
	 *          For example, 1, 2, X, X, X, 3, X, X.
	 *
	 *          The root is 1 and its left node, 2, follows it.
	 *          2.left must be 4. 4 must have 2 NULL nodes （since it is followed by 2 Xs）.
	 *          4 is complete, so we move back to its parent 2. 2.right is another X(NULL). 1's left
	 *          subtree is now complete, so we move to 1's right child. We place 3 with 2 NULL children there.
	 *          The tree is now complete.
	 *
	 *                              1
	 *                           /     \
	 *                         2        3
	 *                       /   \     /  \
	 *                     4     X    X    X
	 *                   /  \
	 *                  X   X
	 *
	 *          This whole process was deterministic, as it will be on any other tree. A pre-order traversal always
	 *          starts at the root and, from there, the path we take is defined by the traversal. Therefore, 2 trees
	 *          are identical if they have the same pre-order traversal.
	 *
	 *          Now consider the subtree problem. If T2's pre-order traversal is a substring of T1's pre-order traversal
	 *          then T2's root element must be found in T1. If we do a pre-order traversal from this element
	 *          in T1, we will follow an identical path to T2's traversal. Therefore, T2 is a subtree of T1.
	 *
	 *          This approach takes O(n + m) and O(n + m) space, where n and m are the number of nodes in T1
	 *          and T2, respectively. Given million of nodes, we might want to reduce the space complexity.
	 *
	 * @param t1
	 * @param t2
	 * @return
	 */
	public boolean checkSubtree(BinaryTreeNode t1, BinaryTreeNode t2){
		
		StringBuilder str1 = new StringBuilder();
		StringBuilder str2 = new StringBuilder();
		
		getOrderString(t1, str1);
		getOrderString(t2, str2);
		
		return str1.indexOf(str2.toString()) != -1;
	}
	
	/**
	 *      Pre-order traversal
	 * @param node
	 * @param strB
	 */
	private void getOrderString(BinaryTreeNode node, StringBuilder strB) {
		if(node == null){
			strB.append("X");               // add null indicator
			return;
		}
		
		strB.append(node.val);              // add root
		getOrderString(node.left, strB);    // add left
		getOrderString(node.right, strB);   // add right
	}
	
	/**
	 *      The Alternative Approach
	 *
	 *      An alternative approach is to search through the larger tree, T1.
	 *      Each time a node in T1 matches the root of T2, called matchTree.
	 *      The matchTree method will compare the 2 subtrees to see if they are
	 *      identical.
	 *
	 *      Analyzing the runtime is somewhat complex. A naive answer would be to
	 *      say that it is O(nm) time, where n is the number of nodes in T1 and
	 *      m is the number of nodes in T2. While this is technically correct, a
	 *      little more thought can produce a tighter bound.
	 *
	 *      We do not actually call matchTree on every node in T2. Rather, we call
	 *      it k times, where k is the number of occurrences of T2's root in T1. The
	 *      runtime is closer to O(n + km).
	 *
	 *      In fact, even that overstates the runtime. Even if the root were identical,
	 *      we exit matchTree when we find a difference between T1 and T2. We therefore
	 *      probably do not actually look at m nodes on each call of matchTree.
	 *
	 */
	public boolean checkSubtree2(BinaryTreeNode t1, BinaryTreeNode t2){
		if(t2 == null) return true;     //the empty tree is always a subtree
		return subTree(t1, t2);
	}
	
	private boolean subTree(BinaryTreeNode r1, BinaryTreeNode r2) {
		
		if(r1 == null){
			return false;
		}else if(r1.val == r2.val && matchTree(r1, r2)){
			return true;
		}
		
		return subTree(r1.left, r2) || subTree(r1.right, r2);
	}
	
	private boolean matchTree(BinaryTreeNode r1, BinaryTreeNode r2) {
		
		if(r1 == null && r2 == null){
			return true;            // nothing left in the subtree
		}else if(r1 == null || r2 == null){
			return false;           // exactly tree is empty, therefore trees don't match
		}else if(r1.val != r2.val){
			return false;           // data doesn't match
		}else{
			return matchTree(r1.left, r2.left) && matchTree(r1.right, r2.right);
		}
	}
	
	/**
	 *      When might the simple solution be better, and when might the alternative
	 *      approach be better? This is a great conversation to have with your interviewer.
	 *      Here are few thoughts on that matter:
	 *
	 *      1. The simple solution takes O(n + m) memory. The alternative solution takes
	 *         O(log(n) + log(m)) memory. Remember: memory deal can be a very big deal
	 *         when it comes to scalability.
	 *
	 *      2. The simple solution is O(n + m) time and the alternative solution has a worst
	 *         case time of O(nm). The worst case time can be deceiving; we need to look deeper
	 *         than that.
	 *
	 *      3. A slighter tighter bound on the runtime, as explained earlier, is O(n + km), where
	 *         k is the number of occurrence of T2's root in T1. Let's suppose the the node data
	 *         for T1 and T2 were random numbers between 0 and p. The value of k would be
	 *         approximately n/p. why? Because each of n nodes in T1 has a 1/p chance of equaling
	 *         the root, so approximately n/p nodes in T1 should equal T2.root. So, Let's say
	 *         p = 1000, n = 1000000 and m = 100. We would do somewhere around 1,100,000 node checks
	 *         (1100000 = 1000000 + (100 * 1000000)/1000)
	 *
	 *      4. More complex mathematics and assumptions could get us an even tighter bound.
	 *         We assume in #3 in above that if we call matchTree, we would end up traversing all
	 *         m nodes of T2. It is far more likely, though, that we will find a difference very
	 *         early on in the tree and will then exit early.
	 *
	 *      In summary, the alternative approach is more optimal in terms of space and is likely more
	 *      optimal in terms of time as well. It all depends on what assumptions you make and
	 *      whether you prioritize reducing the average case runtime at the expense of worst case
	 *      runtime. This is an excellent point to make to your interviewer.
	 */
	
}

