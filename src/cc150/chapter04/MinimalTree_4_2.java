package cc150.chapter04;

public class MinimalTree_4_2 {
	
	/**
	 *  To create a tree of minimal height, we need to match the number of nodes
	 *  in the left subtree to the number of nodes in the right subtree as much
	 *  as possible. This means we want the root to be the middle of the array,
	 *  since this would mean that half the elements would be less than the root
	 *  and half would be greater than it.
	 *
	 *  the middle of each subsection of the array becomes the root of the node.
	 *  the left half of the array will become our left subtree, and the right half
	 *  of the array will become the right subtree.
	 *
	 *  one way to implement this is to use a single root.insertNode(int v) method which
	 *  insert the value v through a !!! recursive process that starts with the root node.!!!
	 *  this will indeed construct a tree with minimal height but it will not do so
	 *  very efficiently. Each insertion will require traversing the tree, giving the
	 *  total cost of O(N logN) to the tree.
	 *
	 *  Alternatively, we can cut out the extra traversals by recursively using the
	 *  createMinimalBST() method. this method is passed just a subsection of the array
	 *  and returns the root of a minimal tree for that array.
	 *
	 *  the algorithm is as follow:
	 *  1. insert the middle element of the array into the tree.
	 *  2. insert into the left subtree the left subarray elements.
	 *  3. insert into the right subtree the right subarray elements.
	 *  4. recursion
	 *
	 * @return
	 */
	public BinaryTreeNode createMinimalTree(int array[]){
		return createMinimalBST(array, 0, array.length - 1);
	}
	
	private BinaryTreeNode createMinimalBST(int[] array, int start, int end) {
		
		if(end < start){
			return null;
		}
		int mid = (start + end)/2;
		BinaryTreeNode node = new BinaryTreeNode(array[mid]);
		node.left = createMinimalBST(array, start, mid - 1);
		node.right = createMinimalBST(array, mid + 1, end);
		
		return node;
	}
	
}
