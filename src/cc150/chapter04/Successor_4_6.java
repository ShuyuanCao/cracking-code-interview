package cc150.chapter04;


public class Successor_4_6 {
	
	/**
	 *
	 * Input is a binary search tree.
	 *
	 * if we are already on the far right of the tree, then
	 * there is no in-order successor. we should return null.
	 *
	 */
	/*
	// pseudo code
	Node inorderSuccessor(Node n){
		if(n has right subtree){
			return leftmost child of right subtree
		}else{
			// n has no right subtree, then go up and check
			// whether n's parent is traversed or not.
			while(n is a right child of n.parent){
				//n.parent already visited
				n = n.parent;
			}
			
			//return the one not visited (n is the right child of
			// n.parent)
			return n.parent;
		}
	}
	*/
	public BinaryTreeNode inorderSuccessor(BinaryTreeNode root){
		if(root == null) return null;
		
		/*found right children, and return the left most node of right subtree*/
		if(root.right != null){
			return findLeftMostNode(root.right);
		}else{
		
			BinaryTreeNode current = root;
			BinaryTreeNode parent = root.parent;
			// keep going up until we get left instead of right
			while(parent != null && current != parent.left){
				current = parent;
				parent = parent.parent;
			}
			
			return parent;
		}
		
	}
	
	private BinaryTreeNode findLeftMostNode(BinaryTreeNode root) {
		
		// if we are already on the far right of the tree, then
	    // there is no in-order successor. we should return null.
		if(root == null) return null;
		
		while(root.left != null){
			root = root.left;
		}
		
		return root;
	}
	
	
}
