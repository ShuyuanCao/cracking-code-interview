package cc150.chapter04;

import java.util.ArrayList;
import java.util.HashMap;

public class NewGraph {

	private ArrayList<Project> nodes = new ArrayList<Project>();
	private HashMap<String, Project> map = new HashMap<String, Project>();
	
	public Project getOrCreateNode(String name){
		if(!map.containsKey(name)){
			Project node = new Project(name);
			nodes.add(node);
			map.put(name, node);
		}
		
		return map.get(name);
	}
	
	public void addEdge(String sName, String eName){
		Project start = getOrCreateNode(sName);
		Project end = getOrCreateNode(eName);
		
		start.addNeighbor(end);
	}
	
	public ArrayList<Project> getNodes() {
		return nodes;
	}
}
