package cc150.chapter04;

public class FirstCommonAncestor_4_8 {
	
	public BinaryTreeNode findFirstCommonAnscestorS4(BinaryTreeNode root, BinaryTreeNode p, BinaryTreeNode q){
		return null;
	}
	
	/**
	 *
	 * Solution 3:
	 *      without links to parents
	 *
	 *      alternatively, you could follow a chain in which p and q are on the same side.
	 *      that is, if p and q are both on the left side of the node, branch left to look
	 *      for the common ancestor. if they are both on the right, branch right to look
	 *      for the common ancestor. when p and q are no longer on the same side, you must
	 *      have found the first common ancestor.
	 *
	 * @param root
	 * @param p
	 * @param q
	 * @return BinaryNode
	 */
	public BinaryTreeNode findFirstCommonAncestorS3(BinaryTreeNode root, BinaryTreeNode p, BinaryTreeNode q){
	
		// check whether they are all in the tree or not
		if(!covers(root, p) || !covers(root, q)){
			return null;
		}
		
		return ancestorHelper(root, p, q);
	}
	
	private BinaryTreeNode ancestorHelper(BinaryTreeNode root, BinaryTreeNode p, BinaryTreeNode q) {
		
		if(root == null || root == p || root == q){
			return root;
		}
		
		//check both p and q are on the left side or not
		boolean pIsOnLeft = covers(root.left, p);
		boolean qIsOnLeft = covers(root.left, q);
		
		if(pIsOnLeft != qIsOnLeft){
			// p and q are on different sub side.
			// just return the current root, because this is a binary tree
			return root;
		}
		
		// p, q on same child side.
		BinaryTreeNode childSide = pIsOnLeft?root.left:root.right;
		
		return ancestorHelper(childSide, p, q);
	}
	
	/**
	 *  Solution 2:
	 *      With link to Parent (better for worst-case runtime)
	 *
	 *      Similar to the earlier approach, we could trace p's path upwards and check if
	 *      any of the nodes cover q.
	 *      The first node that covers q (we already know that every node this path
	 *      will cover p) must be the first common ancestor.
	 *
	 *      Observe that we don't need to check the entire subtree. As we move from a node x
	 *      to its parent y, all of nodes under x have already been checked for q. Therefore we
	 *      only need to check the new nodes 'uncovered', which will be the nodes under a's
	 *      sibling.
	 *
	 *                                  20
	 *                                /    \
	 *                              10      30
	 *                            /    \
	 *                          5       15
	 *                        /   \       \
	 *                       3    7       17
	 *
	 *      E.g. suppose we are looking for the first common ancestor of node p = 7 and node
	 *      q = 17. when we go to p.parent (5), we uncover the subtree rooted at 3. We therefore
	 *      need to search this subtree for q.
	 *      Next we go to node 10, uncovered the subtree rooted at 15 and we checked this subtree
	 *      for node 17. there it is.
	 *
	 *      To implement this, we can just traverse up from p, storing parent and the sibling node in
	 *      a variable. (the sibling node is always a child of parent and referred to the newly
	 *      uncovered subtree.) At each iteration, sibling gets set to the old parent's sibling node
	 *      and parent gets set to parent.parent.
	 *
	 *      this solution takes O(t) time, where t is the subtree of the first common ancestor.
	 *      in the worst case, this will be O(n), where n is the number of nodes in the tree.
	 *      we can derive this runtime by noticing that each node in that subtree is searched once.
	 *
	 * @param p
	 * @param q
	 * @return BinaryTreeNode
	 */
	public BinaryTreeNode findFirstCommonAnscestorS2(BinaryTreeNode root, BinaryTreeNode p, BinaryTreeNode q){
		
		// check whether either node is not in the tree, or if one covers the other
		if(!covers(root, p) || !covers(root, q)){
			return null;
		}else if(covers(p, q)){
			return p;
		}else if(covers(q, p)){
			return q;
		}
		
		// traverse up until you find a node that covers q
		BinaryTreeNode sibling = getSibling(p);
		BinaryTreeNode parent = p.parent;
		
		while(!covers(sibling, q)){
			sibling = getSibling(parent);
			parent = parent.parent;
		}
		
		return parent;
	}
	
	private BinaryTreeNode getSibling(BinaryTreeNode node) {
		
		if(node == null || node.parent == null)return null;
		
		BinaryTreeNode parent = node.parent;
		
		return parent.left == node? parent.right:parent.left;
	}
	
	private boolean covers(BinaryTreeNode root, BinaryTreeNode p) {
		
		// termination condition for recursion
		if(root == null) return false;
		if(root == p) return true;
		
		return covers(root.left, p) || covers(root.right, p);
	}
	
	/**
	 * Solution 1:
	 *      Suppose each nod has link to its Parent
	 *
	 *      let us assume that we are looking for the common ancestor of nodes p and q.
	 *      if each node has a link to its parent, we could trace p and q's paths
	 *      up until they intersect. the linkedlist in this case is the path from each
	 *      node up to the root.
	 *
	 *      This approach takes O(d), d is the depth of the deeper node.
	 *
	 * @param p
	 * @param q
	 * @return
	 */
	public BinaryTreeNode findFirstCommonAncestor(BinaryTreeNode p, BinaryTreeNode q){
	
		// get the difference in depths
		int difference = getDepth(p) - getDepth(q);
		
		BinaryTreeNode shallower = difference > 0? q : p;
		BinaryTreeNode deeper = difference > 0? p : q;
		
		// move the deeper node up by difference
		deeper = moveUpByDifference(deeper, Math.abs(difference));
		
		// find the intersecting point of the two paths
		while (shallower != deeper && deeper != null && shallower != null){
			deeper = deeper.parent;
			shallower = shallower.parent;
		}
	
		return deeper == null || shallower == null? null:deeper;
	}
	
	private BinaryTreeNode moveUpByDifference(BinaryTreeNode deeper, int abs) {
		
		while (deeper != null && abs > 0){
			deeper = deeper.parent;
			abs--;
		}
		
		return deeper;
	}
	
	private int getDepth(BinaryTreeNode node) {
		
		int depth = 0;
		
		while(node != null){
			node = node.parent;
			depth++;
		}
		
		return depth;
	}
	
}
