package cc150.chapter04;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListsOfDepth_4_3 {
	
	/**
	 *
	 * Solution 1:
	 *      Pre-order traversal: pass the level + 1 to current node's children(the next recursive call).
	 *
	 */
	public ArrayList<LinkedList<BinaryTreeNode>> preOrderTraversal(BinaryTreeNode root){
		
		ArrayList<LinkedList<BinaryTreeNode>> lists = new ArrayList<LinkedList<BinaryTreeNode>>();
		
		// level starts from 0, which will be used as the index of lists
		createLevelLinkedListPreOrderTraversal(root, lists, 0);
		
		return lists;
	}
	
	private void createLevelLinkedListPreOrderTraversal(BinaryTreeNode root, ArrayList<LinkedList<BinaryTreeNode>> lists, int level) {
	
		if(root == null)return;
		
		LinkedList<BinaryTreeNode> list = null;
		
		// need to check whether the list of this level is existing or not.
		if(lists.size() == level){
			list = new LinkedList<BinaryTreeNode>();
			/**
			 * levels are always traversed in order. so, if this is the first time
			 * we have visited level i, we must have seen levels 0 through i-1, we
			 * can therefore safely add the level at the end.
			 */
			lists.add(list);
		}else{
			list = lists.get(level);
		}
		
		//add the current root node
		list.add(root);
		//start left sub tree
		createLevelLinkedListPreOrderTraversal(root.left, lists, level + 1);
		//start right sub tree
		createLevelLinkedListPreOrderTraversal(root.right, lists, level + 1);
		
	}
	
	/**
	 * Solution 2:
	 *      Breadth First Search.
	 */
	public ArrayList<LinkedList<BinaryTreeNode>> breadthFirstSearch(BinaryTreeNode root){
	
		ArrayList<LinkedList<BinaryTreeNode>> results = new ArrayList<LinkedList<BinaryTreeNode>>();
		
		// just need to keep the previous level and current level
		LinkedList<BinaryTreeNode> current = new LinkedList<BinaryTreeNode>();
		if(root != null){
			current.add(root);
		}
		
		while(current.size() > 0){
			results.add(current);
			// keep the previous level nodes
			LinkedList<BinaryTreeNode> previous = current;
			current = new LinkedList<BinaryTreeNode>();
			
			for(BinaryTreeNode node : previous){
				if(node.left != null){
					current.add(node.left);
				}
				if(node.right != null){
					current.add(node.right);
				}
			}
		}
		
		return results;
	}
	
	
}
