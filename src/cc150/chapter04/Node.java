package cc150.chapter04;

public class Node {

	public String name;
	public State state;
	public Node[] ajacent;
	
	public Node(String name){
		this.name = name;
		this.state = State.Unvisited;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public State getState() {
		return state;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public Node[] getAjacent() {
		return ajacent;
	}
	
	public void setAjacent(Node[] ajacent) {
		this.ajacent = ajacent;
	}
}
