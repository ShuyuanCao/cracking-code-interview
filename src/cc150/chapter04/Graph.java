package cc150.chapter04;

public class Graph {
	public Node[] nodes;
	
	public Graph(Node[] nodes){
		this.nodes = nodes;
	}
	
	public Node[] getNodes() {
		return nodes;
	}
	
	public void setNodes(Node[] nodes) {
		this.nodes = nodes;
	}
}
