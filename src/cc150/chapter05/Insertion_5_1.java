package cc150.chapter05;

/**
 *  Two's Complement and Negative Numbers:
 *      The binary representation of -K (Negative K) as a N-bit number is
 *      concat(1, 2^(N-1) - K):
 *          Let's look at the 4 bit integer -3 as example. If it is a 4 bit
 *          number, we have one bit for the sign and 3 bits for the value.
 *          We want the complement with respect to 2^3, which is 8. The
 *          complement of 3 is (the absolute value of -3) with respect to
 *          8 is 5. 5 in binary is 101. Therefore, -3 in binary as a 4 bit
 *          number is 1101, with the first bit being the sign bit.
 *
 *      Another way to look at this is that we invert the bits in the positive
 *      representation and then add 1. 3 is 011 in binary. Flip the bits to
 *      get 100 and add 1 to get 101, then prepend the sign bit (1) to get 1101.
 *
 *
 *  Arithmetic vs. Logical Right shift
 *      There are 2 types of right shift operators. The arithmetic right shift
 *      essentially divides by 2. The logical right shift does what we would
 *      visually see as shifting the bits. This is best seen on a negative
 *      number.
 *
 *      In a logical right shift, we shift the bits and put a 0 in the most
 *      significant bit. It is indicated with a >>> operator. On a 8 bit integer(
 *      where the sign bit is the most significant bit), this would look like
 *      the image below.
 *
 *              1 0 1 1 0 1 0 1 = -75
 *               \ \ \ \ \ \ \
 *              0 1 0 1 1 0 1 0 = 90
 *
 *      In an arithmetic right shift, we shift values to the right but fill in
 *      the new bits with the value of the sign bit. This has the effect of roughly
 *      dividing by 2. It is indicated by a >> operator.
 *
 *              1 0 1 1 0 1 0 1 = -75
 *               \ \ \ \ \ \ \
 *              1 1 0 1 1 0 1 0 = -38
 */
public class Insertion_5_1 {
	
	/**
	 * This problem can be approached in three key steps:
	 *      1. clear bits j through i in N
	 *      2. shift M so that it lines up with bits j through i
	 *      3. merge M and N
	 *
	 *      The trickiest part is Step 1. How do we clear the bits in N?
	 *      We can do this with a mask. This mask will have all 1s, except
	 *      for 0s in the bits j through i. We create this mask by creating
	 *      the left half of the mask first, and then the right half.
	 *
	 * @param n
	 * @param m
	 * @param i
	 * @param j
	 * @return
	 */
	public int updateBits(int n, int m, int i, int j){
		
		/**
		 * create a mask to clear bits i through j in n. EXAMPLE: i = 2,
		 * j = 4. Result should be 11100011. For simplicity, we'll use just
		 * 8 bits for the example.
		 */
		int allOnes = ~0; // will equal all sequence of all 1s.
		
		// 1s before position j, then 0s.  left = 11100000
		int left = allOnes << (j + 1);
		
		// 1's after position i. right = 00000011
		int right = ((1 << i) - 1);
		
		// All 1s, except for 0s and 1s between i and j. mask = 11100011
		int mask = left | right;
		
		// clear bits i through j then put m in there
		int n_cleared = n & mask; // clear bits j through i
		int m_shifted = m << i;   // move m into correct position
		
		return n_cleared | m_shifted; // OR them, then we're done.
	
	}
	
	/**
	 * What do you think these functions would do on parameters x = -93242 and count = 40?
	 *
	 *      With the logical shift, we would get 0, because we are shifting 0 into most
	 *      significant bit repeatedly.
	 *
	 *      With the arithmetic shift, we would get -1, because we are shifting a one into
	 *      the most significant bit repeatedly. A sequence of all 1s in a signed integer
	 *      represents -1.
	 *
	 */
	int repeatedArimeticShift(int x, int count){
		
		for(int i = 0; i < count; i++){
			x >>= 1; // arithmetic shift by 1
		}
		return x;
	}
	
	int repeatedLogicalShift(int x, int count){
		
		for(int i = 0; i < count; i++){
			x >>>= 1; // Logical shift by 1
		}
		return x;
	}
	
	/**
	 * Get Bit
	 */
	public boolean getBit(int num, int i){
		return ((num & (1 << i)) != 0);
	}
	
	/**
	 * Set Bit
	 */
	public int setBit(int num, int i){
		return num | (1 << i);
	}
	
	/**
	 * Update Bit
	 */
	public int updateBit(int num, int i, boolean bitIs1){
		int value = bitIs1?0:1;
		
		int mask = ~(1 << i);
		
		return (num & mask) | (value << i);
	}
}

