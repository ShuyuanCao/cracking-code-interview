package cc150.chapter05;


/**
 * Solution:
 *      There are a number of ways to approach this problem, including
 *      brute force, using bit manipulation, and using clever arithmetic.
 *      Note that arithmetic approach builds on the bit manipulation approach.
 *      You will want to understand the bit manipulation approach before going
 *      on to the arithmetic one.
 *
 *      Note:
 *          The terminology can be confusing for this problem. We call getNext
 *          the bigger number and getPrev the smaller number.
 *
 *      The Brute Approach:
 *      An easy approach is simply brute force: count the number of 1s in n,
 *      and then increment (or decrement) until you find a number with the same
 *      number of 1s. Easy--but not terribly interesting. Can we do sth a bit
 *      more optimal? Yes!
 *      Let's start with the code for getNext, and then move on to getPrev.
 */
public class NextNumber_5_4 {
	
	/**
	 * Bit Manipulation Approach for Get Next Number
	 *
	 * If we think about what the next number should be, we can observe the following.
	 * Given the number 13948, the binary representation look like:
	 *          1   1  0  1  1  0  0  1  1  1  1  1  0  0
	 *         13  12 11 10  9  8  7  6  5  4  3  2  1  0
	 *
	 * We want to make this number bigger but not too big(but not too big). We also
	 * need to keep the same number of ones.
	 * Observation: Given a number n and 2 bit locations i and j, suppose we flip
	 * bit i from 1 to 0, and bit j from 0 to 1. If i > j, then n will have decreased.
	 * If i < j, then n will have increased.
	 *
	 * We know the following:
	 *      1. if we flip a zero to a one, we must flip a one to zero.
	 *      2. when we do that, the number will be bigger if and only if
	 *          the zero-to-one was to the left of one-to-zero bit.
	 *      3. We want to make the number bigger, but not unnecessarily bigger.
	 *          Therefore, we need to flip the right-most zero which has ones on
	 *          the right of it.
	 *
	 * To put this in a different way, we are flipping the rightmost non-trailing zero.
	 * That is, using the above example, the trailing 0s are in the 0th and 1st spot.
	 * The right-most non-trailing zero is at bit 7.  Let's call this position p.
	 *
	 * Step 1:
	 *      Flip right-most non-trailing 0
	 *
	 *          1   1   0   1   1   0   1   1   1   1   1   1   0   0
	 *         13  12  11  10   9   8   7   6   5   4   3   2   1   0
	 *
	 *     With this in mind, we have increased the size of n. But we also have one
	 *     too many ones and one too few zeros. We'll need to shrink the size of our
	 *     number as much as possible while keeping that in mind.
	 *
	 *     We can shrink the number by rearranging all the bits to the right of bit p
	 *     such that 0s are on the left and the 1s are on the right. As we do this,
	 *     we want to replace one of the 1s with a 0s.
	 *
	 *     A relatively easy way of doing this is to count how many ones are to the
	 *     right of p, clear all the bits from 0 until p, and then add back in c1-1 ones.
	 *     Let c1 be the number of ones to the right of p, and c0 be the number of 0s to
	 *     the right of p.
	 *
	 *     Let's walk through this with an example.
	 *
	 * Step 2:
	 *      Clear bits to the right of p. From before, c0 = 2, c1 = 5, p = 7.
	 *              1   1   0   1   1   0   1   0   0   0   0   0   0   0
	 *             13   12  11  10  9   8   7   6   5   4   3   2   1   0
	 *      To clear these bits, we need to clear a mask that is a sequence of
	 *      1s, followed by p zeros. We can do this as follows:
	 *      a = 1 << p;     // all zeros except for a 1 at position p.
	 *      b = a - 1;      // all zeros followed by p 1s.
	 *      mask = ~b;      // all ones followed by p zeros.
	 *      n = n & mask;   // clears rightmost p bits.
	 *
	 *      Or, we concisely do:
	 *          n &= ~((1 << p) - 1)
	 *
	 *Step 3:
	 *      Add in (c1 - 1) ones.
	 *             1    1   0   1   1   0   1   0   0   0   1   1   1   1
	 *            13   12  11  10   9   8   7   6   5   4   3   2   1   0
	 *
	 *    To insert (c1 - 1) ones on the right, we do the following:
	 *          a = 1 << (c1 - 1);      // 0s with a 1 at position (c1 - 1)
	 *          b = a - 1;              // 0s with 1s at position 0 through c1 - 1
	 *          n = n | b;              // insert 1s at positions 0 through c1 - 1
	 *    Or, more concisely:
	 *          n |= (1 << (c1 - 1)) - 1;
	 *
	 *    We have arrived at the smallest number bigger than n with the same number of ones.
	 *
	 *    The code for getNext is below.
	 */
	public int getNext(int n){
		/* compute c0 and c1 */
		int c = n;
		int c0 = 0;
		int c1 = 0;
		
		while((c & 1) == 0 && (c != 0)){
			c0++;
			c >>= 1;
		}
		
		while((c & 1) != 0 && (c != 0)){
			c1++;
			c >>= 1;
		}
		
		/* Error: if n == 11...1100...00, then there is no bigger number with the same
		  number of 1s.
		    */
		if(c0 + c1 == 31 || c0 + c1 == 0){ // the left-most bit is sign bit, so it is 31
			return -1;
		}
		
		int p = c0 + c1;    //position of right-most non-trailing zero
		
		n |= (1 << p);      // flip the rightmost non-trailing zero
		n &= ~((1 << p) - 1); // clear all bits to the right of p
		n |= (1 << (c1 - 1)) - 1; // insert (c1 - 1) ones on the right
		
		return n;
	}
	
	/**
	 * Bit Manipulation Approach for Get Previous Number(small number)
	 *      To implement getPrevious(), we follow a very similar approach.
	 *      1) compute c0 and c1. Note that c1 is the number of trailing 1s, and
	 *          c0 is the size of the block of 0s immediately to the right of
	 *          the trailing 1s.
	 *      2) flip the right-most non-trailing one to a zero. This will be at
	 *          position p = c1 + c0.
	 *      3) clear all bits to the right of p.
	 *      4) Insert c1 + 1 ones immediately to the right of position p.
	 *
	 *      Note that:
	 *      Step 2 sets bit p to a zero and Step 3 sets bit 0 through p - 1 to a zero.
	 *      We can merge these steps. Let's walk through this with an example.
	 *
	 *      Step 1: (initial number) p = 7, c1 = 2. c0 = 5.
	 *          1   0   0   1   1   1   1   0   0   0   0   0   1   1
	 *          13  12  11  10  9   8   7   6   5   4   3   2   1   0
	 *
	 *      Step 2&3: Clear bits 0 through p
	 *          1   0   0   1   1   1   0   0   0   0   0   0   0   0
	 *          13  12  11  10  9   8   7   6   5   4   3   2   1   0
	 *
	 *      We can do the follows:
	 *      int a = ~0;     // sequence of 1s
	 *      int b = a << (p + 1); //sequence of 1s followed by p+1 0s.
	 *      n &= b;         // clears bit 0 through p
	 *
	 *      Step 4:
	 *          insert c1 + 1 ones immediately to the right of position p.
	 *          1   0   0   1   1   1   0   1   1   1   0   0   0   0
	 *          13  12  11  10  9   8   7   6   5   4   3   2   1   0
	 *
	 *      Note that since p = c1 + c0, the (c1 + 1) ones will be followed by
	 *      (c0 - 1) zeros.
	 *
	 *      We can do this as follows:
	 *          int a = 1 << (c1 + 1);  // 0s with 1 at position (c1 + 1)
	 *          int b = a - 1;          // 0s followed by c1 + 1 ones
	 *          int c = b << (c0 - 1);  // c1 + 1 ones followed by c0 - 1 zeros.
	 *          n |= c;
	 *
	 *
	 * @param n
	 * @return
	 */
	public int getPrevious(int n){
	
		int tmp = n;
		int c0 = 0;
		int c1 = 0;
		
		while((tmp & 1) == 1){
			c1++;
			tmp >>= 1;
		}
		
		if(tmp == 0) return -1;
		
		while(((tmp & 1) == 0) && (tmp != 0)){
			c0++;
			tmp >>= 1;
		}
		
		int p = c0 + c1;    // position of the rightmost non-trailing one
		n &= ((~0) << (p + 1));            // clears from bit p onwards
		
		int mask = (1 << (c1 + 1)) -1;      //sequence of (c1 + 1) ones
		n |= mask << (c0 - 1);
		
		return n;
	}
	
	/**
	 * Arithmetic Approach to Get Next Number
	 *      if c0 is the number of trailing zeros, c1 is the size of the one block
	 *      immediately following, and p = c0 + c1, we can word our solution from
	 *      earlier as follows:
	 *      n += 2^c0 - 1;  //sets trailing 0s to 1, giving us p trailing 1s.
	 *      n += 1;         // flip first p from 1s 0s, and puts a 1 at bit p.
	 *
	 *      n += 2^(c1-1)-1;    // sets trailing c1 - 1 zeros to ones.
	 *
	 *      This math reduces to:
	 *      next = n + (2^c0 - 1) + 1 + (2^(c1-1)-1)
	 *          = n + 2^c0 + 2^(c1-1) - 1
	 *
	 */
	public int getNextArith(int n){
		
		/* compute c0 and c1 */
		int c = n;
		int c0 = 0;
		int c1 = 0;
		
		while((c & 1) == 0 && (c != 0)){
			c0++;
			c >>= 1;
		}
		
		while((c & 1) != 0 && (c != 0)){
			c1++;
			c >>= 1;
		}
		
		return n + (1 << c0) + (1 << (c1 - 1)) - 1;
	}
	
	/**
	 *
	 *  Assume n = 10000011:
	 *      c1 = 2; c0 = 5;
	 *
	 *      n -= 2^(c1) - 1;    // removes trailing 1s. n is now 10000000.
	 *      n -= 1;             // flip trailing 0s. n is now 01111111.
	 *      n -= 2^(c0-1) -1;   // flip last (c0 - 1) 0s. n is now 01110000.
	 *
	 *      next = n - (2^(c1) - 1) - 1 - (2^(c0-1) -1)
	 *              = n - 2^(c1) - 2^(c0-1) + 1
	 *
	 *
	 *
	 *
	 * @param n
	 * @return
	 */
	public int getPrevArith(int n){
		
		int tmp = n;
		int c0 = 0;
		int c1 = 0;
		
		while((tmp & 1) == 1){
			c1++;
			tmp >>= 1;
		}
		
		if(tmp == 0) return -1;
		
		while(((tmp & 1) == 0) && (tmp != 0)){
			c0++;
			tmp >>= 1;
		}
		
		return n - (1 << c1) - (1 << (c0 - 1)) + 1;
	}

}
