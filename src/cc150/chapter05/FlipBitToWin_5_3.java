package cc150.chapter05;

import java.util.ArrayList;

public class FlipBitToWin_5_3 {
	
	/**
	 *  We can think about each integer as an alternating
	 *  sequence sequence 0s and 1s. Whenever a 0s sequence has length one,
	 *  we can potentially merge the adjacent 1s sequence.
	 *
	 *  Brute Force:
	 *      One approach is to convert an integer into an array that reflects
	 *      the lengths of the 0s and 1s sequences. For example, 11011101111
	 *      would be (reading from right to left)[0(0), 4(1), 1(0), 3(1), 1(0),
	 *      2(1), 21(0)]. The num in the parentheses reflects whether the integer
	 *      corresponds to a 0s sequence or a 1s sequence, but the actual solution
	 *      doesn't need this. It's a strictly alternating sequence, always starting
	 *      with 0s sequence.
	 *
	 *      Once we have this, we just walk through the array. At each 0s sequence,
	 *      then we consider merging the adjacent 1s sequences if 0s sequence has length
	 *      1.
	 *
	 *      This is pretty good. It is O(b) time O(b) memory, where b is the length of
	 *      the sequence.
	 *
	 *      Note: Be careful with how you express the runtime. Fox example, if you say
	 *      runtime is O(n), what is n? it is not correct to day that this algorithm is
	 *      O(value of the integer). This algorithm is O(num of bits). For this reason,
	 *      when you have potential ambiguity in what n might mean, it is best just not
	 *      to use n. Then neither you nor your interviewer will be confused. Pick a
	 *      different variable name. We used 'b', for the number of bits. Something
	 *      logical works well.
	 *
	 */
	public int longestSequence(int n){
		
		if(n == -1) return Integer.BYTES * 8;
		
		ArrayList<Integer> sequences = getAlternatingSequences(n);
		
		return findLongestSequence(sequences);
	}
	
	/**
	 * Return a list of the sizes of the sequences.
	 * The sequence starts off with number of 0s (which might be 0)
	 * and then alternates with the counts of each value.
	 * @param n
	 * @return
	 */
	private ArrayList<Integer> getAlternatingSequences(int n) {
		
		ArrayList<Integer> sequences = new ArrayList<>();
		
		int searchingFor = 0;
		int counter = 0;
		
		for(int i = 0; i < Integer.BYTES * 8; i++){
			if((n & 1) != searchingFor){
				sequences.add(counter);
				searchingFor = n & 1; // keep the previous bit
				counter = 0;
			}
			counter++;
			n >>>= 1;
		}
		sequences.add(counter);
		
		return sequences;
	}
	
	/**
	 * Given the lengths of alternating sequences of 0s and 1s, find the
	 * longest one we can build.
	 * @param sequences
	 * @return
	 */
	private int findLongestSequence(ArrayList<Integer> sequences) {
		
		int maxSeq = 1;
		
		for(int i = 0; i < sequences.size(); i += 2){
			int zeroSeq = sequences.get(i);
			int onesSeqRight = i - 1 >= 0 ? sequences.get(i - 1):0;
			int onesSeqLeft = i + 1 < sequences.size() ? sequences.get(i + 1) : 0;
			
			int thisSeq = 0;
			if(zeroSeq == 1){ // can merge
				thisSeq = onesSeqLeft + 1 + onesSeqRight;
			}else if(zeroSeq > 1){ // just add a zero to either side
				thisSeq = 1 + Math.max(onesSeqLeft, onesSeqRight);
			}else if(zeroSeq == 0){ // no zero but take either side
				thisSeq = Math.max(onesSeqLeft, onesSeqRight);
			}
			
			maxSeq = Math.max(maxSeq, thisSeq);
		}
		
		return maxSeq;
	}
	
	/**
	 * Solution 2:
	 *      Recall the concept of Best Conceivable Runtime. The B.C.R for this algorithm is
	 *      O(b)(since we will always have to read through the sequence), so we know we
	 *      cannot optimize the time. We can, however, reduce the memory usage.
	 *
	 *      Optimal Algorithm:
	 *      To reduce the space usage, note that we don't need to hang on to the length of each
	 *      sequence the entire time. We only need it long enough to compare each 1s sequence to
	 *      the immediately preceding 1s sequence.
	 *
	 *      Therefore, we can just walk through the integer doing this, tracking the current 1s
	 *      sequence length and the previous ones sequence length. When we see a zero, update
	 *      previousLength:
	 *          1) if the next bit is 1, previousLength should be set to currentLength.
	 *          2) if the next bit is 0, then we cannot merge these sequences together. So, set
	 *          previousLength to 0.
	 *
	 *      Update maxLength as we go.
	 */
	public int flipBit(int a){
		/* if all 1s, this is already the longest sequence */
		if(~a == 0) return Integer.BYTES * 8;
		
		int currentLength = 0;
		int previousLength = 0;
		int maxLength = 1; // we can always have a sequence of at least one 1.
		
		while(a != 0){
			if((a & 1) == 1){ // current bit is 1
				currentLength++;
			}else if((a & 1) == 0){ //current bit is 0
				// update to 0 (if next bit is 0) or currentLength (if next bit is 1)
				previousLength = (a & 2) == 0? 0 : currentLength;
				currentLength = 0;
			}
			
			maxLength = Math.max(previousLength + currentLength + 1, maxLength);
			a >>>= 1;
		}
		return maxLength;
	}
}
