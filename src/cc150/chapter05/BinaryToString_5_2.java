package cc150.chapter05;

public class BinaryToString_5_2 {
	
	/**
	 *  Binary To String:
	 *      Given a real number between 0 and 1 (e.g., 0.72) that is passed in
	 *      as a double, print the binary representation. If the number cannot
	 *      be represented accurately in binary with at most 32 characters,
	 *      print 'ERROR'.
	 *
	 *
	 *  Solution:
	 *      Note: when otherwise ambiguous, we will use the subscripts x2 and
	 *      x10 to indicate whether x is in base 2 or base 10.
	 *
	 *      First, let's start off by asking ourselves what a non-integer
	 *      number in binary looks like. By analogy to a decimal number,
	 *      the binary number 0.101(2) would look like:
	 *
	 *      To print decimal part, we can multiply by 2 and check if 2n is
	 *      greater than or equal to 1. This is essentially "shifting" the
	 *      fractional sum. That is:
	 *          r = 2(10) * 10
	 *            = 2(10) * 0.101(2)
	 *            = 1.01(2)
	 *      If r >= 1, then we know that n had a 1 right after the decimal point.
	 *      By doing this continuously, we can check every digit.
	 *
	 *
	 */
	public String printBinaryStr(double num){
	
		if(num >= 1 || num <= 0){
			return "ERROR";
		}
		
		StringBuilder binary = new StringBuilder();
		binary.append('.');
		
		while(num > 0){
			/* setting a limit on length: 32 characters */
			if(binary.length() >= 32){
				return "ERROR";
			}
			
			double r = num * 2;
			if(r >= 1){
				binary.append(1);
				num = r - 1;
			}else{
				binary.append(0);
				num = r;
			}
		}
	
		return binary.toString();
	}
	
	/**
	 *
	 *      Solution 2:
	 *             Alternatively, rather than multiply the number by 2 and
	 *             comparing to 1, we can compare the number to .5 then .25
	 *             The code below demonstrates this approach.
	 *
	 * @param num
	 * @return
	 */
	public String printBinaryStrS2(double num){
		if(num >= 1 || num <= 0){
			return "ERROR";
		}
		
		StringBuilder binary = new StringBuilder();
		double frac = 0.5;
		
		binary.append(".");
		while (num > 0){
			/* setting a limit on length: 32 characters */
			if(binary.length() > 32){
				return "ERROR";
			}
			
			if(num >= frac){
				binary.append(1);
				num -= frac;
			}else{
				binary.append(0);
			}
			frac /= 2;
		}
		
		return binary.toString();
	}

}
