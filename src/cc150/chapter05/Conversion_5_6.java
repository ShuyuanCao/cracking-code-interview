package cc150.chapter05;


/**
 * Conversion:
 *      Write a function to determine the number of bits you would need to
 *      flip to convert Integer A to Integer B.
 *
 * Example:
 *      Input: 29 (or: 11101), 15 (or: 01111)
 *      Output: 2
 */
public class Conversion_5_6 {
	
	/**
	 * Simple Solution:
	 *      with an XOR
	 *
	 *      Each bit in the XOR represents a bit that is different between A
	 *      and B. Therefore, to check the number of bits that are different
	 *      between A and B, we simply need to count the number of bits in
	 *      A^B that are 1.
	 */
	public int bitSwapRequired(int a, int b){
		int count = 0;
		for (int xor = a ^ b; xor != 0; xor = xor >> 1){
			count += (xor & 1);
		}
		
		return count;
	}
	
	/**
	 * Rather simply shifting c repeatedly while checking the least significant
	 * bit, we can continuously flip the least significant bit and count how long
	 * it takes c to reach 0. The operation c = c & (c - 1) will clear the least
	 * significant bit.
	 * @param a
	 * @param b
	 * @return
	 */
	public int bitSwapRequiredS2(int a, int b){
		int count = 0;
		for(int xor = a ^ b; xor != 0; xor = xor & (xor - 1)){
			count++;
		}
		
		return count;
	}

}
