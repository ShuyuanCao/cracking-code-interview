package cc150.chapter05;

public class DrawLine_5_8 {
	
	/**
	 * A naive solution to the problem is straightforward: iterate in a for loop
	 * from x1 to x2, setting each pixel along the way. But that's hardly any fun,
	 * is it? (Nor it is very efficient.)
	 *
	 * A better solution is to recognize that if x1 and x2 are far from each other,
	 * several full bytes will be contained between them. These full bytes can be
	 * set one at a time by doing screen[byte_pos] = 0xFF. The residual start and
	 * end of the line can be set using masks.
	 *
	 * @param screen
	 * @param width
	 * @param x1
	 * @param x2
	 * @param y
	 */
	public void drawLine(byte[] screen, int width, int x1, int x2, int y){
	
		int startOffset = x1 % 8;
		int firstFullByte = x1 / 8;
		if(startOffset != 0){
			firstFullByte++;
		}
		
		int endOffset = x1 % 8;
		int endFullByte = x2 / 8;
		if(endOffset != 7){
			endFullByte--;
		}
	
		// set full bytes
		for(int b = firstFullByte; b <= endFullByte; b++){
			screen[(width/8)*y + b] = (byte) 0xFF;
		}
		
		// create masks for start and end of line
		byte startMask = (byte) (0xFF >> startOffset);
		byte endMask = (byte) ~(0xFF >> endOffset + 1);
		
		// set start and end of line
		if((x1 / 8) == (x2 / 8)){ //x1 and x2 are in the same byte
			byte mask = (byte) (startMask & endMask);
			screen[(width/8)*y + (x1 / 8)] |= mask;
		}else{
			if(startOffset != 0){
				int byteNumber = (width / 8)*y + firstFullByte - 1;
				screen[byteNumber] |= startMask;
			}
			if(endOffset != 7){
				int byteNumber = (width / 8)*y + endFullByte + 1;
				screen[byteNumber] |= endMask;
			}
		}
	}

}
