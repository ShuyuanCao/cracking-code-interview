package cc150.chapter05;

public class PairwiseSwap_5_7 {
	
	/**
	 * PairWise Swap:
	 *      Write a program to swap odd and even bits in an integer with as few
	 *      instructions as possible.(e.g. bit 0 and 1 are swapped, bit 2 and 3
	 *      are swapped, and so on.)
	 *
	 *
	 * Solution:
	 *      We can approach this as operating on the odds bits first, and then
	 *      the even bits. Can we take a number n and move the odd bits over by
	 *      1? Sure. We can mask all odd bits with 10101010 in binary (which is
	 *      0xAA), then shift them right by 1 to put them in the even spots.
	 *      For the even bits, we do an equivalent operation. Finally, we merge
	 *      these 2 values.
	 *
	 *      This takes a total of 5 instructions. The code below implements this
	 *      approach.
	 *
	 * @param i
	 * @return
	 */
	public int swapOddEvenBits(int i) {
		return (((i & 0xaaaaaaaa)) >>> 1 | (i & 0x55555555) << 1);
	}
}
