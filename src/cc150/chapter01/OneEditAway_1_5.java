package cc150.chapter01;


/**
 * Created by Steven on 9/10/17.
 */
public class OneEditAway_1_5 {

    /**
     * Method 2:
     *      the method checkReplacement does nothing other than flag the
     *      difference, whereas checkInsertion increments the pointer to
     *      the longer string. we can handle both of these in the same
     *      one method.
     * @param tmp1
     * @param tmp2
     * @return boolean
     */
    private boolean checkOneEditAway2(String tmp1, String tmp2){
        if(Math.abs(tmp1.length() - tmp2.length()) > 1){
            return false;
        }

        //get shorter string and longer string
        String shorter = tmp1.length() > tmp2.length()?tmp2:tmp1;
        String longer = tmp1.length() > tmp2.length()?tmp1:tmp2;

        int indexS = 0;
        int indexL = 0;
        boolean foundDifference = false;
        while(indexS < shorter.length() && indexL < longer.length()){

            if(shorter.charAt(indexS) != longer.charAt(indexL)){
                //make sure this is the first difference found
                if(foundDifference)return false;
                if(shorter.length() == longer.length()){
                    //move shorter string
                    indexS++;
                }
                foundDifference = true;
            }else{
                //if they are the same characters, increase the pointer for short string
                indexS++;
            }

            indexL++;
        }

        return true;
    }

    /**
     * Method 1:
     *      We don't need to check the strings for insertion, removal
     *      and replacement edits. The lengths of the strings will indicate
     *      which of these you need to check.
     * @param tmp1, tmp2
     * @return boolean
     */
    private boolean checkOneEditAway(String tmp1, String tmp2){

        if(tmp1.length() == tmp2.length()){
            return checkReplacement(tmp1, tmp2);
        }else if(tmp1.length() - 1 == tmp2.length()){
            return checkInsertion(tmp2, tmp1);
        }else if(tmp1.length() + 1 == tmp2.length()){
            return checkInsertion(tmp1, tmp2);
        }
        return false;

    }

    /**
     * Check if you can insert a character into tmp1 to make tmp2.
     * Essentially, insertion is the same with removal
     * @param tmp1
     * @param tmp2
     * @return boolean
     */
    private boolean checkInsertion(String tmp1, String tmp2) {
        int index1 = 0;
        int index2 = 0;

        while(index1<tmp1.length() && index2<tmp2.length()){
            if(tmp1.charAt(index1) != tmp2.charAt(index2)){
                if(index1 != index2){
                    return false;
                }
                index2++;
            }else{
                index1++;
                index2++;
            }
        }

        return true;
    }

    /**
     * assumption:
     *      those two strings should not be the same.
     * @param tmp1
     * @param tmp2
     * @return
     */
    private boolean checkReplacement(String tmp1, String tmp2) {
        boolean foundDifference = false;
        for(int i=0; i<tmp1.length();i++){
            if(tmp1.charAt(i) != tmp2.charAt(i)){
                if(foundDifference){
                    return false;
                }
                foundDifference = true;
            }
        }
        return foundDifference;
    }

    public static void main(String args[]){

        OneEditAway_1_5 oneEditAway = new OneEditAway_1_5();
        //check insertion
        System.out.println("-----------check insertion-----------");
        String str1 = "hell";
        String str2 = "hello";
        System.out.println(oneEditAway.checkOneEditAway(str1, str2));
        System.out.println(oneEditAway.checkOneEditAway2(str1, str2));

        System.out.println("-----------check replacement-----------");
        str1 = "hello";
        str2 = "fello";
        System.out.println(oneEditAway.checkOneEditAway(str1, str2));
        System.out.println(oneEditAway.checkOneEditAway2(str1, str2));

        System.out.println("-----------check removal-----------");
        str1 = "hello";
        str2 = "hell";
        System.out.println(oneEditAway.checkOneEditAway(str1, str2));
        System.out.println(oneEditAway.checkOneEditAway2(str1, str2));
    }
}
