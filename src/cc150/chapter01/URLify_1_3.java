package cc150.chapter01;

/**
 * Created by Steven on 9/3/17.
 */
public class URLify_1_3 {

    /**
     * In this method, we do two scans:
     *  one scan is 
     * @param tmp
     * @param len
     * @return
     */
    private String transform(char[] tmp, int len){
        int counter_space = 0;
        for(int i=0;i < len;i++){
            if(tmp[i] == ' '){
                counter_space++;
            }
        }
        int new_end = len + counter_space * 2 - 1;
        for(int i = len - 1; i >= 0; i--){
            if(tmp[i] == ' '){
                tmp[new_end] = '0';
                tmp[new_end - 1] = '2';
                tmp[new_end - 2] = '%';
                new_end -= 3;
            }else{
                tmp[new_end] = tmp[i];
                new_end -= 1;
            }
        }

        return new String(tmp);
    }

    public static void main(String args[]){
        String tmp = "hello world w";
        char[] array = {'h','e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', ' ', 'w',' ', ' ', ' ', ' '};
        URLify_1_3 urLify_1_3 = new URLify_1_3();
        System.out.println(urLify_1_3.transform(array, tmp.length()));
    }
}
