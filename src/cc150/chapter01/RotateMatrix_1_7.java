package cc150.chapter01;

/**
 * Created by Steven on 9/13/17.
 */
public class RotateMatrix_1_7 {

    /**
     *  we should move the element from the outside layer into
     *  the inside layer.
     * @param matrix
     * @param n
     */
    private void rotateMatrix(int[][] matrix, int n){

        for(int layer=0;layer<n/2;layer++){
            int first = layer;
            int last = n-1-layer;

            // follow clockwise order and start from top
            /*
                i should start from the first element of
                the first layer and end at the one position
                before the last(not equal to last),
                because for example, if we start from top, then
                move left side to top, we don't need to rotate when
                i = last. because in the last step, the element
                at i, already been moved to the position last.
             */
            for(int i=first;i<last;i++){
                int offset = i - first;
                //save top element to top variable
                int top = matrix[first][i];
                //move left to top
                matrix[first][i] = matrix[last - offset][first];
                //move bottom to left
                matrix[last - offset][first] = matrix[last][last - offset];
                //move right to bottom
                matrix[last][last - offset] = matrix[i][last];
                // copy top to right
                matrix[i][last] = top;
            }
        }

        for (int i=0;i<matrix.length;i++){
            for (int j=0;j<matrix[0].length;j++){
                System.out.println(matrix[i][j]);
            }
            System.out.println("--------");
        }

    }

    public static void main(String args[]){
        int[][] matrix = {{1,2,3},
                            {4,5,6},
                            {7,8,9}};
        RotateMatrix_1_7 rotateMatrix_1_7 = new RotateMatrix_1_7();
        rotateMatrix_1_7.rotateMatrix(matrix, 3);
    }
}
