package cc150.chapter01;

/**
 * Created by Steven on 9/5/17.
 */
public class Palindrome_Permutation_1_4 {

    /**
     * Method 3:
     *      we can use bit to represent whether the char has an
     *      odd count or not ('1' means odd count and '0' means
     *      even count). Use an integer to represent the status
     *      of the chars instead of an array.
     *
     * @param str
     * @return boolean
     */
    private boolean checkPalindrome3(String str){
        int bitVector = constructBitVector(str);
        return checkOnlyOneBitSet(bitVector);
    }

    /**
     * check that exactly one bit is set by subtracting 1 from the
     * integer and ANDing it with the original integer.
     * @param bitVector
     * @return boolean
     */
    private boolean checkOnlyOneBitSet(int bitVector) {
        return (bitVector & (bitVector - 1)) == 0;
    }

    /**
     * construct a bit vector for string str, and for each letter
     * with value v, toggle vth bit in the vector.
     */
    private int constructBitVector(String str) {
        int bitVector = 0;
        for(char c:str.toCharArray()){
            int val = this.getCharVal(c);
            bitVector = toggle(bitVector, val);
        }
        return bitVector;
    }

    /**
     * toggle the val th bit in the bit vector
     * @param bitVector
     * @param val
     * @return
     */
    private int toggle(int bitVector, int val) {
        if(val < 0) return bitVector;

        int mask = 1 << val;
        if((bitVector & mask) == 0){
            bitVector |= mask;
        }else{
            bitVector &= (~mask);
        }
        return bitVector;
    }

    /**
     * Method 2:
     *      we don't need to go through the table array one more time.
     *      when we build the table array, we can count the number of chars
     *      which have odd count.
     * @param str
     * @return boolean
     */
    private boolean checkPalindrome2(String str){

        int[] table = new int[Character.getNumericValue('z')-Character.getNumericValue('a')+1];
        int count_odd = 0;
        for(int i=0; i<str.length();i++){
            int val = this.getCharVal(str.charAt(i));
            if(val != -1){
                table[val]++;
                if(table[val] % 2 == 1){
                    count_odd++;
                }else{
                    count_odd--;
                }
            }
        }

        return count_odd <= 1;
    }
    /**
     * Method 1:
     *      If it is a permutation of a palindrome string,
     *      the number of characters (non-space and normal Ascii
     *      characters) should either be even or only contain
     *      one character which appeared in odd times in the original
     *      string.
     *      Therefore, to be a permutation of palindrome, the string
     *      can have no more than one character that is odd.
     * @param str
     * @return boolean
     */
    private boolean checkPalindrome(String str){

        int[] table = buildFrequencyTable(str);
        return checkMoreThanOneOdd(table);

    }

    /**
     *  Check no more than one character has an odd count.
     * @param table
     * @return boolean
     */
    private boolean checkMoreThanOneOdd(int[] table) {
        boolean foundOdd = false;
        for(int i : table){
            if( i % 2 != 0 ){
                if(foundOdd){
                    return false;
                }
                foundOdd = true;
            }
        }
        return true;
    }

    /**
     * this function will create an array containing the
     * number of each alphabetical letter appeared in the
     * origin string.
     * @param str
     * @return int[]
     */
    private int[] buildFrequencyTable(String str){
        // the array size should be the number of chars 'a' ~ 'z'
        int[] table = new int[Character.getNumericValue('z')-Character.getNumericValue('a')+1];
        for(int i=0; i<str.length();i++){
            int val = this.getCharVal(str.charAt(i));
            if(val != -1){
                table[val]++;
            }
        }
        return table;
    }

    /**
     * this function will convert those normal Ascii Chars
     * into integers and return -1 when those aren't normal
     * chars (a ~ z, A ~ Z)
     * @param tmp
     * @return int
     */
    private int getCharVal(char tmp){
        int a = Character.getNumericValue('a');
        int z = Character.getNumericValue('z');
        int A = Character.getNumericValue('A');
        int Z = Character.getNumericValue('Z');

        int val = Character.getNumericValue(tmp);
        if(val >= a && val <= z){
            return val - a;
        }else if(val >= A && val <= Z){
            return val - A;
        }
        return -1;
    }

    public static void main(String args[]){

        Palindrome_Permutation_1_4 palindrome_Permutation_1_4 = new Palindrome_Permutation_1_4();
        // should return true
        String str1 = "Tact Coa";
        System.out.println("---------Method 1---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome(str1));
        //testing method 2
        System.out.println("---------Method 2---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome2(str1));
        System.out.println("---------Method 3---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome3(str1));
        // should return false
        str1 = "hllo";
        System.out.println("---------Method 1---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome(str1));
        System.out.println("---------Method 2---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome2(str1));
        System.out.println("---------Method 3---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome3(str1));
        // should return false
        str1 = "hello";
        System.out.println("---------Method 1---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome(str1));
        System.out.println("---------Method 2---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome2(str1));
        System.out.println("---------Method 3---------");
        System.out.println(palindrome_Permutation_1_4.checkPalindrome3(str1));

    }
}
