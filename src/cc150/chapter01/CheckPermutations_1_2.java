package cc150.chapter01;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by Steven on 8/30/17.
 */
public class CheckPermutations_1_2 {

    /**
     * Method 1:
     *  1. convert those 2 strings into 2 char arrays
     *  2. sort those 2 array alphabaltically
     *  3. created 2 new string using those 2 new arrays
     *  4. compare those two new strings(equal or not)
     *
     * @param tmp1
     * @param tmp2
     * @return
     */
    private boolean checkPer_1(String tmp1, String tmp2){

        if(tmp1.length() != tmp2.length()){
            return false;
        }
        if(sortStr(tmp1).equals(sortStr(tmp2))){
            return true;
        }

        return false;
    }

    private String sortStr(String tmp){
        char[] array = tmp.toCharArray();
        Arrays.sort(array);
        return new String(array);
    }

    public static void main(String[] args){
        CheckPermutations_1_2 checkPermutations_1_2 = new CheckPermutations_1_2();
        System.out.println(checkPermutations_1_2.checkPer_1("1s3", "321"));
    }
}
