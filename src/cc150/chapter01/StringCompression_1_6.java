package cc150.chapter01;

/**
 * Created by Steven on 9/11/17.
 */
public class StringCompression_1_6 {

    /**
     *  Method 2:
     *      compare the current char with the next consecutive char,
     *      so that we can remove some redundant code from method 1.
     *      Using StringBuilder will reduce the time complexity from
     *      O(n + k(raise to 2))(which is the time complexity of using '+'
     *      to concatenate strings and k is the length of the number the
     *      character sequence, which we are going to output) to O(n),
     *      n is the length of the original string.
     *
     * @param sample
     * @return String
     */
    private String compressStr2(String sample){

        StringBuilder result = new StringBuilder();
        int countSame = 0;
        for(int i=0; i<sample.length();i++){
            countSame++;
            /*
                check whether the current is different from the next
                character.
                There are only 2 scenarios that we need to append the
                character into the result string:
                    1. current char is different from next char
                    2. 'i' is pointing to the last in the string.
             */
            if(i+1 >= sample.length() || sample.charAt(i) != sample.charAt(i+1)){
                result.append(sample.charAt(i));
                result.append(countSame);

                //clear the counter, because going to count new char or reaching
                // the end of the str.
                countSame = 0;
            }
        }
        String str = new String(result);
        return str.length()>sample.length()?sample:str;
    }
    /**
     * Assumption:
     *      the string only contains Upper and Lower letters(a~z, A~Z)
     * @param sample
     * @return String
     */
    private String compressStr(String sample) {

        if(sample == null || sample.length()==0 || sample.trim().equals("")){
            return "";
        }

        StringBuilder result = new StringBuilder();

        char preC = sample.charAt(0);
        int counter = 0;
        for(int i=0; i<sample.length();i++){
            char curC = sample.charAt(i);

            if(curC == preC){
                counter++;
            }else{
                result.append(preC);
                result.append(counter);
                counter = 1;
                preC = curC;
            }
        }
        result.append(preC);
        result.append(counter);

        String str = new String(result);
        return str.length()>sample.length()?sample:str;
    }

    public static void main(String args[]){
        String sample = "wweeFFFFFFFiijjjjjj";
        StringCompression_1_6 stringCompression_1_6 = new StringCompression_1_6();
        System.out.println(stringCompression_1_6.compressStr(sample));
        System.out.println(stringCompression_1_6.compressStr2(sample));

        sample = "wwFii";
        System.out.println(stringCompression_1_6.compressStr(sample));
        System.out.println(stringCompression_1_6.compressStr2(sample));
    }

}
