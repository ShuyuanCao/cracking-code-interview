package cc150.chapter01;

/**
 * Created by Steven on 8/28/17.
 */
public class IsUnique_1_1 {

    /**
     * Trying to use the ASCII code for normal string characters which
     * contains 256 (0~255) characters in total, but 128 among them are
     * normal characters which can be typed into using keyboard.
     *
     * Assumptions:
     *      1. the string only contains the normal characters (128 in total)
     *
     * @param tmp
     * @return boolean
     */
    public boolean checkUnique(String tmp){

        boolean status[] = new boolean[128];
        if(tmp.length() == 0 || tmp.length() > 128){
            return false;
        }
        for(int i=0; i < tmp.length(); i++){
            char c = tmp.charAt(i);
            if(status[c]){
                return false;
            }
            status[c] = true;
        }
        return true;

    }

    /**
     * This function will use the bit operation 8 bits binary digits.
     * e.g. 0000 0001
     * using the first  bits to represent the 128.
     * assumption:
     *      1. the string only contains the english characters.
     *
     * advantage:
     *      1. this method will use less storage
     *
     * @param tmp
     * @return boolean
     */
    public boolean checkUnique_2(String tmp){
        int status = 0;

        for(int i=0; i < tmp.length();i++){
            int offset = tmp.charAt(i) - 'a';
            if((status & (1 << offset)) > 0){
                return false;
            }
            status |= (1 << offset);
        }

        return true;
    }

    public static void main(String[] args){
        String tmp = "abcdef";
        IsUnique_1_1 isUnique11 = new IsUnique_1_1();
        System.out.println(isUnique11.checkUnique(tmp));
        System.out.println(isUnique11.checkUnique_2(tmp));
        System.out.println( 1 << 2);
    }

}
