package cc150.chapter01;

/**
 * Created by Steven on 9/14/17.
 */
public class ZeroMatrix_1_8 {

    /**
     *
     * @param matrix
     */
    private void zeroMatrix(int[][] matrix){
        
        // we need to use 2 status array to mark the index of
        // the elements in the matrix
        boolean col_marker[] = new boolean[matrix[0].length];
        boolean row_marker[] = new boolean[matrix.length];
        
        for(int i=0; i<matrix.length; i++){
            for (int j=0;j<matrix[0].length; j++){
                if(matrix[i][j] == 0){
                    row_marker[i] = true;
                    col_marker[j] = true;
                }
            }
        }
        
        //change those values in the status array that are marked as true
        //change column values
        for(int i=0;i<col_marker.length;i++){
            if(col_marker[i]){
                for(int j=0;j<matrix.length;j++){
                    matrix[j][i]=0;
                }
            }
        }
        //change row values
        for(int i=0;i<row_marker.length;i++){
            if(row_marker[i]){
                for(int j=0;j<matrix[0].length;j++){
                    matrix[i][j]=0;
                }
            }
        }
        
        // print out the modified matrix
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println("");
        }
    }

    public static void main(String args[]){
        int[][] matrix = {{1, 2, 3, 5}, {4, 0, 6, 0}, {7, 8, 9, 10}};
        ZeroMatrix_1_8 zeroMatrix_1_8 = new ZeroMatrix_1_8();
        zeroMatrix_1_8.zeroMatrix(matrix);
    }
}
