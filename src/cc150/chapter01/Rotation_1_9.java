package cc150.chapter01;

public class Rotation_1_9 {
	
	public static void main(String args[]){
		String str1="";
		String str2="";
		Rotation_1_9 rotation_1_9 = new Rotation_1_9();
		System.out.println(rotation_1_9.checkRotation(str1, str2));
	}
	
	/**
	 * str1: waterbottle
	 * x = wat
	 * y = erbottle
	 *
	 * str2 = yx = erbottlewat
	 *
	 * Regardless of where the division between x and y is, we
	 * can see that yx will always be a substring of xyxy.
	 * That means str2 will always be a substring of str1str1.
	 *
	 * @param str1
	 * @param str2
	 * @return boolean
	 */
	private boolean checkRotation(String str1, String str2) {
	
		int length = str1.length();
		if(length == str2.length() && length > 0){
			String concatenate = str1 + str1;
			return isSubString(concatenate, str2);
		}
		return false;
	}
	
	// assume that this function is already provided
	private boolean isSubString(String concatenate, String str2) {
		return false;
	}
}
