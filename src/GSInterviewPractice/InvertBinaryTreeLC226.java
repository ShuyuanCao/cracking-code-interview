package GSInterviewPractice;

import java.util.LinkedList;

public class InvertBinaryTreeLC226 {
	// recursively
     public TreeNode invertTreeS1(TreeNode root) {
         if(root == null){
             return root;
         }

         TreeNode tmp = root.left;
         root.left = root.right;
         root.right = tmp;

         invertTreeS1(root.left);
         invertTreeS1(root.right);

         return root;
     }
     
     // iteratively
     // iteratively
     public TreeNode invertTreeS2(TreeNode root){
	     LinkedList<TreeNode> list = new LinkedList<>();
	     if(root != null){
		     list.add(root);
	     }
	     while(!list.isEmpty()){
		     TreeNode node = list.poll();
		     if(node.left != null){
			     list.add(node.left);
		     }
		     if(node.right != null){
			     list.add(node.right);
		     }
		
		     TreeNode tmp = node.right;
		     node.right = node.left;
		     node.left = tmp;
	     }
	
	     return root;
     }
}
 class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
}
