package GSInterviewPractice;

public class ReverseSentence {
	
	
	public String reverseSentence(String tmp) {
		
		StringBuilder result = new StringBuilder();
		String words[] = tmp.split(" ");
		for(String word : words){
			StringBuilder sb = new StringBuilder(word);
			sb.reverse();
			result.append(sb.toString()).append(" ");
		}
		
		return result.toString().trim();
	}
}
