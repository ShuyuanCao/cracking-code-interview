package GSInterviewPractice;

import java.util.LinkedList;

public class TreeTraversals {
	
	/**
	 * Pre-Order Traversal:
	 *      root, left, right
	 * @param root
	 * @return
	 */
	public LinkedList<Integer> preOrderTraversal(BinaryTreeNode root) {
		
		LinkedList<Integer> results = new LinkedList<>();
		
		preOrderTraverse(root, results);
		
		return results;
	}
	
	private void preOrderTraverse(BinaryTreeNode root, LinkedList<Integer> results) {
		
		if(root == null){
			return;
		}
		
		results.add(root.val);
		//traverse left child
		preOrderTraverse(root.left, results);
		//traverse right child
		preOrderTraverse(root.right, results);
	}
	
	/**
	 * In-Order Traversal:
	 *       left, root, right
	 * @param root
	 * @return
	 */
	public LinkedList<Integer> inOrderTraversal(BinaryTreeNode root) {
		
		LinkedList<Integer> results = new LinkedList<>();
		
		inOrderTraverse(root, results);
		
		return results;
	}
	
	private void inOrderTraverse(BinaryTreeNode root, LinkedList<Integer> results) {
		if(root == null){
			return;
		}
		
		//traverse left at first
		inOrderTraverse(root.left, results);
		//add root into results
		results.add(root.val);
		//traverse right
		inOrderTraverse(root.right, results);
	}
	
	/**
	 * Post-Order Traversal:
	 *       left, right, root
	 * @param root
	 * @return
	 */
	public LinkedList<Integer> postOrderTraversal(BinaryTreeNode root) {
		
		LinkedList<Integer> results = new LinkedList<>();
		
		postOrderTraverse(root, results);
		
		return results;
	}
	
	private void postOrderTraverse(BinaryTreeNode root, LinkedList<Integer> results) {
		if(root == null){
			return;
		}
		
		//left
		postOrderTraverse(root.left, results);
		//right
		postOrderTraverse(root.right, results);
		//root
		results.add(root.val);
		
	}
}

class BinaryTreeNode{
	int val;
	BinaryTreeNode left;
	BinaryTreeNode right;
	public BinaryTreeNode(int val){
		this.val = val;
		this.left = null;
		this.right = null;
	}
}
