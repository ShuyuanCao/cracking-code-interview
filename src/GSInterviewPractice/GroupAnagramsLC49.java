package GSInterviewPractice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GroupAnagramsLC49 {
	
	public List<List<String>> groupAnagrams(String[] strs) {
		
		if(strs == null || strs.length == 0){
			return null;
		}
		
		HashMap<String, List<String>> map = new HashMap<>();
		
		for(int i = 0; i < strs.length; i++){
			char[] tmp = strs[i].toLowerCase().toCharArray();
			Arrays.sort(tmp);
			String tStr = new String(tmp);
			
			if(map.containsKey(tStr)){
				List<String> tmpList = map.get(tStr);
				tmpList.add(strs[i]);
				map.put(tStr, tmpList);
			}else{
				List<String> newList = new ArrayList<>();
				newList.add(strs[i]);
				map.put(tStr, newList);
			}
		}
		
		List<List<String>> results = new ArrayList<>();
		for(String key : map.keySet()){
			results.add(map.get(key));
		}
	
		return results;
	}

}
