package GSInterviewPractice;

public class BinarySearchTreeSearchInsertion {
	
	/**
	 * Time Complexity in worst case: (highly skewed binary search tree)
	 *      O(n): n is number of nodes in the binary search tree.
	 *
	 * @param root
	 * @param key
	 * @return
	 */
	public BinaryTreeNode search(BinaryTreeNode root, int key) {
		
		if(root == null || root.val == key){
			return root;
		}
		
		if(key > root.val){
			// search through right subtree
			return search(root.right, key);
		}
		
		return search(root.left, key);
	}
	
	/**
	 * A new key is always inserted at leaf. We start searching a key
	 * from root till we hit a leaf node. Once a leaf node is found,
	 * the new node is added as a child of the leaf node.
	 *
	 * @param root
	 * @param key
	 * @return
	 */
	public BinaryTreeNode insert(BinaryTreeNode root, int key) {
		
		// if the tree is empty, return a new node.
		if(root == null){
			root = new BinaryTreeNode(key);
			return root;
		}
		
		// otherwise, recurse down the tree.
		if(key > root.val){
			root.right = insert(root.right, key);
		}else{
			root.left = insert(root.left, key);
		}
	
		return root;
	}
}
