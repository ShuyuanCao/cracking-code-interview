package GSInterviewPractice;

import java.util.LinkedHashMap;
import java.util.Map;

public class FindUniqueChar {
	public String findFirstUniqueChar(String str) {
	
		if(str == null || str.trim().isEmpty()){
			return "";
		}
		
		Map<Character, Integer> map = new LinkedHashMap<>();
		for(int i = 0; i < str.length(); i++){
			char c = str.charAt(i);
			if(map.containsKey(c)){
				map.put(c, map.get(c) + 1);
			}else{
				map.put(c, 1);
			}
		}
	
		for(char c : map.keySet()){
			if(map.get(c) == 1){
				return String.valueOf(c);
			}
		}
		
		return "";
	}
}
