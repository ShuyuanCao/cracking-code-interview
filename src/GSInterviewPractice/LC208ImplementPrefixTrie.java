package GSInterviewPractice;


public class LC208ImplementPrefixTrie {
	
	private TrieNode root;
	
	/** Initialize your data structure here. */
	public LC208ImplementPrefixTrie() {
		this.root = new TrieNode();
	}
	
	/** Inserts a word into the trie. */
	public void insert(String word) {
	
		TrieNode tmp = root;
		for(int i = 0; i < word.length(); i++){
			char c = word.charAt(i);
			if(tmp.children[c - 'a'] == null){
				tmp.children[c - 'a'] = new TrieNode();
			}
			tmp = tmp.children[c - 'a'];
		}
		tmp.hasWord = true;
	}
	
	/** Returns if the word is in the trie. */
	public boolean search(String word) {
	
		TrieNode tmp = root;
		for(int i = 0; i < word.length(); i++){
			char c = word.charAt(i);
			// the word doesn't exist in this Trie
			if(tmp.children[c - 'a'] == null) return false;
			tmp = tmp.children[c - 'a'];
		}
		
		return tmp.hasWord;
	}
	
	/** Returns if there is any word in the trie that starts with the given prefix. */
	public boolean startsWith(String prefix) {
	
		TrieNode tmp = root;
		for(int i = 0; i < prefix.length(); i++){
			char c = prefix.charAt(i);
			
			if(tmp.children[c - 'a'] == null) return false;
			tmp = tmp.children[c - 'a'];
		}
	
		return true;
	}
}

class TrieNode {
	public boolean hasWord;
	public TrieNode[] children;
	
	public TrieNode(){
		this.hasWord = false;
		children = new TrieNode[26];
	}
}
