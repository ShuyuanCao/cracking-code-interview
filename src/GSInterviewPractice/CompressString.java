package GSInterviewPractice;

public class CompressString {
	
	
	public String compressStr(String tmp) {
		
		if(tmp == null || tmp.trim().length() == 0){
			return "";
		}
		
		char[] array = tmp.toCharArray();
		StringBuilder sb = new StringBuilder();
		int count = 0;
		char prev = array[0];
		
		for(int i = 0; i < array.length; i++){
			char c = array[i];
			if(c == prev){
				count++;
			}else{
				sb.append(prev).append(count);
				count = 1;
			}
			
			prev = c;
		}
	
		return sb.append(prev).append(count).toString();
	}
}
