package GSInterviewPractice;


public class CloneLinkedinListWithNextAndRandomPointers {
	
	/**
	 *
	 * Clone a linked list with next and random pointer.
	 *      space complexity O(n)
	 * Reference:
	 *      https://www.geeksforgeeks.org/a-linked-list-with-next-and-arbit-pointer/
	 * @param head
	 * @return
	 */
	public LinkedListNode copyLinkedListS1(LinkedListNode head) {
		
		LinkedListNode tmp = head;    //iterate through the original list
		LinkedListNode newList = new LinkedListNode(-1);//pointing to the head of the copied list
		LinkedListNode tmp2 = newList;   //iterate through the copied list
		
		// newList.next should be the first node in the copied linked list
		while(tmp != null){
			//(1) Create all nodes in copy linked list using next pointers
			//(2) Store the node and its next pointer mappings of original linked list
			tmp2.next = new LinkedListNode(tmp.val);
			//(3) move the tmp2 to point to the latest node at first.
			//(4) random pointer of current node in the copied linked list should
			// point to the corresponding node in the original linked list.
			tmp2 = tmp2.next;
			tmp2.random = tmp;
			
			tmp = tmp.next;
		}
		
		// Now construct the random pointer in copy linked list as below and restore the
		// next pointer of nodes in the original linked list.
		LinkedListNode tmpNewList = newList.next;
		
		while(tmpNewList != null){
		
			tmpNewList.random = tmpNewList.random.random;
			tmpNewList = tmpNewList.next;
		
		}
		
		return newList.next;
	}
}


class LinkedListNode{
	int val;
	LinkedListNode next;
	LinkedListNode random;
	public LinkedListNode(int val){
		this.val = val;
		this.next = null;
		this.random = null;
	}
}