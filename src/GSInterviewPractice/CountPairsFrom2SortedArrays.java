package GSInterviewPractice;

import java.util.HashMap;

public class CountPairsFrom2SortedArrays {
	
	/**
	 * Count pairs from two sorted arrays whose sum is equal to a given value x.
	 *
	 * Reference:
	 *      https://www.geeksforgeeks.org/count-pairs-two-sorted-arrays-whose-sum-equal-given-value-x/
	 *
	 * Solution 1: 2 for loops. Time Complexity: O(mn), m and n are lengths of
	 *              those 2 arrays.
	 *
	 * Solution 2: Binary Search
	 *      for each element array1[i] in array1, !!!!binary search!!!! the value (sum - array1[i]) in
	 *      array2.  if the search is successful, then increment the count.
	 */
	public int countPairsEqualsTargetS2(int[] array1, int[] array2, int sum){
		
		if(array1.length == 0 || array2.length == 1){
			return 0;
		}
		
		int result = 0;
		for(int i = 0; i < array1.length; i++){
		
			if(binarySearchTrue(sum - array1[i], array2, 0, array2.length - 1)){
				result++;
			}
		
		}
		
		return result;
	}
	
	/**
	 *  this is a sorted array, so use binary search directly.
	 * @param target
	 * @param array2
	 * @param left
	 * @param right
	 * @return
	 */
	private boolean binarySearchTrue(int target, int[] array2, int left, int right) {
		
		while(left <= right){
		
			int mid = (left + right)/2;
			
			if(array2[mid] == target){
				return true;
			}else if(array2[mid] > target){
				right = mid - 1;
			}else{
				left = mid + 1;
			}
			
		}
		
		// value not found
		return false;
	}
	
	/**
	 * Using HashMap to store the first array and check whether sum - array2[i] is
	 * existing in HashMap or not.
	 *
	 * @return
	 */
	public int countPairsEqualsTargetS3(int[] array1, int[] array2, int sum){
	
		if(array1.length == 0 || array2.length == 0){
			return 0;
		}
		
		HashMap<Integer, Integer> map = new HashMap<>();
		
		for(int i : array1){
			if(map.containsKey(i)){
				map.put(i, map.get(i) + 1);
			}else{
				map.put(i, 1);
			}
		}
		
		int countPair = 0;
		for (int i : array2){
		
			int target = sum - i;
			
			if(map.containsKey(target)){
				countPair += map.get(target);
			}
		
		}
		
		return countPair;
	}
	
}
