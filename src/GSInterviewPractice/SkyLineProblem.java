package GSInterviewPractice;

import java.util.*;

public class SkyLineProblem {
	/**
	 *  References:
	 *      https://segmentfault.com/a/1190000003786782
	 *      https://blog.csdn.net/u013325815/article/details/52979439
	 *      https://leetcode.com/problems/the-skyline-problem/description/
	 *
	 *  [Li, Ri, Hi]
	 */
	public List<int[]> getSkyline(int[][] buildings) {
		
		List<int[]> results = new ArrayList<>();
		List<int[]> points = new ArrayList<>();
		
		for(int[] b : buildings){
			// left
			points.add(new int[]{b[0], -b[2]});
			// right
			points.add(new int[]{b[1], b[2]});
		}
		
		// sort points by ascending order of x coordinate
		// if two points have same x coordinate, the one
		// with smaller y coordinate will be put before another.
		Collections.sort(points, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				if(o1[0] != o2[0]){
					return o1[0] - o2[0];
				}else{
					return o1[1] - o2[1];
				}
			}
		});
	
		// create a priority queue, the highest value will put at the
		// top of the tree. Descending order.
		PriorityQueue<Integer> pq = new PriorityQueue<>(10000, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		
		// add horizon line into pq
		pq.add(0);
		int preHeight = 0;
		
		for(int[] point: points){
			
			// add the left vertice into pq
			if(point[1] < 0){
				pq.add(-point[1]);
			}else{
				// if this is the right vertice, remove the corresponding
				// right vertice.
				pq.remove(point[1]);
			}
			
			int curHeight = pq.peek();
			if(curHeight != preHeight){
				results.add(new int[]{point[0], curHeight});
				preHeight = curHeight;
			}
		
		}
		
		return results;
	}
}
