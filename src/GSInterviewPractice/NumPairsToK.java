package GSInterviewPractice;

import java.util.HashMap;

public class NumPairsToK {
	
	
	public int getNumPairs(int[] array, int sum) {
		
		HashMap<Integer, Integer> map = new HashMap<>();
		int size = array.length;
		// store the count of the each unique element in the array
		for(int i = 0; i < size; i++){
			if(!map.containsKey(array[i])){
				map.put(array[i], 0);
			}
			
			map.put(array[i], map.get(array[i]) + 1);
		}
		
		// count the pairs
		int countPairTwice = 0;
		for(int i = 0; i < size; i++){
			int residual = sum - array[i];
			if(map.get(residual) != null){
				countPairTwice += map.get(residual);
			}
			
			if(residual == array[i]){
				// reduce 1 pair which is with itself
				countPairTwice--;
			}
		}
		
		return countPairTwice/2;
	}
}
