package GSInterviewPractice;

import java.util.*;

public class UniqueSubstrWithLengthK {
	
	/**
	 * Given a string s, find all the unique substrings of this string s with
	 * length k and sort the the unique substrings.
	 *
	 * For example: 'caaab', if k = 2, return 'aa', 'ab', 'ca'
	 *
	 * @param str
	 * @param k
	 * @return
	 */
	public String[] numberOfSubstrs(String str, int k){
	
		if(str == null || str.trim().length() == 0 || k <= 0 || k > str.length()){
			return new String[0];
		}
		
		HashSet<String> hashSet = new HashSet<>();
		for(int i = 0; i <= (str.length() - k); i++){
			String tmp = str.substring(i, i + k);
			hashSet.add(tmp);
		}
		
		List<String> tmpList = new ArrayList<>(hashSet);
		
		Collections.sort(tmpList);
		String[] results = new String[tmpList.size()];
		for (int i = 0; i < tmpList.size(); i++){
			results[i] = tmpList.get(i);
		}
		
		return results;
	}

}
