package GSInterviewPractice;

import java.util.Arrays;

public class IsAnagram {
	
	public boolean solution1(String s, String t) {
		char[] arrS = s.toCharArray();
		char[] arrT = t.toCharArray();
		Arrays.sort(arrS);
		Arrays.sort(arrT);
		
		return String.valueOf(arrS).equals(String.valueOf(arrT));
	}
	
	public boolean solution2(String s, String t){
		if(s == null || t == null || s.length() != t.length()){
			return false;
		}
		
		int[] array = new int[255];
		for(int i = 0; i < s.length(); i++){
			array[s.charAt(i) - 'a']++;
		}
		
		for(int i = 0; i < t.length(); i++){
			array[t.charAt(i) - 'a']--;
		}
		
		for(int i : array){
			if(i != 0){ return false;}
		}
		return true;
	}
}
