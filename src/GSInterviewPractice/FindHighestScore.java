package GSInterviewPractice;

import java.util.ArrayList;
import java.util.HashMap;

public class FindHighestScore {
	
	public double findHighestScore(String[][] input) {
		
		if(input == null || input.length == 0 || input[0].length == 0){
			return 0;
		}
		
		HashMap<String, ArrayList<Double>> map = new HashMap<>();
		
		for(int i = 0; i < input.length; i++){
			String name = input[i][0];
			double score = Double.parseDouble(input[i][1]);
			
			if(map.containsKey(name)){
				ArrayList<Double> tmp_list = map.get(name);
				tmp_list.add(score);
				map.put(name, tmp_list);
			}else{
				ArrayList<Double> tmp_list = new ArrayList<>();
				tmp_list.add(score);
				map.put(name, tmp_list);
			}
		}
		
		/* calculate average score and find the highest score */
		/**
		 * !!!!!!!Note this finalScore should be initialized with Integer.MIN_VALUE!!!!!!!!
		 */
		double finalScore = 0;
		for(String name : map.keySet()){
			ArrayList<Double> scores = map.get(name);
			double tmpScore = 0;
			for(double tmp:scores){
				tmpScore += tmp;
			}
			
			double average = tmpScore/scores.size();
			if(average < 0){
				average = Math.floor(average);
			}
			if(average > finalScore){
				finalScore = average;
			}
		}
		
		return finalScore;
	}
}
