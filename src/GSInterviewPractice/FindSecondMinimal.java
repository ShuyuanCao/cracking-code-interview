package GSInterviewPractice;

public class FindSecondMinimal {
	public int findSecondMinimal(int[] arr) {
	
		if(arr == null || arr.length < 2){
			return Integer.MIN_VALUE;
		}
		
		int firstMinimal, secMinimal;
		firstMinimal = secMinimal = Integer.MAX_VALUE;
		
		for(int i = 0; i < arr.length; i++){
			// if current element is smaller than both, update
			// the first to it and second to first.
			if(arr[i] < firstMinimal){
				secMinimal = firstMinimal;
				firstMinimal = arr[i];
			}else if(arr[i] < secMinimal && arr[i] != firstMinimal){
				secMinimal = arr[i];
			}
		}
		
		return secMinimal;
	}
}
