package GSInterviewPractice;

import java.util.ArrayList;
import java.util.List;

public class MyPrefixSearch1806Phone {
	
	private String _document;
	public MyPrefixSearch1806Phone(String _document){
		this._document = _document;
	}
	/*
	 * findAll: Return a list of all locations in a document where the
	 * (case insensitive) word begins with the given prefix.
	 *
	 * Example:  given the document "a aa Aaa abca bca",
	 *   1) findAll("a")   -> [0, 2, 5, 9]
	 *   2) findAll("bc")  -> [14]
	 *   3) findAll("aA")  -> [2, 5]
	 *   4) findAll("abc") -> [9]
	 *
	 **/
	public List<Integer> findAll(String prefix)
	{
		List<Integer> list = new ArrayList<>();
		if(prefix == null || prefix.trim().equals("")){
			return list;
		}
		
		prefix = prefix.toLowerCase();
		int docLen = _document.length();
		
		// iterate through this document
		int i = 0;
		while(i < docLen){
			char c = _document.charAt(i);
			
			if(c == ' '){
				i++;
				continue;
			}else{
				int endI = i + 1;
				
				if(endI >= docLen){
					endI = docLen - 1;
				}
				
				while(_document.charAt(endI) != ' '){
					endI++;
					if(endI >= docLen){
						endI = docLen - 1;
						break;
					}
				}
				// find the string start with i and ended with endI - 1
				String subStr = _document.substring(i, endI);
				subStr = subStr.toLowerCase();
				
				if(subStr.startsWith(prefix)){
					list.add(i);
				}
				
				// update i with the value of endI + 1
				i = endI + 1;
			}
			
		}
		
		return list;
	}

}
