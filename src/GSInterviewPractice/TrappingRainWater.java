package GSInterviewPractice;

public class TrappingRainWater {
	
	/**
	 * Reference:
	 *      Geeks for Geeks: https://www.geeksforgeeks.org/trapping-rain-water/
	 *      LeetCode: https://leetcode.com/problems/trapping-rain-water/description/
	 */
	public int findWater(int array[]){
		
		if(array == null || array.length <= 2){
			return 0;
		}
		
		int countWater = 0;
		for(int i = 1; i < array.length - 1; i++){
			int tmp = array[i];
			
			int maxLeft = -1;
			for(int left = i - 1; left >= 0; left--){ // NOTE: have to be left--
				maxLeft = Math.max(maxLeft, array[left]);
			}
			
			int maxRight = -1;
			for(int right = i + 1; right < array.length; right++){
				maxRight = Math.max(maxRight, array[right]);
			}
			
			if(maxLeft > tmp && maxRight > tmp){
				// NOTE: has to be the difference between Math.min(maxLeft, maxRight) - tmp
				countWater += (Math.min(maxLeft, maxRight) - tmp);
			}
		}
		
		return countWater;
	}
	
	public int findWaterS2(int array[]){
		/**
		 * A Simple Solution is to traverse every array element and find the
		 * highest bars on left and right sides. Take the smaller of two
		 * heights. The difference between smaller height and height of
		 * current element is the amount of water that can be stored in this
		 * array element. Time complexity of this solution is O(n2).
		 *
		 * An Efficient Solution is to prre-compute highest bar on left and
		 * right of every bar in O(n) time. Then use these pre-computed values
		 * to find the amount of water in every array element.
		 */
		
		if(array == null || array.length <= 2){
			return 0;
		}
		
		// fill in left
		int left[] = new int[array.length];
		left[0] = array[0];
		for(int i = 1; i < array.length; i++){ // NOTE: i should start from 1
			left[i] = Math.max(left[i - 1], array[i]);
		}
		
		// fill in right
		int right[] = new int[array.length];
		right[array.length - 1] = array[array.length - 1];
		for(int i = array.length - 2; i >= 0; i--){
			right[i] = Math.max(right[i + 1], array[i]);
		}
		
		// find the count
		int countWater = 0;
		for(int i = 0; i < array.length; i++){
			countWater += (Math.min(left[i], right[i]) - array[i]);
		}
		
		return countWater;
	}

	public int findWaterS3(int array[]){
		/**
		 * Most Optimal Solution:
		 *      Reduced Space Complexity.
		 *
		 * Good Explanation:
		 *      https://blog.csdn.net/gwt0425/article/details/79476269
		 *      The above blog is using some pictures to explain the process of
		 *      this algorithm, which is highly easy to understand and highly
		 *      recommended.
		 *
		 *      !!!!!!......This is an amazing method......!!!!!!
		 */
		int left = 0;
		int right = array.length - 1;
		int leftMax = 0, rightMax = 0;
		int result = 0;
		
		while(left < right){
		
			if(array[left] < array[right]){ // update the smaller on left side
				if(array[left] > leftMax){
					leftMax = array[left];
				}else{
					result += (leftMax - array[left]);
				}
				left++;
			}else{                          // update the smaller on right side
				if(array[right] >= rightMax){
					rightMax = array[right];
				}else{
					result += (rightMax - array[right]);
				}
				right--;
			}
		
		}
		
		return result;
	}
	
}
