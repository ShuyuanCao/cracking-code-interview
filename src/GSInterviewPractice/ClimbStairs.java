package GSInterviewPractice;

import java.util.LinkedList;

public class ClimbStairs {
	
	/**
	 * Solution 1:
	 *      Recursion
	 *
	 *      Assumption:
	 *          when n == 1, there is only 1 way to get
	 *          step 1.
	 */
	public int solution1(int n){
		if(n < 0){
			return 0;
		}else if(n == 0){
			return 1;
		}else{
			return solution1(n - 1) + solution1(n - 2);
		}
		
	}
	
	/**
	 * Solution 2:
	 *      Dynamic Programming Array
	 */
	public int solution2(int n){
	
		int ways[] = new int[n];
		ways[0] = 1;
		if(n >= 2){
			ways[1] = 2;
		}
		for(int i = 2; i < n; i++){
			ways[i] = ways[i - 1] + ways[i - 2];
		}
		
		return ways[n - 1];
	}
	
	/**
	 * Solution 3:
	 *      Using queue to only store the past 2 values in order
	 *      to reduce space complexity.
	 *
	 *      Space Complexity: O(1), only 3 elements
	 */
	public int solution3(int n){
		
		LinkedList<Integer> queue = new LinkedList<>();
		queue.add(1);
		queue.add(2);
		
		if(n == 1){
			return queue.get(0);
		}else if(n == 2){
			return queue.get(1);
		}else{
			for(int i = 2; i < n; i++){
				//add the last one; Note: the index
				queue.add(queue.get(0) + queue.get(1));
				//remove the first element
				queue.removeFirst();
			}
			
			// always return the last element in the queue.
			return queue.getLast();
		}
	}
	
	/**
	 * N steps stair:
	 *      Every time you can go 1~m steps.
	 *
	 *      从每步1～2级阶梯到1～m级阶梯，需要改变的不仅仅只是步长，还有每步走完之后，
	 *      对剩余台阶总数的判断。当当总剩余的台阶数n 大于等于 步长m的时候，则第一步为1～m的一个数值，
	 *      循环递归求解可能的走法。如果总剩余台阶数n小于步长m的时候，则步长调整为n，递归求解。
	 */
	public int generalizedSolution(int n, int m){
	
		int ways = 0;
		
		if(n == 0){
			return 1;
		}
		
		if(n >= m){ // Note: this operator should be '>='
			for(int i = 1; i <= m; i++){
				ways += generalizedSolution(n - i, m);
			}
		}else{
			ways += generalizedSolution(n, n);
		}
		
		return ways;
	}
}
