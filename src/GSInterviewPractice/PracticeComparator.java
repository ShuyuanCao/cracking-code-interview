package GSInterviewPractice;

import java.util.*;

public class PracticeComparator {

	class Dog{
		public int age;
		public String name;
		public Dog(int age, String name){
			this.age = age;
			this.name = name;
		}
		
		@Override
		public String toString() {
			return "Dog{" +
					"age=" + age +
					", name='" + name + '\'' +
					'}';
		}
	}
	
	public static void main(String[] args){
		List<Dog> list = new ArrayList<>();
		list.add(new PracticeComparator().new Dog(5, "A"));
		list.add(new PracticeComparator().new Dog(6, "B"));
		list.add(new PracticeComparator().new Dog(7, "C"));
		
		// In Java 8, we can use lambda expression to sort the elements
		// in a list
		list.sort((o1, o2) -> o2.age - o1.age);
		
		// Or we can use Collections.sort(list, new Comparator(){})
//		Collections.sort(list, new Comparator<Dog>() {
//			@Override
//			public int compare(Dog o1, Dog o2) {
//				return o2.age - o1.age;     // following the descending order of age
//			}
//		});
		
		System.out.println("Descending order of age: " + list);
		
		Collections.sort(list, new Comparator<Dog>() {
			@Override
			public int compare(Dog o1, Dog o2) {
				
				// str1.compareTo(str2) method is used for comparing the unicode
				// value of those 2 strings.
				// if unicode(str1) == unicode(str2), this method will return 0
				// if unicode(str1) > unicode(str2), this method will return positive number
				// if unicode(str1) < unicode(str2), this method will return negative number
				return o1.name.compareTo(o2.name);
			}
		});
		
		System.out.println("Alphabetical order of name: " + list);
	}

}
