package GSInterviewPractice;

public class DotProductArrays {
	
	
	public int dotProduct(int[] firArray, int[] secArray) {
		
		int sum = 0;
		
		if(firArray.length == 0 || secArray.length == 0 ||
				firArray.length != secArray.length){
			return sum;
		}
		
		int size = firArray.length;
		for(int i = 0; i < size; i++){
			sum += firArray[i] * secArray[i];
		}
		
		return sum;
	}
}
