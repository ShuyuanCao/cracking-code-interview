package GSInterviewPractice;

import java.util.HashMap;
import java.util.LinkedList;

public class ConstructTreeGivenInorderAndPreOrderTraversals {
	
	/**
	 * LeetCode:
	 *      https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/
	 *
	 * Reference:
	 *      https://blog.csdn.net/linhuanmars/article/details/24389549
	 *      https://www.geeksforgeeks.org/print-postorder-from-given-inorder-and-preorder-traversals/
	 *
	 * @param inOrder
	 * @param preOrder
	 * @return
	 */
	public Node buildBinaryTree(char[] inOrder, char[] preOrder) {
		if(inOrder == null || preOrder == null){
			return null;
		}
		// map the relationship between index and value in inOrder array into hash map
		HashMap<Character, Integer> map = new HashMap<>();
		for(int i = 0; i < inOrder.length; i++){
			map.put(inOrder[i], i);
		}
		
		return helper(preOrder, 0, preOrder.length - 1, 0, inOrder.length - 1, map);
	}
	
	private Node helper(char[] preOrder, int preLeft, int preRight, int inLeft, int inRight, HashMap<Character, Integer> map) {
		
		if(preLeft > preRight || inLeft > inRight){
			return null;
		}
		
		Node root = new Node(preOrder[preLeft]);
		int inOrderIndex = map.get(root.val);
		
		root.left = helper(preOrder, preLeft + 1, inOrderIndex - inLeft + preLeft, inLeft, inOrderIndex - 1, map);
		root.right = helper(preOrder, preLeft + 1 + inOrderIndex - inLeft, preRight, inOrderIndex + 1, inRight, map);
		
		return root;
	}
	
	public LinkedList<Character> postOrderTraversal(Node root){
		LinkedList<Character> result = new LinkedList<>();
		postOrderTraverse(root, result);
		return result;
	}
	
	private void postOrderTraverse(Node root, LinkedList<Character> result) {
	
		if(root != null){
			// traverse left subtree
			postOrderTraverse(root.left, result);
			// traverse right subtre
			postOrderTraverse(root.right, result);
			// add root into result list
			result.add(root.val);
		}
		
	}
	
	public LinkedList<Character> preOrderTraversal(Node root){
		LinkedList<Character> result = new LinkedList<>();
		preOrderTraverse(root, result);
		return result;
	}
	
	public void preOrderTraverse(Node root, LinkedList<Character> result) {
		
		if(root != null){
			// add root into result list
			result.add(root.val);
			// traverse left subtree
			preOrderTraverse(root.left, result);
			// traverse right subtree
			preOrderTraverse(root.right, result);
		}
		
	}
	
	public LinkedList<Character> inOrderTraversal(Node root){
		LinkedList<Character> result = new LinkedList<>();
		inOrderTraverse(root, result);
		return result;
	}
	
	public void inOrderTraverse(Node root, LinkedList<Character> result) {
		
		if(root != null){
			// traverse left subtree
			inOrderTraverse(root.left, result);
			// add root into result list
			result.add(root.val);
			// traverse right subtree
			inOrderTraverse(root.right, result);
		}
		
	}
}

/**
 * Suppose this is a binary tree.
 */
class Node{
	char val;
	Node left;
	Node right;
	public Node(char val){
		this.val = val;
		left = right = null;
	}
}
