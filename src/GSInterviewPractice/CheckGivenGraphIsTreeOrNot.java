package GSInterviewPractice;

import java.util.Iterator;
import java.util.LinkedList;

public class CheckGivenGraphIsTreeOrNot {
	
	/**
	 * Check if a given graph is a tree or not.
	 *      Reference:
	 *          https://www.geeksforgeeks.org/check-given-graph-tree/
	 *
	 *
	 * This class represents a directed graph using adjacency list representation
	 *
	 */
	private int V; // No. of vertices.
	private LinkedList<Integer> adj[]; // adjacency list
	
	public CheckGivenGraphIsTreeOrNot(int v){
		V = v;
		adj = new LinkedList[v];
		for(int i = 0; i < v; i++){
			adj[i] = new LinkedList<>();
		}
	}
	
	// function to add an edge into this graph
	public void addEdge(int v, int w){
		adj[w].add(v);
		adj[v].add(w);
	}
	
	/**
	 *  Using DFS(Depth First Method):
	 *      A recursive function that uses visited[] and parent
	 *      to detect cycle in sub-graph reachable from vertex v.
	 * @param v
	 * @param visited
	 * @param parent
	 * @return
	 */
	public boolean isCyclicUtil(int v, boolean[] visited, int parent){
		
		// mark the current as visited
		visited[v] = true;
		
		// recurse for all the vertices that are adjacent to this vertex.
		Iterator<Integer> iterator = adj[v].iterator();
		while(iterator.hasNext()){
		
			int i = iterator.next();
			
			if(!visited[i]){
				// if an adjacent is not visited, then recurse that adjacent.
				if(isCyclicUtil(i, visited, v)){
					return true;
				}
			}else if(i != parent){
				// if an adjacent is visited, and not parent of the current
				// vertex, then there is a cycle.
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 *  Returns true if a graph is a tree, else false.
	 * @return
	 */
	public boolean isTree(){
	
		// mark all the vertexes as not visited.
		boolean visited[] = new boolean[V];
		for(int i = 0; i < visited.length; i++){
			visited[i] = false;
		}
		
		// the call to isCyclicUtil() serves multiple purposes:
		// (1) returns true, if graph reachable from vertex 0 is cyclic.
		// (2) marks all vertices reachable from vertex 0.
		if(isCyclicUtil(0, visited, -1)){
			return false;
		}
		
		// check whether the vertexes are all available or not.
		for(int i = 0; i < visited.length; i++){
			if(!visited[i]){
				return false;
			}
		}
		
		return true;
	}
	
}
