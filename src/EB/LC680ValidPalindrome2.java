package EB;

public class LC680ValidPalindrome2 {
	
	public boolean validPalindrome(String s) {
		
		/**
		 * reference:
		 *      https://www.cnblogs.com/Dylan-Java-NYC/p/7561172.html
		 *
		 *
		 * 当出现错位时，或左或右向内移动一位.
		 *
		 * Time Complexity: O(s.length()). Space: O(1).
		 */
		int left = 0;
		int right = s.length() - 1;
		
		while(left < right){
			
			if(s.charAt(left) != s.charAt(right)){
				// we have to return immediately,
				// because we are required to delete at most only 1 character.
				return isPalindrome(s, left + 1, right) || isPalindrome(s, left, right - 1);
			}
			
			left++;
			right--;
			
		}
		
		return true;
	}
	
	public boolean isPalindrome(String s, int left, int right) {
		
		while(left < right){
			if(s.charAt(left) != s.charAt(right)){
				return false;
			}
			
			left++;
			right--;
		}
		
		return true;
	}
	
}
