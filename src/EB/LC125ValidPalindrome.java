package EB;

public class LC125ValidPalindrome {
	
	public boolean isPalindrome(String s) {
	
		if(s == null){
			return false;
		}
		
		if(s.trim().length() == 0){
			return true;
		}
		
		int left = 0;
		int right = s.length() - 1;
		while(left < right){
			char cL = s.charAt(left);
			char cR = s.charAt(right);
			
			if(!isValid(cL)){
				left++;
				continue;
			}
			if(!isValid(cR)){
				right--;
				continue;
			}
			
			if(isEqual(cL, cR)){
				left++;
				right--;
			}else{
				return false;
			}
		}
	
		return true;
	}
	
	public boolean isEqual(char cL, char cR) {
		
		if(cL <= 'Z' && cL >= 'A'){
			cL = (char) (cL - 'A' + 'a');
		}
		
		if(cR <= 'Z' && cL >= 'A'){
			cR = (char) (cR - 'A' + 'a');
		}
		
		return cL == cR;
	}
	
	public boolean isValid(char tmp){
		if((tmp >= 'a' && tmp <= 'z') || (tmp >= 'A' && tmp <= 'Z')
				|| (tmp >= '0' && tmp <= '9')){
			return true;
		}
		return false;
	}

}
