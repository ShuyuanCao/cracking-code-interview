package classical_algo.linkedlist;

public class ReverseLinkedList {
	
	/**
	 * Solution 1:
	 *      iteration
	 *
	 * @param head
	 * @return
	 */
	public Node reverseS1(Node head) {
	
		Node pre = null;
		Node next;
		
		// has to be head != null and finally
		// return pre which is pointing to the final valid node.
		while(head != null){
			
			next = head.next;
			head.next = pre;
			pre = head;
			head = next;
		
		}
		
		return pre;
	}
	
	/**
	 * Solution 2:
	 *      recursion
	 *
	 * @return
	 */
	public Node reverseS2(Node head){
	
		if(head == null || head.next == null){
			return head;
		}
		
		Node newHead = reverseS2(head.next);
		head.next.next = head;
		head.next = null;
	
		return newHead;
	}
}

class Node{
	int val;
	Node next;
	public Node(int val){
		this.val = val;
	}
}
