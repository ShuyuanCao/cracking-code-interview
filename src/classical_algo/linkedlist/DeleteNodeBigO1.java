package classical_algo.linkedlist;

public class DeleteNodeBigO1 {
	
	/**
	 * 题目描述：给定链表的头指针和一个节点指针，在O(1)时间删除该节点。
	 *
	 */
	
	public void deleNodeBigO1(Node target) {
	
		//delete the next node of target and assign value of
		// next node to target.
		// NOTE: the target node shouldn't be last node in the linkedlist.
		target.val = target.next.val;
		target.next = target.next.next;
	
	}
}
