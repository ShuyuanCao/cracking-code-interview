package classical_algo.sorting_algorithms;

public class SortingAlgorithms {
	
	/**
	 *  This Java program is for reviewing 8 different sorting
	 *  algorithms.
	 *
	 *  ******************Bubble Sort*****************
	 *
	 *  First Iteration(Ascending Order):
	 *         7, 6, 5, 4, 3, 2, 1
	 *         6, 7, 5, 4, 3, 2, 1
	 *         6, 5, 7, 4, 3, 2, 1
	 *         6, 5, 4, 7, 3, 2, 1
	 *         6, 5, 4, 3, 7, 2, 1
	 *         6, 5, 4, 3, 2, 7, 1
	 *         6, 5, 4, 3, 2, 1, 7
	 *
	 *  After 1st iteration: [6, 5, 4, 3, 2, 1], 7
	 *     ...2nd iteration: [5, 4, 3, 2, 1], 6, 7
	 *     ...3rd iteration: [4, 3, 2, 1], 5, 6, 7
	 *     ...4th iteration: [3, 2, 1], 4, 5, 6, 7
	 *     ...5th iteration: [2, 1], 3, 4, 5, 6, 7
	 *     ...6th iteration: [1], 2, 3, 4, 5, 5, 7
	 *
	 * @param nums
	 */
	public void bubleSort(int[] nums){
	
		int tmp = 0;
		int size = nums.length;
		
		// i should stop at the second element counting from the back,
		// then we can compare the last pair.
		for(int i = 0; i < size-1; i++){
			
			// when compare the pairs, every time start from beginning.
			// the val at position nums.length-i is biggest val found in last round.
			// so the final pair in current round should be nums.length-i-1 and nums.length-i-2
			for(int j = 0; j < nums.length - i - 1; j++){
				if(nums[j] > nums[j + 1]){
					// swap these 2 numbers
					tmp = nums[j + 1];
					nums[j + 1] = nums[j];
					nums[j] = tmp;
				}
			}
			
		}
	
	}
	
	/**
	 *  ******************Quick Sort*****************
	 *
	 *  Methodology: Divide && Conquer
	 *
	 *  Example(will be sorted in ascending order):
	 *      Initial status:  3(i), 4, 5, 2, 1(j)    pivot: 3 (first status in the initial status)
	 *
	 *      Target: starting from position j, find those val which is smaller than
	 *              pivot and move this val to pivot's position. Then starting from
	 *              position i, find those val which is bigger than pivot and move this
	 *              val to the previous empty value at j.
	 *
	 *      Steps:
	 *          1st iteration:
	 *              1st swap:   1,     4(i),        5,        2,   empty(j)        pivot: 3      3 > 1
	 *              2nd swap:   1, empty(i),        5,     2(j),   4               pivot: 3      3 < 4
	 *              3rd swap:   1,        2,     5(i), empty(j),   4               pivot: 3      3 > 2
	 *              4th swap:   1,        2, empty(i),        5,   4               pivot: 3      3 < 5
	 *              final:      1,        2,     3(i),     5(j),   4
	 */
	public void quickSort(int nums[]){
		
		int low = 0;
		int high = nums.length - 1;
		int tmp = nums[low];
		
		while(low < high){
		
			// check those nums which are bigger than pivot
			
			
		}
	
	}

}
