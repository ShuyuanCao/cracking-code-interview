package classical_algo.sorting_algorithms;

public class BubbleSort {
	
	public int[] sort(int[] tmp) {
		
		for(int i = 0; i < tmp.length; i++){
		
			for(int j = 0; j < tmp.length - i - 1; j++){
				if(tmp[j] > tmp[j + 1]){
					int tmpVal = tmp[j + 1];
					tmp[j + 1] = tmp[j];
					tmp[j] = tmpVal;
				}
			}
		
		}
		
		return tmp;
	}
	
}
