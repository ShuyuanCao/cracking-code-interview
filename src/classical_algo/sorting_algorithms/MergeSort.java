package classical_algo.sorting_algorithms;

public class MergeSort {
	
	/**
	 * MergeSort in Ascending Order.
	 * Time Complexity:
	 *      O(nlogn)
	 *
	 * Reference:
	 *      https://blog.csdn.net/jianyuerensheng/article/details/51262984
	 * @param input
	 * @param lo
	 * @param hi
	 */
	public void mergeSort(int[] input, int lo, int hi){
		int mid = (lo + hi)/2;
		
		if(lo < hi){
			// recurse left side
			mergeSort(input, lo, mid);
			
			// recurse right side
			mergeSort(input, mid + 1, hi);
			
			// merge left and right side
			merge(input, lo, mid, hi);
		}
		
	}
	
	private void merge(int[] input, int lo, int mid, int hi) {
		int[] tmp = new int[hi - lo + 1];
		int i = lo; // left pointer
		int j = mid + 1; // right pointer
		int k = 0;
		
		// 把较小的数先移到new数组中
		while(i <= mid && j <= hi){
			if(input[i] < input[j]){
				tmp[k++] = input[i++];
			}else{
				tmp[k++] = input[j++];
			}
		}
		
		// 把左边剩余的数移入数组
		while(i <= mid){
			tmp[k++] = input[i++];
		}
		
		// 把右边剩余的数移入数组
		while(j <= hi){
			tmp[k++] = input[j++];
		}
		
		// 把新数组中的数覆盖掉 input数组中的数
		for(int k2 = 0; k2 < tmp.length; k2++){
			input[k2 + lo] = tmp[k2];
		}
		
	}
	
}
