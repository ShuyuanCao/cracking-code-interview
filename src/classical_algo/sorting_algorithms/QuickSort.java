package classical_algo.sorting_algorithms;

public class QuickSort {
	
	/**
	 * Reference:
	 *      https://blog.csdn.net/morewindows/article/details/6684558
	 *
	 * @param tmp
	 * @return
	 */
	public void sort(int[] tmp, int left, int right) {
		
		if(left < right){
		
			int pivotPosition = partition(tmp, left, right);
			sort(tmp, left, pivotPosition - 1);
			sort(tmp, pivotPosition + 1, right);
		
		}
		
	}
	
	private int partition(int[] tmp, int left, int right) {
		
		int x = tmp[left]; // pivot
		
		while(left < right){
		
			// 从右向左来找小于x的数来填tmp[left]
			while(left < right && tmp[right] >= x){
				right--;
			}
			if(left < right){
				tmp[left] = tmp[right];
				left++;
			}
			
			// 从左向右找大于x的数来填tmp[right]
			while(left < right && tmp[left] < x){
				left++;
			}
			if(left < right){
				tmp[right] = tmp[left];
				right--;
			}
		}
		
		tmp[left] = x;
		return left;
	}
	
}
