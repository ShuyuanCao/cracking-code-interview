package classical_algo.sorting_algorithms;

public class ShellSort {
	/**
	 * Reference:
	 *      https://blog.csdn.net/jianyuerensheng/article/details/51258460
	 *
	 * @param tmp
	 * @return
	 */
	public int[] sort(int[] tmp) {
		
		int j = 0;
		int tmpVal = 0;
		
		for(int increment = tmp.length/2; increment > 0; increment /= 2){
		
			for(int i = increment; i < tmp.length; i++){
				
				tmpVal = tmp[i];
				
				for(j = i - increment; j >= 0; j -= increment){
				
					if(tmpVal < tmp[j]){
						tmp[j + increment] = tmp[j];
					}else{
						break;
					}
				
				}
				
				tmp[j + increment] = tmpVal;
			}
		
		}
		
		return tmp;
	}
}
