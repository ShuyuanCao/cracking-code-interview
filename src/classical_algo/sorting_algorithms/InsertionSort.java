package classical_algo.sorting_algorithms;

public class InsertionSort {
	
	public int[] sort(int[] tmp) {
		
		for(int i = 1; i < tmp.length; i++){
			int j = i - 1;
			int key = tmp[i];
			
			while(j >= 0 && tmp[j] > key){
				tmp[j + 1] = tmp[j];
				j--;
			}
			
			tmp[j + 1] = key;
		}
		
		return tmp;
	}
	
}
