package IndeedPractice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PasswordCandidates18OA {
	
	private static int passwordCandidate(int N, String T) {
		
		// check N and T
		if(N <= 0 || T == null || T.trim().length() == 0 || N <= T.trim().length()){
			return 0;
		}
		
		// check whether T only contains english letters and uppercase letter or not
		char[] array = T.toCharArray();
		for (char tmp : array) {
			if (tmp < 'a' || tmp > 'z') {
				return 0;
			}
		}
		
		int numOfPotentialPwd = (int) (Math.pow(26, N) - (N - T.length() + 1) * 26);
		
		// remove duplicates
		
		
		return numOfPotentialPwd;
	}

	public static void main(String args[]){
		Scanner s = new Scanner(System.in);
		
		List<String> tmp = new ArrayList<>();
		
		while(s.hasNext()){
			tmp.add(s.next());
			
			if(tmp.size() == 2){
				System.out.println(tmp);
				System.out.println(passwordCandidate(Integer.parseInt(tmp.get(0)), tmp.get(1)));
				
				break;
			}
			
		}
		
		s.close();
	}
}
