package IndeedPractice;


public class OA5LC53MaximumSubArray {
	
	/**
	 * Solution 1:
	 *      Time Limited Exceeded.
	 * @param nums
	 * @return
	 */
	public int maxSubArrayS1(int[] nums) {
		if(nums == null || nums.length < 1){
			return 0;
		}
		
		// NOTE: corner case, so should initialize sum with Integer.MIN_VALUE
		int sum = Integer.MIN_VALUE;
		int tmp = 0;
		for(int i = 0; i < nums.length; i++){
			for(int j = i; j < nums.length; j++){
				for(int t = i; t <= j; t++){
					tmp += nums[t];
				}
				
				if(tmp > sum){
					sum = tmp;
				}
				// NOTE: to clear tmp
				tmp = 0;
			}
		}
		
		return sum;
	}
	
	/**
	 *
	 * Reference: https://blog.csdn.net/linhuanmars/article/details/21314059
	 * NOTES: Using Dynamic Programming
	 *      This is a Local Optima and Global Optima problem
	 *      Time Complexity: O(n), n is number of elements in the array
	 *      Space Complexity: O(1), only 2 variable: localOptima and globalOptima
	 */
	public int maxSubArrayS2(int[] nums){
		
		if(nums == null || nums.length == 0){
			return 0;
		}
		int localOptima = nums[0];
		int globalOptima = nums[0];
		
		for(int i = 1; i < nums.length; i++){
			localOptima = Math.max(localOptima + nums[i], nums[i]);
			globalOptima = Math.max(localOptima, globalOptima);
		}
		
		return globalOptima;
	}
	
	/**
	 *  Divide and Conquer
	 *      Time Complexity: O(nlogn)
	 *
	 *      Reference: https://simpleandstupid.com/2014/10/28/maximum-subarray-leetcode-解题笔记/
	 */
	public int maxSubArrayS3(int[] nums){
		if(nums == null || nums.length == 0){
			return 0;
		}
		int maxSum = Integer.MIN_VALUE;
		return findMaxSub(nums, 0, nums.length - 1, maxSum);
	}
	
	// recursive to find max sum
	// may appear on the left or right part, or across mid(from left to right)
	private int findMaxSub(int[] nums, int left, int right, int maxSum) {
		
		if(left > right) return Integer.MIN_VALUE;
		
		// get max sub sum from both left and right cases
		int mid = (left + right)/2;
		int leftMax = findMaxSub(nums, left, mid - 1, maxSum);
		int rightMax = findMaxSub(nums,mid + 1, right, maxSum);
		maxSum = Math.max(maxSum, Math.max(leftMax, rightMax));
		
		// get max sum of this range (case: across mid)
		// so need to expend to both left and right using mid as center
		// mid -> left
		int sum = 0, midLeftMax = 0;
		for(int i = mid - 1; i >= left; i--){
			sum += nums[i];
			if(sum > midLeftMax) midLeftMax = sum;
		}
		// mid -> right
		int midRightMax = 0;
		sum = 0;
		for(int i = mid + 1; i <= right; i++){
			sum += nums[i];
			if(sum > midRightMax) midRightMax = sum;
		}
		
		// get the max value from the left, right and across mid
		maxSum = Math.max(maxSum, midLeftMax + midRightMax + nums[mid]);
		
		return maxSum;
	}
	
}
